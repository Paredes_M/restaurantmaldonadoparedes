-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: restaurante
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comida`
--

DROP TABLE IF EXISTS `comida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comida` (
  `num_comida` int(11) NOT NULL,
  `nombre_comida` varchar(80) DEFAULT NULL,
  `descripcion_comida` varchar(400) DEFAULT NULL,
  `es_platillo` tinyint(4) NOT NULL,
  `precio_unitario` double NOT NULL,
  PRIMARY KEY (`num_comida`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comida`
--

LOCK TABLES `comida` WRITE;
/*!40000 ALTER TABLE `comida` DISABLE KEYS */;
INSERT INTO `comida` VALUES (1,'hamburguesa completa','Hamburguesa casera, con queso cheddar, tomate y lechuga',1,350),(2,'milanesa de pollo completa','Milanesa con salsa, queso, y tomate',1,400),(3,'gaseosa coca cola','Agua carbonatada sabor cola',0,70),(4,'gaseosa sprite','Agua carbonatada sabor lima limon',0,70),(5,'gaseosa fanta','Agua carbonatada sabor naranja',0,70),(6,'fideos con salsa','Fideos con salsa roja, acompañada con pollo o carne',1,430),(7,'papas fritas','Papas cortadas y fritadas, con sal',1,160);
/*!40000 ALTER TABLE `comida` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comida_ingrediente`
--

DROP TABLE IF EXISTS `comida_ingrediente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comida_ingrediente` (
  `num_comida` int(11) NOT NULL,
  `num_ingrediente` int(11) NOT NULL,
  `cant_ingredientes_usado` int(11) DEFAULT NULL,
  PRIMARY KEY (`num_comida`,`num_ingrediente`),
  KEY `fkey_idx_num_ingrediente` (`num_ingrediente`),
  KEY `fkey_idx_num_comida` (`num_comida`),
  CONSTRAINT `fkey_num_comida` FOREIGN KEY (`num_comida`) REFERENCES `comida` (`num_comida`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkey_num_ingrediente` FOREIGN KEY (`num_ingrediente`) REFERENCES `ingrediente` (`num_ingrediente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comida_ingrediente`
--

LOCK TABLES `comida_ingrediente` WRITE;
/*!40000 ALTER TABLE `comida_ingrediente` DISABLE KEYS */;
INSERT INTO `comida_ingrediente` VALUES (1,1,1),(1,2,1),(1,3,1),(1,4,2),(1,5,1),(1,6,1),(1,7,1),(1,8,1),(1,9,1),(1,10,1),(1,11,1),(1,15,1),(1,18,1),(1,26,1),(2,1,3),(2,2,1),(2,4,1),(2,6,1),(2,12,1),(2,13,1),(2,15,2),(2,16,1),(2,17,1),(2,18,1),(2,29,1),(2,30,1),(3,21,1),(4,22,1),(5,23,1),(6,7,1),(6,12,3),(6,13,2),(6,15,1),(6,16,1),(6,18,2),(6,24,2),(6,25,2),(6,27,1),(6,28,1),(6,31,1),(7,4,3),(7,14,3),(7,16,4);
/*!40000 ALTER TABLE `comida_ingrediente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `legajo` int(11) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  `apellido` varchar(80) NOT NULL,
  `dni` varchar(8) NOT NULL,
  `num_contacto` varchar(15) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `usuario` varchar(50) NOT NULL,
  `contraseña` varchar(50) NOT NULL,
  `tipo_empleado` varchar(15) NOT NULL,
  PRIMARY KEY (`legajo`),
  UNIQUE KEY `dni_UNIQUE` (`dni`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` VALUES (1,'Pepe','Sanchez','21547875','+542974356987','pepe_email@gmail.com','admin','root','Administrador'),(2,'Miguel','Paredes','37150499','+542975949959','miguel_arg@live.com','meseroMiguel','1234','Mesero'),(3,'Lucas','Maldonado','39203577','+542975022846','maldoluk@gmail.com','cocineroLucas','4321','Cocinero');
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingrediente`
--

DROP TABLE IF EXISTS `ingrediente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingrediente` (
  `num_ingrediente` int(11) NOT NULL,
  `nombre_ingrediente` varchar(80) DEFAULT NULL,
  `es_envasado` tinyint(4) NOT NULL,
  `es_bebida` tinyint(4) NOT NULL,
  `descripcion_ingrediente` varchar(400) DEFAULT NULL,
  `cant_ingrediente` int(11) DEFAULT NULL,
  `stock_ingrediente` int(11) DEFAULT NULL,
  PRIMARY KEY (`num_ingrediente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingrediente`
--

LOCK TABLES `ingrediente` WRITE;
/*!40000 ALTER TABLE `ingrediente` DISABLE KEYS */;
INSERT INTO `ingrediente` VALUES (1,'huevo',0,0,'Huevo de gallina',60,6),(2,'pan rallado',0,0,'Pan duro rallado',60,6),(3,'perejil',0,0,'Una planta para condimentar',50,5),(4,'sal',0,0,'Sustancia cristalina para condimentar',100,10),(5,'carne picada de ternera',0,0,'Esto es carne picada de ternera',50,5),(6,'pimienta molida negra',0,0,'Grano de una planta negra para condimentar',50,5),(7,'pimenton dulce',0,0,'Pimenton o aji de color es un condimento en polvo de color rojo',50,5),(8,'sal de ajo',0,0,'Mezcla de ajo seco molido y sal de mesa con un humectante',50,5),(9,'pan de hamburguesa',0,0,'Esto es un pan',60,6),(10,'queso chedar',0,0,'Queso palido de sabor agrio',60,6),(11,'lechuga',0,0,'Planta herbacea verde y plana',50,5),(12,'tomate triturado',1,0,'Planta herbacea roja y redondeada envasado',50,5),(13,'pechuga de pollo',0,0,'Pechuga del pollo',50,5),(14,'papa',0,0,'Tuberculo para acompañar en comidas',100,10),(15,'diente de ajo',0,0,'Bulbillo o diente del ajo',120,12),(16,'aceite de girasol',0,0,'Aceite de origen vegetal que se extrae del prensado de las semilla de girasol',120,20),(17,'queso mozzarella',0,0,'Tipo de queso originario de la cocina italiana',60,6),(18,'cebolla',0,0,'Planta herbacea con numerosas propiedades nutritivas y medicinales',60,6),(19,'pimienta molida blanca',0,0,'Grano de una planta blanca para condimentar',60,6),(20,'oregano',0,0,'Planta medicinal para condimentar',60,6),(21,'coca cola de 350ml',1,1,'Botella de gaseosa coca cola como para un vaso',30,3),(22,'sprite de 350ml',1,1,'Botella de gaseosa sprite como para un vaso',30,3),(23,'fanta de 350ml',1,1,'Botella de gaseosa fanta como para un vaso',30,3),(24,'tallarines',0,0,'Un tipo de pasta alargada, de ancho pequeño y forma achatada',80,8),(25,'zanahoria',0,0,'Hortaliza rica en potazio y fosforo',60,6),(26,'tomate',0,0,'Planta herbacea',50,5),(27,'cubo de caldo de pollo',0,0,'Es una forma concentrada y deshidratada de almacenar un caldo de pollo',50,5),(28,'adobo para pizza',0,0,'Oregano, perejil, aji molido, pimenton y laurel en polvo',50,5),(29,'vinagre balsamico',1,0,'Vinagre de origen italiano',50,5),(30,'azucar',0,0,'Endulzante de origen natural, solido, cristalizado',120,12),(31,'morron',0,0,'Pimiento carnoso',50,5);
/*!40000 ALTER TABLE `ingrediente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mesa`
--

DROP TABLE IF EXISTS `mesa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mesa` (
  `num_mesa` int(11) NOT NULL,
  `cant_mesa` int(11) DEFAULT NULL,
  `estado_mesa` tinyint(4) NOT NULL,
  PRIMARY KEY (`num_mesa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mesa`
--

LOCK TABLES `mesa` WRITE;
/*!40000 ALTER TABLE `mesa` DISABLE KEYS */;
INSERT INTO `mesa` VALUES (1,0,1),(2,0,1),(3,0,1),(4,0,1),(5,0,1),(6,0,1),(7,0,1),(8,0,1),(9,0,1),(10,0,1),(11,0,1),(12,0,1),(13,0,1),(14,0,1),(15,0,1);
/*!40000 ALTER TABLE `mesa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido`
--

DROP TABLE IF EXISTS `pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido` (
  `num_pedido` int(11) NOT NULL,
  `num_venta` int(11) NOT NULL,
  `num_mesa` int(11) NOT NULL,
  `descripcion_pedido` varchar(400) DEFAULT NULL,
  `terminado_pedido` tinyint(4) NOT NULL,
  `entregado_mesa` tinyint(4) NOT NULL,
  `precio_pedido` double DEFAULT NULL,
  PRIMARY KEY (`num_pedido`),
  KEY `num_mesa_fkey_idx` (`num_mesa`),
  KEY `num_venta_idx` (`num_venta`),
  CONSTRAINT `num_mesa_fkey` FOREIGN KEY (`num_mesa`) REFERENCES `mesa` (`num_mesa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `num_venta_fkey` FOREIGN KEY (`num_venta`) REFERENCES `venta` (`num_venta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido`
--

LOCK TABLES `pedido` WRITE;
/*!40000 ALTER TABLE `pedido` DISABLE KEYS */;
INSERT INTO `pedido` VALUES (1,1,15,'Sin queso la milanesa',1,1,630),(2,1,15,'Sin tomate la hamburguesa',1,1,420),(3,2,12,'Normal',1,1,420),(4,3,5,'Normal',1,1,420);
/*!40000 ALTER TABLE `pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_comida`
--

DROP TABLE IF EXISTS `pedido_comida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_comida` (
  `num_pedido` int(11) NOT NULL,
  `num_comida` int(11) NOT NULL,
  PRIMARY KEY (`num_pedido`,`num_comida`),
  KEY `num_comida_fkey_idx` (`num_comida`),
  KEY `num_pedido_fkey_idx` (`num_pedido`),
  CONSTRAINT `num_comida_fkey` FOREIGN KEY (`num_comida`) REFERENCES `comida` (`num_comida`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `num_pedido_fkey` FOREIGN KEY (`num_pedido`) REFERENCES `pedido` (`num_pedido`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_comida`
--

LOCK TABLES `pedido_comida` WRITE;
/*!40000 ALTER TABLE `pedido_comida` DISABLE KEYS */;
INSERT INTO `pedido_comida` VALUES (2,1),(3,1),(4,1),(1,2),(1,3),(4,3),(2,4),(3,5),(1,7);
/*!40000 ALTER TABLE `pedido_comida` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venta`
--

DROP TABLE IF EXISTS `venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venta` (
  `num_venta` int(11) NOT NULL,
  `legajo` int(11) NOT NULL,
  `total_pagar` double DEFAULT NULL,
  `fecha_venta` date DEFAULT NULL,
  `hora_venta` time DEFAULT NULL,
  PRIMARY KEY (`num_venta`),
  KEY `legajo_fkey_idx` (`legajo`),
  CONSTRAINT `legajo_fkey` FOREIGN KEY (`legajo`) REFERENCES `empleado` (`legajo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venta`
--

LOCK TABLES `venta` WRITE;
/*!40000 ALTER TABLE `venta` DISABLE KEYS */;
INSERT INTO `venta` VALUES (1,2,1050,'2019-11-11','13:00:00'),(2,2,420,'2019-11-11','13:15:00'),(3,2,420,'2019-11-11','13:30:00');
/*!40000 ALTER TABLE `venta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-08 16:20:47
