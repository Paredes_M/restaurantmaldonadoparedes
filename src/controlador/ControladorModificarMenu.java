package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import modelo.Comida;
import vista.ModificarMenu;

public class ControladorModificarMenu implements ActionListener {

	private ModificarMenu menu;

	public ControladorModificarMenu() {
		this.setMenu(new ModificarMenu(this));
	}

	public ModificarMenu getMenu() {
		return menu;
	}

	public void setMenu(ModificarMenu menu) {
		this.menu = menu;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == this.getMenu().getBtnSiguiente()) {

			String num_comida = this.getMenu().getTxtNumeroComida().getText();
			String nombre_comida = this.getMenu().getTxtNombreComida().getText();
			String descripcion_comida = this.getMenu().getTextDescripcion().getText();
			String es_platillo = "";
			if (this.getMenu().getRdbtnPlatilloSi().isSelected()) {
				es_platillo = "true";
			}
			if (this.getMenu().getRdbtnPlatilloNo().isSelected()) {
				es_platillo = "false";
			}
			String precio_unitario = this.getMenu().getTxtPrecioUnitario().getText();
			Boolean validacion = (!num_comida.equals("") && !nombre_comida.equals("") && !descripcion_comida.equals("") && !es_platillo.equals("")
					&& !precio_unitario.equals(""));
			if (e.getSource() == this.getMenu().getBtnSiguiente()) {
				if (validacion) {
					Comida comida = new Comida(Integer.valueOf(num_comida), nombre_comida, descripcion_comida, Boolean.valueOf(es_platillo), Double.valueOf(precio_unitario));
					
					this.getMenu().setVisible(false);
					new ControladorModificarMenuDos(comida);
				} else {
					JOptionPane.showMessageDialog(this.getMenu(), "Complete todos los campos", "Sistema",
							JOptionPane.ERROR_MESSAGE);
				}
		}

		if (e.getSource() == this.getMenu().getBtnCancelar()) {

			int n = JOptionPane.showConfirmDialog(this.getMenu(), "�Deseas cancelar? ", "Cancelar Operacion ",
					JOptionPane.YES_NO_OPTION);

			if (n == JOptionPane.YES_OPTION) {

				this.getMenu().setVisible(false);
			}
		}
	}

}
}
