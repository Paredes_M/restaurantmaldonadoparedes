
package controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import modelo.Empleado;
import modelo.Mesa;
import modelo.MesaDaoImp;
import modelo.Pedido;
import modelo.PedidoDaoImp;
import modelo.Venta;
import modelo.VentaDaoImp;
import vista.VistaInicio;

public class ControladorInicio implements ActionListener, MouseListener, WindowListener { // CONTROLADOR DE LA VISTA DE
																							// MESAS

	private VistaInicio vistaI;
	private MesaDaoImp mdi;
	private VentaDaoImp vdi;
	private PedidoDaoImp pdi;
	private Empleado mesero;

	public ControladorInicio(Empleado mesero) {
		this.setVistaI(new VistaInicio(this));
		this.setMdi(new MesaDaoImp());
		this.setVdi(new VentaDaoImp());
		this.setPdi(new PedidoDaoImp());
		this.setMesero(mesero);
		getVistaI().setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getSource() == this.getVistaI().getMntmAlmacen()) {

			ControladorIngredienteABM controlador = new ControladorIngredienteABM();
			controlador.getIngABM().getBtnAgregar().setEnabled(false);
			controlador.getIngABM().getBtnModificar().setEnabled(false);
			controlador.getIngABM().getBtnBorrar().setEnabled(false);
			controlador.getIngABM().setVisible(true);
		}

		if (arg0.getSource() == this.getVistaI().getMntmMenu()) {

			ControladorMenuABM controlador = new ControladorMenuABM();
			controlador.getMenu().getBtnAltaPlatillo().setEnabled(false);
			controlador.getMenu().getBtnModificarPlatillo().setEnabled(false);
			controlador.getMenu().getBtnBorrarPlatillo().setEnabled(false);
			controlador.getMenu().setVisible(true);
			this.getVistaI().setAlwaysOnTop(false);
		}

		if (arg0.getSource() == this.getVistaI().getMntmSalir()) {

			int n = JOptionPane.showConfirmDialog(this.getVistaI(), "�Deseas Cerrar Sesion? ", "Salir ",
					JOptionPane.YES_NO_OPTION);

			if (n == JOptionPane.YES_OPTION) {
				ControladorLogin cont = new ControladorLogin();
				cont.getVista().setVisible(true);
				this.getVistaI().setVisible(false);

			}

		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		ArrayList<Mesa> mesasBase = this.getMdi().buscarMesas();

		if (e.getSource() == this.getVistaI().getBtnMesa1()) {
			Venta ventaMesa1 = new Venta(this.getVdi().nuevoNumVenta(), this.getMesero().getLegajo(), 0.00,
					LocalDate.now(), LocalTime.now());
			new ControladorPedido(mesasBase.get(0), ventaMesa1);
		}

		if (e.getSource() == this.getVistaI().getBtnMesa2()) {
			Venta ventaMesa2 = new Venta(this.getVdi().nuevoNumVenta(), this.getMesero().getLegajo(), 0.00,
					LocalDate.now(), LocalTime.now());
			new ControladorPedido(mesasBase.get(1), ventaMesa2);
		}

		if (e.getSource() == this.getVistaI().getBtnMesa3()) {
			Venta ventaMesa3 = new Venta(this.getVdi().nuevoNumVenta(), this.getMesero().getLegajo(), 0.00,
					LocalDate.now(), LocalTime.now());
			new ControladorPedido(mesasBase.get(2), ventaMesa3);
		}

		if (e.getSource() == this.getVistaI().getBtnMesa4()) {
			Venta ventaMesa4 = new Venta(this.getVdi().nuevoNumVenta(), this.getMesero().getLegajo(), 0.00,
					LocalDate.now(), LocalTime.now());
			new ControladorPedido(mesasBase.get(3), ventaMesa4);
		}

		if (e.getSource() == this.getVistaI().getBtnMesa5()) {
			Venta ventaMesa5 = new Venta(this.getVdi().nuevoNumVenta(), this.getMesero().getLegajo(), 0.00,
					LocalDate.now(), LocalTime.now());
			new ControladorPedido(mesasBase.get(4), ventaMesa5);
		}

		if (e.getSource() == this.getVistaI().getBtnMesa6()) {
			Venta ventaMesa6 = new Venta(this.getVdi().nuevoNumVenta(), this.getMesero().getLegajo(), 0.00,
					LocalDate.now(), LocalTime.now());
			new ControladorPedido(mesasBase.get(5), ventaMesa6);
		}

		if (e.getSource() == this.getVistaI().getBtnMesa7()) {
			Venta ventaMesa7 = new Venta(this.getVdi().nuevoNumVenta(), this.getMesero().getLegajo(), 0.00,
					LocalDate.now(), LocalTime.now());
			new ControladorPedido(mesasBase.get(6), ventaMesa7);
		}

		if (e.getSource() == this.getVistaI().getBtnMesa8()) {
			Venta ventaMesa8 = new Venta(this.getVdi().nuevoNumVenta(), this.getMesero().getLegajo(), 0.00,
					LocalDate.now(), LocalTime.now());
			new ControladorPedido(mesasBase.get(7), ventaMesa8);
		}

		if (e.getSource() == this.getVistaI().getBtnMesa9()) {
			Venta ventaMesa9 = new Venta(this.getVdi().nuevoNumVenta(), this.getMesero().getLegajo(), 0.00,
					LocalDate.now(), LocalTime.now());
			new ControladorPedido(mesasBase.get(8), ventaMesa9);
		}

		if (e.getSource() == this.getVistaI().getBtnMesa10()) {
			Venta ventaMesa10 = new Venta(this.getVdi().nuevoNumVenta(), this.getMesero().getLegajo(), 0.00,
					LocalDate.now(), LocalTime.now());
			new ControladorPedido(mesasBase.get(9), ventaMesa10);
		}

		if (e.getSource() == this.getVistaI().getBtnMesa11()) {
			Venta ventaMesa11 = new Venta(this.getVdi().nuevoNumVenta(), this.getMesero().getLegajo(), 0.00,
					LocalDate.now(), LocalTime.now());
			new ControladorPedido(mesasBase.get(10), ventaMesa11);
		}
		if (e.getSource() == this.getVistaI().getBtnMesa12()) {
			Venta ventaMesa12 = new Venta(this.getVdi().nuevoNumVenta(), this.getMesero().getLegajo(), 0.00,
					LocalDate.now(), LocalTime.now());
			new ControladorPedido(mesasBase.get(11), ventaMesa12);
		}
		if (e.getSource() == this.getVistaI().getBtnMesa13()) {
			Venta ventaMesa13 = new Venta(this.getVdi().nuevoNumVenta(), this.getMesero().getLegajo(), 0.00,
					LocalDate.now(), LocalTime.now());
			new ControladorPedido(mesasBase.get(12), ventaMesa13);
		}
		if (e.getSource() == this.getVistaI().getBtnMesa14()) {
			Venta ventaMesa14 = new Venta(this.getVdi().nuevoNumVenta(), this.getMesero().getLegajo(), 0.00,
					LocalDate.now(), LocalTime.now());
			new ControladorPedido(mesasBase.get(13), ventaMesa14);
		}
		if (e.getSource() == this.getVistaI().getBtnMesa15()) {
			Venta ventaMesa15 = new Venta(this.getVdi().nuevoNumVenta(), this.getMesero().getLegajo(), 0.00,
					LocalDate.now(), LocalTime.now());
			new ControladorPedido(mesasBase.get(14), ventaMesa15);
		}

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	public VistaInicio getVistaI() {
		return vistaI;
	}

	public void setVistaI(VistaInicio vistaI) {
		this.vistaI = vistaI;
	}

	@Override
	public void windowOpened(WindowEvent e) {

		ArrayList<Mesa> mesasBase = this.getMdi().buscarMesas();

		for (JButton botonMesa : this.getVistaI().getBotonesMesas()) {

			Integer i = 0;
			if (mesasBase.get(i).getEstado()) {
				botonMesa.setBackground(Color.GREEN);
			} else {
				botonMesa.setBackground(Color.RED);
			}
			i++;
		}
		String[] columnas = { "Numero Pedido", "Numero Mesa", "Terminado", "Entregado" };
		DefaultTableModel modelo = new DefaultTableModel();
		modelo.setColumnIdentifiers(columnas);
		ArrayList<Pedido> pedidos = new ArrayList<Pedido>();
		pedidos = this.getPdi().buscarPedidosTerminadoNoEntregado();
		for (Pedido pedido : pedidos) {
			Object[] row = new Object[4];
			row[0] = pedido.getNum_pedido();
			row[1] = pedido.getNum_mesa();
			row[2] = pedido.getTerminado_pedido();
			row[3] = pedido.getEntregado_mesa();
			modelo.addRow(row);
		}
		this.getVistaI().getTable().setModel(modelo);
		this.getVistaI().getContentPane().revalidate();
	}

	@Override
	public void windowClosing(WindowEvent e) {

	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {

	}

	@Override
	public void windowDeiconified(WindowEvent e) {

	}

	@Override
	public void windowActivated(WindowEvent e) {
		ArrayList<Mesa> mesasBase = this.getMdi().buscarMesas();
		for (JButton botonMesa : this.getVistaI().getBotonesMesas()) {

			Integer i = 0;
			if (mesasBase.get(i).getEstado()) {
				botonMesa.setBackground(Color.GREEN);

			} else {
				botonMesa.setBackground(Color.RED);
			}
			i++;
		}
		String[] columnas = { "Numero Pedido", "Numero Mesa", "Terminado", "Entregado" };
		DefaultTableModel modelo = new DefaultTableModel();
		modelo.setColumnIdentifiers(columnas);
		ArrayList<Pedido> pedidos = new ArrayList<Pedido>();
		pedidos = this.getPdi().buscarPedidosTerminadoNoEntregado();
		for (Pedido pedido : pedidos) {
			Object[] row = new Object[4];
			row[0] = pedido.getNum_pedido();
			row[1] = pedido.getNum_mesa();
			row[2] = pedido.getTerminado_pedido();
			row[3] = pedido.getEntregado_mesa();
			modelo.addRow(row);
		}
		this.getVistaI().getTable().setModel(modelo);
		this.getVistaI().getContentPane().revalidate();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {

	}

	public MesaDaoImp getMdi() {
		return mdi;
	}

	public void setMdi(MesaDaoImp mdi) {
		this.mdi = mdi;
	}

	public VentaDaoImp getVdi() {
		return vdi;
	}

	public void setVdi(VentaDaoImp vdi) {
		this.vdi = vdi;
	}

	public Empleado getMesero() {
		return mesero;
	}

	public void setMesero(Empleado mesero) {
		this.mesero = mesero;
	}

	public PedidoDaoImp getPdi() {
		return pdi;
	}

	public void setPdi(PedidoDaoImp pdi) {
		this.pdi = pdi;
	}

}
