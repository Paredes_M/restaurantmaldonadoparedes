package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JOptionPane;

import modelo.Empleado;
import modelo.EmpleadoDaoImp;
import vista.VistaNuevoUsuario;

public class ControladorNuevoUsuario implements ActionListener, KeyListener, MouseListener {

	private EmpleadoDaoImp empleado;
	private VistaNuevoUsuario vistaN;

	public ControladorNuevoUsuario() {

		// setModeloN(new ModeloNuevoUsuario());
		setVistaN(new VistaNuevoUsuario(this));

		getVistaN().setVisible(true);

		this.getVistaN().getRdbtnMesero().setSelected(true);
		this.getVistaN().getRdbtnCocinero().setSelected(false);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		String cadenaEmail = String.valueOf(this.getVistaN().getCmbEmail().getSelectedObjects());

		String legajo = this.getVistaN().getTxtLegajo().getText();

		String nombre = this.getVistaN().getTxtNombre().getText();

		String apellido = this.getVistaN().getTxtApellido().getText();

		String dni = this.getVistaN().getTxtDni().getText();

		String usuario = this.getVistaN().getTxtUsuario().getText();

		String contrasenia = this.getVistaN().getTxtContrasenia().getText() + cadenaEmail;

		String correoElectronico = this.getVistaN().getTxtEmail().getText();

		String numeroContacto = this.getVistaN().getTxtNcontacto().getText();

		Boolean validacion = (!legajo.equals("") && !nombre.equals("") && !apellido.equals("") && !dni.equals("")
				&& !usuario.equals("") && !contrasenia.equals("") && !correoElectronico.equals("")
				&& !numeroContacto.equals(""));

		System.out.println(validacion);

		if (e.getSource() == this.getVistaN().getBtnGuardarUsuario()) {

			if (this.getVistaN().getRdbtnMesero().isSelected()) {
				if (validacion) {
					Empleado emp = new Empleado(Integer.valueOf(legajo), nombre, apellido, dni, "Mesero", usuario, contrasenia,
							correoElectronico, numeroContacto);
					if (this.getEmpleado().agregarEmpleado(emp)) {
						JOptionPane.showMessageDialog(this.getVistaN(), "Empleado agregado correctamente", "Sistema",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(this.getVistaN(), "Problema al ingresar el empleado", "Sistema",
								JOptionPane.ERROR_MESSAGE);
					}

				} else {
					JOptionPane.showMessageDialog(this.getVistaN(), "Complete todos los campos", "Sistema",
							JOptionPane.ERROR_MESSAGE);
				}
			} else if (this.getVistaN().getRdbtnCocinero().isSelected()) {
				Empleado emp = new Empleado(Integer.valueOf(legajo), nombre, apellido, dni, "Cocinero" , usuario, contrasenia,
						correoElectronico, numeroContacto);
				JOptionPane.showMessageDialog(this.getVistaN(), "Empleado agregado correctamente", "Sistema",
						JOptionPane.INFORMATION_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(this.getVistaN(), "Problema al ingresar el empleado", "Sistema",
						JOptionPane.ERROR_MESSAGE);
			}

		} else {
			
			if (e.getSource() == this.getVistaN().getBtnCancelar()) {
				this.getVistaN().setVisible(false);
			}else {

			JOptionPane.showMessageDialog(this.getVistaN(), "Complete todos los campos", "Sistema",
					JOptionPane.ERROR_MESSAGE);
			}
		}

		// GUARDA EL USUARIO Y VUELVE AL LOGIN
		
	}

	public VistaNuevoUsuario getVistaN() {
		return vistaN;
	}

	public void setVistaN(VistaNuevoUsuario vistaN) {
		this.vistaN = vistaN;
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		if (e.getSource() == this.getVistaN().getRdbtnMesero()) {

			this.getVistaN().getRdbtnCocinero().setSelected(false);
		} else if (e.getSource() == this.getVistaN().getRdbtnMesero()) {

			this.getVistaN().getRdbtnMesero().setSelected(false);
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {

		char c = e.getKeyChar();
		if ((Character.isLetter(c)) || (this.getVistaN().getTxtLegajo().getText().length() > 0)) {
			this.getVistaN().getToolkit().beep();
			e.consume();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	public EmpleadoDaoImp getEmpleado() {
		return empleado;
	}

	public void setEmpleado(EmpleadoDaoImp empleado) {
		this.empleado = empleado;
	}

}
