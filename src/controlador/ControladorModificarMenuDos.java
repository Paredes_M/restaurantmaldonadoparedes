package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import modelo.Comida;
import modelo.ComidaDaoImp;
import modelo.Ingrediente;
import modelo.IngredienteDaoImp;
import modelo.Receta;
import vista.ModificarMenuDos;

public class ControladorModificarMenuDos implements ActionListener, WindowListener {
	
	private ModificarMenuDos menu2;
	private Comida comida;
	private ComidaDaoImp cdi;
	private IngredienteDaoImp idi;
	private ArrayList<Receta> recetas;

	public ControladorModificarMenuDos(Comida c) {
		this.setMenu2(new ModificarMenuDos(this));
		this.setCdi(new ComidaDaoImp());
		this.setIdi(new IngredienteDaoImp());
		this.setComida(c);
		this.setRecetas(this.getCdi().buscarRecetas(String.valueOf(this.getComida().getNum_comida())));
		this.getMenu2().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == this.getMenu2().getBtnOpcion1()) {

			this.getMenu2().setVisible(false);
			new ControladorModificarMenu();
		}

		if (e.getSource() == this.getMenu2().getBtnOpcion2()) {
			this.getCdi().ModificarRecetaComida(this.getRecetas(), this.getComida());
			JOptionPane.showMessageDialog(this.getMenu2(), "Modificado exitosamente.");
			this.getMenu2().setVisible(false);
		}
		
		if (e.getSource() == this.getMenu2().getBtnAniadir()) {
			Integer indice = this.getMenu2().getTable_1().getSelectedRow();
			String num_ingrediente = this.getMenu2().getTable_1().getValueAt(indice, 0).toString();
			Receta nuevareceta = new Receta(this.getComida().getNum_comida(), Integer.valueOf(num_ingrediente),
					Integer.valueOf(this.getMenu2().getTextCantidad().getText()));
			this.getRecetas().add(nuevareceta);
			String[] columnasRec = { "Numero Ingrediente", "Nombre Ingrediente", "Cantidad" };
			DefaultTableModel modeloRec = new DefaultTableModel();
			modeloRec.setColumnIdentifiers(columnasRec);
			for (Receta receta : this.getRecetas()) {
				Ingrediente ing = this.getIdi().buscarIngredientePorNumero(String.valueOf(receta.getNum_ingrediente()));
				Object[] row = new Object[3];
				row[0] = receta.getNum_ingrediente();
				row[1] = ing.getNombre_ingrediente();
				row[2] = receta.getCant_ingrediente_usado();
				modeloRec.addRow(row);
			}
			this.getMenu2().getTable().setModel(modeloRec);
		}

		if (e.getSource() == this.getMenu2().getBtnQuitar()) {
			this.getRecetas().remove(this.getMenu2().getTable().getSelectedRow());
			String[] columnasRec = { "Numero Ingrediente", "Nombre Ingrediente", "Cantidad" };
			DefaultTableModel modeloRec = new DefaultTableModel();
			modeloRec.setColumnIdentifiers(columnasRec);
			for (Receta receta : this.getRecetas()) {
				Ingrediente ing = this.getIdi().buscarIngredientePorNumero(String.valueOf(receta.getNum_ingrediente()));
				Object[] row = new Object[3];
				row[0] = receta.getNum_ingrediente();
				row[1] = ing.getNombre_ingrediente();
				row[2] = receta.getCant_ingrediente_usado();
				modeloRec.addRow(row);
			}
			this.getMenu2().getTable().setModel(modeloRec);
		}

		if (e.getSource() == this.getMenu2().getBtnBuscar()) {
			String[] columnas = { "Numero Ingrediente", "Nombre Ingrediente" };
			DefaultTableModel modelo = new DefaultTableModel(); // creo un nuevo modelo
			modelo.setColumnIdentifiers(columnas); // seteo las columnas del nuevo modelo
			ArrayList<Ingrediente> ingredientes = this.getIdi()
					.IngredienteporNombre(this.getMenu2().getTxtBusqueda().getText());
			for (Ingrediente ingrediente : ingredientes) {
				Object[] row = new Object[2];
				row[0] = ingrediente.getNum_ingrediente();
				row[1] = ingrediente.getNombre_ingrediente();
				modelo.addRow(row);
			}
			this.getMenu2().getTable_1().setModel(modelo);
		}
	}

	@Override
	public void windowActivated(WindowEvent e) {
		String[] columnasIng = { "Numero Ingrediente", "Nombre Ingrediente" };
		DefaultTableModel modeloIng = new DefaultTableModel();
		modeloIng.setColumnIdentifiers(columnasIng);
		String[] columnasRec = { "Numero Ingrediente", "Nombre Ingrediente", "Cantidad" };
		DefaultTableModel modeloRec = new DefaultTableModel();
		modeloRec.setColumnIdentifiers(columnasRec);
		ArrayList<Ingrediente> ingredientes = this.getIdi().buscarIngredientes();
		for (Ingrediente ingrediente : ingredientes) {
			Object[] row = new Object[2];
			row[0] = ingrediente.getNum_ingrediente();
			row[1] = ingrediente.getNombre_ingrediente();
			modeloIng.addRow(row);
		}
		for (Receta receta : this.getRecetas()) {
			Ingrediente ing = this.getIdi().buscarIngredientePorNumero(String.valueOf(receta.getNum_ingrediente()));
			Object[] row = new Object[3];
			row[0] = receta.getNum_ingrediente();
			row[1] = ing.getNombre_ingrediente();
			row[2] = receta.getCant_ingrediente_usado();
			modeloRec.addRow(row);
		}
		this.getMenu2().getTable_1().setModel(modeloIng);
		this.getMenu2().getTable().setModel(modeloRec);
	}

	@Override
	public void windowClosed(WindowEvent e) {
		
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		
		
	}
	
	public ModificarMenuDos getMenu2() {
		return menu2;
	}

	public void setMenu2(ModificarMenuDos menu2) {
		this.menu2 = menu2;
	}

	public Comida getComida() {
		return comida;
	}

	public void setComida(Comida comida) {
		this.comida = comida;
	}

	public ComidaDaoImp getCdi() {
		return cdi;
	}

	public void setCdi(ComidaDaoImp cdi) {
		this.cdi = cdi;
	}

	public IngredienteDaoImp getIdi() {
		return idi;
	}

	public void setIdi(IngredienteDaoImp idi) {
		this.idi = idi;
	}

	public ArrayList<Receta> getRecetas() {
		return recetas;
	}

	public void setRecetas(ArrayList<Receta> recetas) {
		this.recetas = recetas;
	}


}
