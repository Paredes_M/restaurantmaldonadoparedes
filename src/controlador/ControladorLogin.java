package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;

import modelo.Empleado;
import modelo.EmpleadoDaoImp;
import vista.VistaLogin;

public class ControladorLogin implements ActionListener, KeyListener { // CONTROLADOR DE LOGIN


	private VistaLogin vista;
	private EmpleadoDaoImp edi;

	public ControladorLogin() {
		this.setVista(new VistaLogin(this));
		this.setEdi(new EmpleadoDaoImp());
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		if (arg0.getSource() == this.getVista().getBtnIngresar()) {

			if (this.getVista().getTxtUsuario().getText().isEmpty()
					|| this.getVista().getPsfContrasenia().getText().isEmpty()) {
				JOptionPane.showMessageDialog(this.getVista(), "Complete todos los campos" , "Ingreso Inv�lido",
						JOptionPane.OK_OPTION);
			} else {
				String usuario = this.getVista().getTxtUsuario().getText();
				String contrase�a = this.getVista().getPsfContrasenia().getText();
				Empleado emp = this.getEdi().buscarEmpleadoPorUsuarioContrase�a(usuario, contrase�a);
				switch (emp.getTipo_empleado()) {
				case "Administrador":
					JOptionPane.showMessageDialog(this.getVista(), "Ingreso con exito", "Ingreso ",
							JOptionPane.INFORMATION_MESSAGE);
					new ControladorAdmin();
					this.getVista().setVisible(false);
					break;
				case "Mesero":
					JOptionPane.showMessageDialog(this.getVista(), "Ingreso con exito", "Ingreso ",
							JOptionPane.INFORMATION_MESSAGE);
					new ControladorInicio(emp);
					this.getVista().setVisible(false);
					break;
				case "Cocinero":
					JOptionPane.showMessageDialog(this.getVista(), "Ingreso con exito", "Ingreso ",
							JOptionPane.INFORMATION_MESSAGE);
					new ControladorCocina();
					this.getVista().setVisible(false);
					break;
				default:
					JOptionPane.showMessageDialog(this.getVista(), "Usuario o Contrase�a invalidos", "Ingreso Inv�lido",
							JOptionPane.OK_OPTION);
					break;
				}
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {

		if (e.getKeyChar() == KeyEvent.VK_ENTER) {
			if (this.getVista().getTxtUsuario().getText().isEmpty()
					|| this.getVista().getPsfContrasenia().getText().isEmpty()) {
				JOptionPane.showMessageDialog(this.getVista(), "Complete todos los campos" , "Ingreso Inv�lido",
						JOptionPane.OK_OPTION);
			} else {
				String usuario = this.getVista().getTxtUsuario().getText();
				String contrase�a = this.getVista().getPsfContrasenia().getText();
				Empleado emp = this.getEdi().buscarEmpleadoPorUsuarioContrase�a(usuario, contrase�a);
				switch (emp.getTipo_empleado()) {
				case "Administrador":
					JOptionPane.showMessageDialog(this.getVista(), "Ingreso con exito", "Ingreso ",
							JOptionPane.INFORMATION_MESSAGE);
					new ControladorAdmin();
					this.getVista().setVisible(false);
					break;
				case "Mesero":
					JOptionPane.showMessageDialog(this.getVista(), "Ingreso con exito", "Ingreso ",
							JOptionPane.INFORMATION_MESSAGE);
					new ControladorInicio(emp);
					this.getVista().setVisible(false);
					break;
				case "Cocinero":
					JOptionPane.showMessageDialog(this.getVista(), "Ingreso con exito", "Ingreso ",
							JOptionPane.INFORMATION_MESSAGE);
					new ControladorCocina();
					this.getVista().setVisible(false);
					break;
				default:
					JOptionPane.showMessageDialog(this.getVista(), "Usuario o Contrase�a invalidos", "Ingreso Inv�lido",
							JOptionPane.OK_OPTION);
					break;
				}
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {
	

	}
	public VistaLogin getVista() {
		return vista;
	}

	public void setVista(VistaLogin vista) {
		this.vista = vista;
	}

	public EmpleadoDaoImp getEdi() {
		return edi;
	}

	public void setEdi(EmpleadoDaoImp edi) {
		this.edi = edi;
	}

}
