package controlador;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import modelo.Empleado;
import modelo.EmpleadoDaoImp;
import vista.EmpleadoABM;

public class ControladorEmpleadoABM implements MouseListener, WindowListener {

	private EmpleadoABM epmABM;
	private EmpleadoDaoImp edi;

	public ControladorEmpleadoABM() {
		this.setEpmABM(new EmpleadoABM(this));
		this.setEdi(new EmpleadoDaoImp());
		this.getEpmABM().setVisible(true);
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		if (e.getSource() == this.getEpmABM().getBtnAgregar()) {

			this.getEpmABM().setModal(false);
			new ControladorAltaEmpleado();
		}

		if (e.getSource() == this.getEpmABM().getBtnModificar()) {
			this.getEpmABM().setModal(false);
			ControladorModificarEmpleado cme = new ControladorModificarEmpleado();
			Integer indice = this.getEpmABM().getTable().getSelectedRow();
			ArrayList<String> seleccion = new ArrayList<String>();
			seleccion.add(this.getEpmABM().getTable().getValueAt(indice, 0).toString());
			seleccion.add(this.getEpmABM().getTable().getValueAt(indice, 1).toString());
			seleccion.add(this.getEpmABM().getTable().getValueAt(indice, 2).toString());
			seleccion.add(this.getEpmABM().getTable().getValueAt(indice, 3).toString());
			seleccion.add(this.getEpmABM().getTable().getValueAt(indice, 4).toString());
			seleccion.add(this.getEpmABM().getTable().getValueAt(indice, 5).toString());
			seleccion.add(this.getEpmABM().getTable().getValueAt(indice, 6).toString());
			seleccion.add(this.getEpmABM().getTable().getValueAt(indice, 7).toString());
			seleccion.add(this.getEpmABM().getTable().getValueAt(indice, 8).toString());
			cme.getMdU().completarCampos(seleccion);
			cme.getMdU().setLocationRelativeTo(null);
			cme.getMdU().setVisible(true);
		}

		if (e.getSource() == this.getEpmABM().getBtnBorrar()) {
			Integer indice = this.getEpmABM().getTable().getSelectedRow();
			String legajo = this.getEpmABM().getTable().getValueAt(indice, 0).toString();
			this.getEdi().borrarEmpleado(legajo);
			JOptionPane.showMessageDialog(this.getEpmABM(), "Borrado con exito", "Empleados",
					JOptionPane.INFORMATION_MESSAGE);
		}
		
		if (e.getSource() == this.getEpmABM().getBtnBuscar()) {
			String[] columnas = { "Legajo", "Nombre", "Apellido", "Dni", "Numero de Contacto", "Email", "Usuario",
					"Contraseņa", "Tipo de Empleado" };
			DefaultTableModel modelo = new DefaultTableModel(); // creo un nuevo modelo
			modelo.setColumnIdentifiers(columnas); // seteo las columnas del nuevo modelo
			ArrayList<Empleado> empleados;
			switch (this.getEpmABM().getCbxFiltro().getSelectedIndex()) {
			case 0:
				empleados = this.getEdi().buscarEmpleadosPorNombre(this.getEpmABM().getTxtBusqueda().getText());
				for (Empleado empleado : empleados) {
					Object[] row = new Object[9];
					row[0] = empleado.getLegajo();
					row[1] = empleado.getNombre();
					row[2] = empleado.getApellido();
					row[3] = empleado.getDni();
					row[4] = empleado.getNum_contacto();
					row[5] = empleado.getEmail();
					row[6] = empleado.getUsuario();
					row[7] = empleado.getContrasenia();
					row[8] = empleado.getTipo_empleado();
					modelo.addRow(row);
				}
				this.getEpmABM().getTable().setModel(modelo);
				break;
			case 1:
				empleados = this.getEdi().buscarEmpleadosPorApellido(this.getEpmABM().getTxtBusqueda().getText());
				for (Empleado empleado : empleados) {
					Object[] row = new Object[9];
					row[0] = empleado.getLegajo();
					row[1] = empleado.getNombre();
					row[2] = empleado.getApellido();
					row[3] = empleado.getDni();
					row[4] = empleado.getNum_contacto();
					row[5] = empleado.getEmail();
					row[6] = empleado.getUsuario();
					row[7] = empleado.getContrasenia();
					row[8] = empleado.getTipo_empleado();
					modelo.addRow(row);
				}
				this.getEpmABM().getTable().setModel(modelo);
				break;
			case 2:
				empleados = this.getEdi().buscarEmpleadosPorTipoEmpleado(this.getEpmABM().getTxtBusqueda().getText());
				for (Empleado empleado : empleados) {
					Object[] row = new Object[9];
					row[0] = empleado.getLegajo();
					row[1] = empleado.getNombre();
					row[2] = empleado.getApellido();
					row[3] = empleado.getDni();
					row[4] = empleado.getNum_contacto();
					row[5] = empleado.getEmail();
					row[6] = empleado.getUsuario();
					row[7] = empleado.getContrasenia();
					row[8] = empleado.getTipo_empleado();
					modelo.addRow(row);
				}
				this.getEpmABM().getTable().setModel(modelo);
				break;
			default:
				System.out.println("No entro a ningun lugar");
				break;
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	public EmpleadoABM getEpmABM() {
		return epmABM;
	}

	public void setEpmABM(EmpleadoABM epmABM) {
		this.epmABM = epmABM;

	}

	public EmpleadoDaoImp getEdi() {
		return edi;
	}

	public void setEdi(EmpleadoDaoImp edi) {
		this.edi = edi;
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		String[] columnas = { "Legajo", "Nombre", "Apellido", "Dni", "Numero de Contacto", "Email", "Usuario",
				"Contraseņa", "Tipo de Empleado" };
		DefaultTableModel modelo = new DefaultTableModel(); // creo un nuevo modelo
		modelo.setColumnIdentifiers(columnas); // seteo las columnas del nuevo modelo
		ArrayList<Empleado> empleados = this.getEdi().buscarEmpleados();
		for (Empleado empleado : empleados) {
			Object[] row = new Object[9];
			row[0] = empleado.getLegajo();
			row[1] = empleado.getNombre();
			row[2] = empleado.getApellido();
			row[3] = empleado.getDni();
			row[4] = empleado.getNum_contacto();
			row[5] = empleado.getEmail();
			row[6] = empleado.getUsuario();
			row[7] = empleado.getContrasenia();
			row[8] = empleado.getTipo_empleado();
			modelo.addRow(row);
		}
		this.getEpmABM().getTable().setModel(modelo);
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}
}
