package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import modelo.Comida;
import modelo.ComidaDaoImp;
import modelo.Mesa;
import modelo.Pedido;
import modelo.PedidoDaoImp;
import modelo.Pedido_Comidas;
import modelo.Venta;
import modelo.VentaDaoImp;
import vista.AltaPedido;

public class ControladorAltaPedido implements ActionListener, WindowListener {

	private ComidaDaoImp cdi;
	private PedidoDaoImp pdi;
	private VentaDaoImp vdi;
	private Mesa mesa;
	private Venta venta;
	private Integer num_pedido;
	private ArrayList<Pedido_Comidas> pedido_comidas;
	private AltaPedido ap;

	public ControladorAltaPedido(Mesa mesa, Venta venta, Integer num_pedido) {
		this.setCdi(new ComidaDaoImp());
		this.setPdi(new PedidoDaoImp());
		this.setVdi(new VentaDaoImp());
		this.setMesa(mesa);
		this.setVenta(venta);
		this.setNum_pedido(num_pedido);
		this.setPedido_comidas(new ArrayList<Pedido_Comidas>());
		this.setAp(new AltaPedido(this));
		this.getAp().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == this.getAp().getBtnBuscar()) {
			String[] columnasComida = { "Numero Platillo", "Nombre Platillo" };
			DefaultTableModel modeloComida = new DefaultTableModel();
			modeloComida.setColumnIdentifiers(columnasComida);
			ArrayList<Comida> comidas = this.getCdi().buscarComidaPorNombre(this.getAp().getTxtBusqueda().getText());
			for (Comida comida : comidas) {
				Object[] row = new Object[2];
				row[0] = comida.getNum_comida();
				row[1] = comida.getNombre_comida();
				modeloComida.addRow(row);
			}
			this.getAp().getTable_1().setModel(modeloComida);
		}

		if (e.getSource() == this.getAp().getBtnAniadir()) {
			Integer indice = this.getAp().getTable_1().getSelectedRow();
			String num_comida = this.getAp().getTable_1().getValueAt(indice, 0).toString();
			Pedido_Comidas pc = new Pedido_Comidas(this.getNum_pedido(), Integer.valueOf(num_comida),
					Integer.valueOf(this.getAp().getTextCantidad().getText()));
			this.getPedido_comidas().add(pc);
			String[] columnasPedido_Comidas = { "Numero Platillo", "Nombre Platillo", "Cantidad" };
			DefaultTableModel modeloComida_pedido = new DefaultTableModel();
			modeloComida_pedido.setColumnIdentifiers(columnasPedido_Comidas);
			for (Pedido_Comidas pedidos : this.getPedido_comidas()) {
				Comida comida = this.getCdi().buscarComidaPorNumero(String.valueOf(pedidos.getNum_comida()));
				Object[] row = new Object[3];
				row[0] = comida.getNum_comida();
				row[1] = comida.getNombre_comida();
				row[2] = pedidos.getCant_comida();
				modeloComida_pedido.addRow(row);
			}
			this.getAp().getTable().setModel(modeloComida_pedido);
		}

		if (e.getSource() == this.getAp().getBtnQuitar()) {
			this.getPedido_comidas().remove(this.getAp().getTable().getSelectedRow());
			String[] columnasPedido_Comidas = { "Numero Platillo", "Nombre Platillo", "Cantidad" };
			DefaultTableModel modeloComida_pedido = new DefaultTableModel();
			modeloComida_pedido.setColumnIdentifiers(columnasPedido_Comidas);
			for (Pedido_Comidas pedidos : this.getPedido_comidas()) {
				Comida comida = this.getCdi().buscarComidaPorNumero(String.valueOf(pedidos.getNum_comida()));
				Object[] row = new Object[3];
				row[0] = comida.getNum_comida();
				row[1] = comida.getNombre_comida();
				row[2] = pedidos.getCant_comida();
				modeloComida_pedido.addRow(row);
			}
			this.getAp().getTable().setModel(modeloComida_pedido);
		}

		if (e.getSource() == this.getAp().getBtnOpcion1()) {
			if (this.getPedido_comidas().size() > 0) {
				Double total_pedido = 0.00;
				for (Pedido_Comidas pedido_Comidas2 : this.getPedido_comidas()) {
					Comida comida = this.getCdi()
							.buscarComidaPorNumero(String.valueOf(pedido_Comidas2.getNum_comida()));
					total_pedido = total_pedido + comida.getPrecio_unitario() * pedido_Comidas2.getCant_comida();
				}
				Pedido pedido = new Pedido(num_pedido, this.getVenta().getNumero_Venta(), this.getMesa().getNumero(),
						this.getAp().getTxtpnIngreseSuDescripcion().getText(), false, false, total_pedido);
				
				this.getPdi().agregarPedidoComidas(pedido, this.getPedido_comidas());

			} else {
				JOptionPane.showMessageDialog(this.getAp(), "Agregue comidas al pedido", "Sistema",
						JOptionPane.ERROR_MESSAGE);
			}
			this.getAp().setVisible(false);
		}

		if (e.getSource() == this.getAp().getBtnOpcion2()) {
			this.getAp().setVisible(false);

		}
	}

	public ComidaDaoImp getCdi() {
		return cdi;
	}

	public void setCdi(ComidaDaoImp cdi) {
		this.cdi = cdi;
	}

	public PedidoDaoImp getPdi() {
		return pdi;
	}

	public void setPdi(PedidoDaoImp pdi) {
		this.pdi = pdi;
	}

	public VentaDaoImp getVdi() {
		return vdi;
	}

	public void setVdi(VentaDaoImp vdi) {
		this.vdi = vdi;
	}

	public Mesa getMesa() {
		return mesa;
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}

	public Venta getVenta() {
		return venta;
	}

	public void setVenta(Venta venta) {
		this.venta = venta;
	}

	public AltaPedido getAp() {
		return ap;
	}

	public void setAp(AltaPedido ap) {
		this.ap = ap;
	}

	public ArrayList<Pedido_Comidas> getPedido_comidas() {
		return pedido_comidas;
	}

	public void setPedido_comidas(ArrayList<Pedido_Comidas> pedido_comidas) {
		this.pedido_comidas = pedido_comidas;
	}

	public Integer getNum_pedido() {
		return num_pedido;
	}

	public void setNum_pedido(Integer num_pedido) {
		this.num_pedido = num_pedido;
	}

	@Override
	public void windowActivated(WindowEvent e) {
		String[] columnasComida = { "Numero Platillo", "Nombre Platillo" };
		DefaultTableModel modeloComida = new DefaultTableModel();
		modeloComida.setColumnIdentifiers(columnasComida);
		ArrayList<Comida> comidas = this.getCdi().buscarComidas();
		for (Comida comida : comidas) {
			Object[] row = new Object[2];
			row[0] = comida.getNum_comida();
			row[1] = comida.getNombre_comida();
			modeloComida.addRow(row);
		}
		String[] columnasPedido_Comidas = { "Numero Platillo", "Nombre Platillo", "Cantidad" };
		DefaultTableModel modeloComida_pedido = new DefaultTableModel();
		modeloComida_pedido.setColumnIdentifiers(columnasPedido_Comidas);
		for (Pedido_Comidas pedidos : this.getPedido_comidas()) {
			Comida comida = this.getCdi().buscarComidaPorNumero(String.valueOf(pedidos.getNum_comida()));
			Object[] row = new Object[3];
			row[0] = comida.getNum_comida();
			row[1] = comida.getNombre_comida();
			row[2] = pedidos.getCant_comida();
			modeloComida_pedido.addRow(row);
		}
		this.getAp().getTable_1().setModel(modeloComida);
		this.getAp().getTable().setModel(modeloComida_pedido);
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

}
