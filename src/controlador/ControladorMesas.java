package controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.JButton;
import modelo.Mesa;
import modelo.MesaDaoImp;
import vista.MesaAdmin;

public class ControladorMesas implements WindowListener, ActionListener { // VER LAS MESAS

	private MesaAdmin mesa;
	private MesaDaoImp mdi;

	public ControladorMesas() {

		this.setMesa(new MesaAdmin(this));
		this.getMesa().setVisible(true);
		this.setMdi(new MesaDaoImp());

	}

	@Override
	public void windowOpened(WindowEvent e) {

		// Consulta los estados de las mesas a la base de datos

	}

	@Override
	public void windowClosing(WindowEvent e) {

	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {

	}

	@Override
	public void windowDeiconified(WindowEvent e) {

	}

	@Override
	public void windowActivated(WindowEvent e) {

		// actualiza las mesas (algo asi como un update)

		
		}


	@Override
	public void windowDeactivated(WindowEvent e) {

	}

	public MesaAdmin getMesa() {
		return mesa;
	}

	public void setMesa(MesaAdmin mesa) {
		this.mesa = mesa;
	}

	public MesaDaoImp getMdi() {
		return mdi;
	}

	public void setMdi(MesaDaoImp mdi) {
		this.mdi = mdi;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ArrayList<Mesa> mesasBase = this.getMdi().buscarMesas();
		
		if (e.getSource() == this.getMesa().getCmbxDisponibilidad()) {
			if (this.getMesa().getCmbxDisponibilidad().getSelectedItem().equals("Todas")) {
				
				for (int i = 0; i < this.getMesa().getBotonesMesas().size(); i++) {
					
					this.getMesa().getBotonesMesas().get(i).setBackground(Color.WHITE);
				}
				
				 this.getMesa().getContentPane().revalidate();
			}

			if (this.getMesa().getCmbxDisponibilidad().getSelectedItem().equals("Solo Disponibles")) {

				Integer i = 0;

				for (JButton mesaBoton : this.getMesa().getBotonesMesas()) {

					if (mesasBase.get(i).getEstado()) {

						mesaBoton.setBackground(Color.GREEN);
						i++;
					} else {
						mesaBoton.setBackground(Color.WHITE);
						i++;
					}
					 this.getMesa().getContentPane().revalidate();
					
				}
			} else if (this.getMesa().getCmbxDisponibilidad().getSelectedItem().equals("Solo Ocupadas")) {

				Integer i = 0;

				for (JButton mesaBoton : this.getMesa().getBotonesMesas()) {

					if (!mesasBase.get(i).getEstado()) {

						mesaBoton.setBackground(Color.RED);
						i++;
					} else {
						mesaBoton.setBackground(Color.WHITE);
						i++;
					}
					 this.getMesa().getContentPane().revalidate();
				}
			}
			
		}
		}


		

}
