package controlador;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import vista.PedidosAdmin;

public class ControladorAdminPedidos implements WindowListener{ //CHECK DE PEDIDOS
	
	private PedidosAdmin vistaPedidos;
	
	public ControladorAdminPedidos () {
	
		this.setVistaPedidos(new PedidosAdmin(this));
		//this.getVistaPedidos().setVisible(true);
		
	}

	public PedidosAdmin getVistaPedidos() {
		return vistaPedidos;
	}

	public void setVistaPedidos(PedidosAdmin vistaPedidos) {
		this.vistaPedidos = vistaPedidos;
	}

	@Override
	public void windowOpened(WindowEvent e) {
		
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		
		
	}
	
	
	

}
