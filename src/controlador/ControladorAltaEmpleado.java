package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;

import modelo.Empleado;
import modelo.EmpleadoDaoImp;
import vista.AltaEmpleado;

public class ControladorAltaEmpleado implements ActionListener, KeyListener {

	private EmpleadoDaoImp empleado;
	private AltaEmpleado vistaN;

	public ControladorAltaEmpleado() {

		// setModeloN(new ModeloNuevoUsuario());
		setVistaN(new AltaEmpleado(this));
		setEmpleado(new EmpleadoDaoImp());
		getVistaN().setVisible(true);

		this.getVistaN().getRdbtnMesero().setSelected(true);
		this.getVistaN().getRdbtnCocinero().setSelected(false);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Empleado emp;

		String cadenaEmail = String.valueOf(this.getVistaN().getCmbEmail().getSelectedItem().toString());

		String legajo = this.getVistaN().getTxtLegajo().getText();

		String nombre = this.getVistaN().getTxtNombre().getText();

		String apellido = this.getVistaN().getTxtApellido().getText();

		String dni = this.getVistaN().getTxtDni().getText();

		String usuario = this.getVistaN().getTxtUsuario().getText();

		String contrasenia = this.getVistaN().getTxtContrasenia().getText();

		String correoElectronico = this.getVistaN().getTxtEmail().getText() + cadenaEmail;

		String numeroContacto = this.getVistaN().getTxtNcontacto().getText();

		Boolean validacion = (!legajo.equals("") && !nombre.equals("") && !apellido.equals("") && !dni.equals("")
				&& !usuario.equals("") && !contrasenia.equals("") && !correoElectronico.equals("")
				&& !numeroContacto.equals(""));

		System.out.println(validacion);

		if (e.getSource() == this.getVistaN().getBtnGuardarUsuario()) {

			if (this.getVistaN().getRdbtnMesero().isSelected()) {
				if (validacion) {
					emp = new Empleado(Integer.valueOf(legajo), nombre, apellido, dni, numeroContacto, correoElectronico, usuario,
							contrasenia, "Mesero");

					Boolean ingreso = this.getEmpleado().agregarEmpleado(emp);
				     if (ingreso) {
						JOptionPane.showMessageDialog(this.getVistaN(), "Empleado agregado correctamente", "Sistema",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(this.getVistaN(), "Problema al ingresar el empleado", "Sistema",
								JOptionPane.ERROR_MESSAGE);
					}

				} else {
					JOptionPane.showMessageDialog(this.getVistaN(), "Complete todos los campos", "Sistema",
							JOptionPane.ERROR_MESSAGE);
				}
			} else if (this.getVistaN().getRdbtnCocinero().isSelected()) {
				if (validacion) {
					emp = new Empleado(Integer.valueOf(legajo), nombre, apellido, dni, numeroContacto, correoElectronico, usuario,
							contrasenia, "Cocinero");
					if (this.getEmpleado().agregarEmpleado(emp)) {
						JOptionPane.showMessageDialog(this.getVistaN(), "Empleado agregado correctamente", "Sistema",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(this.getVistaN(), "Problema al ingresar el empleado", "Sistema",
								JOptionPane.ERROR_MESSAGE);
					}

				} else {

					JOptionPane.showMessageDialog(this.getVistaN(), "Complete todos los campos", "Sistema",
							JOptionPane.ERROR_MESSAGE);
				}
			}

		}
		if (e.getSource() == this.getVistaN().getBtnCancelar()) {
			this.getVistaN().setVisible(false);
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {

		
		/*char c = e.getKeyChar();
		if ((Character.isLetter(c)) || (this.getVistaN().getTxtLegajo().getText().length() > 0)) {
			this.getVistaN().getToolkit().beep();
			e.consume();
			
		}
		
		if ((Character.isLetter(c))|| (this.getVistaN().getTxtDni().getText().length() >7) ) {
			this.getVistaN().getToolkit().beep();
			e.consume();
		}
		*/
		
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	public EmpleadoDaoImp getEmpleado() {
		return empleado;
	}

	public void setEmpleado(EmpleadoDaoImp empleado) {
		this.empleado = empleado;
	}

	public AltaEmpleado getVistaN() {
		return vistaN;
	}

	public void setVistaN(AltaEmpleado vistaN) {
		this.vistaN = vistaN;
	}

}
