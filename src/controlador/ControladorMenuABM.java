package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import modelo.Comida;
import modelo.ComidaDaoImp;
import vista.ABMMenu;


public class ControladorMenuABM implements ActionListener, WindowListener{

	private ABMMenu menu;
	private ComidaDaoImp cdi;
	
	public ControladorMenuABM() {

		this.setMenu(new ABMMenu(this));
		this.setCdi(new ComidaDaoImp());
	}


	@Override
	public void actionPerformed(ActionEvent e) {
	
		if (e.getSource() == this.getMenu().getBtnBuscar()) {
			String[] columnas = { "Numero", "Nombre", "Descripcion", "Es platillo?", "Precio Unitario" };
			DefaultTableModel modelo = new DefaultTableModel(); // creo un nuevo modelo
			modelo.setColumnIdentifiers(columnas); // seteo las columnas del nuevo modelo
			ArrayList<Comida> comidas = this.getCdi().buscarComidas();;
			switch (this.getMenu().getComboBox().getSelectedIndex()) {
			case 0:
				comidas.clear();
				Comida comida = this.getCdi().buscarComidaPorNumero(this.getMenu().getTxtBusqueda().getText());
				comidas.add(comida);
				break;
			case 1:
				comidas = this.getCdi().buscarComidaPorNombre(this.getMenu().getTxtBusqueda().getText());
				break;
			case 2:
				comidas = this.getCdi().buscarEs_Platillo();
				break;
			case 3:
				comidas = this.getCdi().buscarEs_Bebida();
				break;
			}
			for (Comida comida : comidas) {
				Object[] row = new Object[5];
				row[0] = comida.getNum_comida();
				row[1] = comida.getNombre_comida();
				row[2] = comida.getDescripcion_comida();
				row[3] = comida.getEs_platillo();
				row[4] = comida.getPrecio_unitario();
				modelo.addRow(row);
			}
			this.getMenu().getTable().setModel(modelo);
			
		}
		
		if (e.getSource() == this.getMenu().getBtnAltaPlatillo()) {
			
			this.getMenu().setAlwaysOnTop(false);
			new ControladorAltaMenu();
			
			
		}
		
		if (e.getSource()== this.getMenu().getBtnModificarPlatillo()) {
			
			this.getMenu().setAlwaysOnTop(false);
			Integer indice = this.getMenu().getTable().getSelectedRow();
			ArrayList<String> seleccion = new ArrayList<String>();
			seleccion.add(this.getMenu().getTable().getValueAt(indice, 0).toString());
			seleccion.add(this.getMenu().getTable().getValueAt(indice, 1).toString());
			seleccion.add(this.getMenu().getTable().getValueAt(indice, 2).toString());
			seleccion.add(this.getMenu().getTable().getValueAt(indice, 3).toString());
			seleccion.add(this.getMenu().getTable().getValueAt(indice, 4).toString());
			ControladorModificarMenu cmu = new ControladorModificarMenu();
			cmu.getMenu().completarCampos(seleccion);
			cmu.getMenu().setVisible(true);
		}
		
		if (e.getSource() == this.getMenu().getBtnBorrarPlatillo()) {
			
			Integer indice = this.getMenu().getTable().getSelectedRow();
			String num_comida = this.getMenu().getTable().getValueAt(indice, 0).toString();
			if (this.getCdi().borrarComida(num_comida)) {
				JOptionPane.showMessageDialog(this.getMenu(), "Borrado con exito", "Comidas",
						JOptionPane.INFORMATION_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(this.getMenu(), "Error al borrar", "Comidas",
						JOptionPane.INFORMATION_MESSAGE);
			}
		}
		
	}
	
	public ABMMenu getMenu() {
		return menu;
	}

	public void setMenu(ABMMenu menu) {
		this.menu = menu;
	}


	@Override
	public void windowOpened(WindowEvent e) {
		
		
	}


	@Override
	public void windowClosing(WindowEvent e) {
		
		
	}


	@Override
	public void windowClosed(WindowEvent e) {
		
	}


	@Override
	public void windowIconified(WindowEvent e) {
		
		
	}


	@Override
	public void windowDeiconified(WindowEvent e) {
		
		
	}


	@Override
	public void windowActivated(WindowEvent e) {
		
		String[] columnas = { "Numero", "Nombre", "Descripcion", "Es platillo?", "Precio Unitario" };
		DefaultTableModel modelo = new DefaultTableModel(); // creo un nuevo modelo
		modelo.setColumnIdentifiers(columnas); // seteo las columnas del nuevo modelo
		ArrayList<Comida> comidas = this.getCdi().buscarComidas();
		for (Comida comida : comidas) {
			Object[] row = new Object[5];
			row[0] = comida.getNum_comida();
			row[1] = comida.getNombre_comida();
			row[2] = comida.getDescripcion_comida();
			row[3] = comida.getEs_platillo();
			row[4] = comida.getPrecio_unitario();
			modelo.addRow(row);
		}
		this.getMenu().getTable().setModel(modelo);
	}


	@Override
	public void windowDeactivated(WindowEvent e) {
		
		
	}


	public ComidaDaoImp getCdi() {
		return cdi;
	}


	public void setCdi(ComidaDaoImp cdi) {
		this.cdi = cdi;
	}




}