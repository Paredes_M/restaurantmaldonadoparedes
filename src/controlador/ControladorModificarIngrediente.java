package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import modelo.Ingrediente;
import modelo.IngredienteDaoImp;
import vista.ModificarIngrediente;

public class ControladorModificarIngrediente implements ActionListener {

	private ModificarIngrediente ing;
	private IngredienteDaoImp idi;

	public ControladorModificarIngrediente() {
		this.setIng(new ModificarIngrediente(this));
		this.setIdi(new IngredienteDaoImp());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Ingrediente ingr;
		String numero_ingrediente = String.valueOf(this.getIng().getTxtNumeroIngrediente().getText());
		String nombre_ingrediente = String.valueOf(this.getIng().getTxtNombreIngrediente().getText());
		String descripcion_ingrediente = String.valueOf(this.getIng().getTextDescripcion().getText());
		String cantidad_ingrediente = String.valueOf(this.getIng().getTxtCantidad().getText());
		String stock_ingrediente = String.valueOf(this.getIng().getTxtStock().getText());

		Boolean validacion = (!numero_ingrediente.equals("") && !nombre_ingrediente.equals("")
				&& !descripcion_ingrediente.equals("") && !cantidad_ingrediente.equals("")
				&& !stock_ingrediente.equals(""));
		if (e.getSource() == this.getIng().getBtnAceptar()) {
			ingr = new Ingrediente(Integer.valueOf(numero_ingrediente), nombre_ingrediente, null, null,
					descripcion_ingrediente, Integer.valueOf(cantidad_ingrediente), Integer.valueOf(stock_ingrediente));
			if (validacion) {
				if (this.getIng().getRdbtnEsBebidaSi().isSelected()) {
					ingr.setEs_bebida(true);
				} else {
					ingr.setEs_bebida(false);
				}
				if (this.getIng().getRdbtnEsEnvasadoSI().isSelected()) {
					ingr.setEs_envasado(true);
				} else {
					ingr.setEs_envasado(false);
				}
				if (this.getIdi().modificarIngrediente(ingr)) {
					JOptionPane.showMessageDialog(this.getIng(), "Modificado con exito", "Ingredientes",
							JOptionPane.INFORMATION_MESSAGE);
					this.getIng().setVisible(false);
				} else {
					JOptionPane.showMessageDialog(this.getIng(), "Error al Modificar", "Ingredientes",
							JOptionPane.ERROR_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(this.getIng(), "Complete todos los campos", "Sistema",
						JOptionPane.ERROR_MESSAGE);
			}
		}
		if (e.getSource() == this.getIng().getBtnCancelar()) {
			this.getIng().setVisible(false);
		}

	}

	public ModificarIngrediente getIng() {
		return ing;
	}

	public void setIng(ModificarIngrediente ing) {
		this.ing = ing;
	}

	public IngredienteDaoImp getIdi() {
		return idi;
	}

	public void setIdi(IngredienteDaoImp idi) {
		this.idi = idi;
	}

}
