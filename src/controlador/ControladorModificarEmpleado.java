package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import modelo.Empleado;
import modelo.EmpleadoDaoImp;
import vista.ModificarEmpleado;

public class ControladorModificarEmpleado implements ActionListener {

	private EmpleadoDaoImp empDImp;
	private ModificarEmpleado mdU;

	public ControladorModificarEmpleado() {

		setEmpDImp(new EmpleadoDaoImp());
		this.setMdU(new ModificarEmpleado(this));
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		String cadenaEmail = String.valueOf(this.getMdU().getCmbEmail().getSelectedObjects());

		String legajo = this.getMdU().getTxtLegajo().getText();

		String nombre = this.getMdU().getTxtNombre().getText();

		String apellido = this.getMdU().getTxtApellido().getText();

		String dni = this.getMdU().getTxtDni().getText();

		String usuario = this.getMdU().getTxtUsuario().getText();

		String contrasenia = this.getMdU().getTxtContrasenia().getText();

		String correoElectronico = this.getMdU().getTxtEmail().getText() + cadenaEmail;

		String numeroContacto = this.getMdU().getTxtNcontacto().getText();

		Boolean validacion = (!legajo.equals("") && !nombre.equals("") && !apellido.equals("") && !dni.equals("")
				&& !usuario.equals("") && !contrasenia.equals("") && !correoElectronico.equals("")
				&& !numeroContacto.equals(""));

		if (e.getSource() == this.getMdU().getBtnGuardarUsuario()) {
			Empleado emp = new Empleado(Integer.valueOf(legajo), nombre, apellido, dni, numeroContacto, correoElectronico, usuario,
					contrasenia, "Mesero");
			if (this.getMdU().getRdbtnMesero().isSelected()) {
				if (validacion) {
					emp.setTipo_empleado("Mesero");
					if (this.getEmpDImp().modificarEmpleado(emp)) {
						JOptionPane.showMessageDialog(this.getMdU(), "Empleado modificado correctamente", "Sistema",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(this.getMdU(), "Problema al modificar el empleado", "Sistema",
								JOptionPane.ERROR_MESSAGE);
					}

				} else {
					JOptionPane.showMessageDialog(this.getMdU(), "Complete todos los campos", "Sistema",
							JOptionPane.ERROR_MESSAGE);
				}
			} else if (this.getMdU().getRdbtnCocinero().isSelected()) {
				if (validacion) {
					emp.setTipo_empleado("Cocinero");
					if (this.getEmpDImp().modificarEmpleado(emp)) {
						JOptionPane.showMessageDialog(this.getMdU(), "Empleado modificado correctamente", "Sistema",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(this.getMdU(), "Problema al modificar el empleado", "Sistema",
								JOptionPane.ERROR_MESSAGE);
					}

				} else {

					JOptionPane.showMessageDialog(this.getMdU(), "Complete todos los campos", "Sistema",
							JOptionPane.ERROR_MESSAGE);
				}

			}

		}
		if (e.getSource() == this.getMdU().getBtnCancelar()) {
			this.getMdU().setVisible(false);
		}

	}

	public ModificarEmpleado getMdU() {
		return mdU;
	}

	public void setMdU(ModificarEmpleado mdU) {
		this.mdU = mdU;
	}

	public EmpleadoDaoImp getEmpDImp() {
		return empDImp;
	}

	public void setEmpDImp(EmpleadoDaoImp empDImp) {
		this.empDImp = empDImp;
	}

}
