package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import modelo.Comida;
import vista.AltaMenu;

public class ControladorAltaMenu implements ActionListener {

	private AltaMenu menu;

	public ControladorAltaMenu() {

		this.setMenu(new AltaMenu(this));
		this.getMenu().setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == this.getMenu().getBtnSiguiente()) {
			
			String num_comida = this.getMenu().getTxtNumeroComida().getText();
			String nombre_comida = this.getMenu().getTxtNombreComida().getText();
			String descripcion_comida = this.getMenu().getTextDescripcion().getText();
			String es_platillo = "";
			if (this.getMenu().getRdbtnPlatilloSi().isSelected()) {
				es_platillo = "true";
			}
			if (this.getMenu().getRdbtnPlatilloNo().isSelected()) {
				es_platillo = "false";
			}
			String precio_unitario = this.getMenu().getTxtPrecioUnitario().getText();
			Boolean validacion = (!num_comida.equals("") && !nombre_comida.equals("") && !descripcion_comida.equals("") && !es_platillo.equals("")
					&& !precio_unitario.equals(""));
			
			if (e.getSource() == this.getMenu().getBtnSiguiente()) {
				if (validacion) {
					Comida comida = new Comida(Integer.valueOf(num_comida), nombre_comida, descripcion_comida, Boolean.valueOf(es_platillo), Double.valueOf(precio_unitario));
					this.getMenu().setVisible(false);
					new ControladorAltaMenuDos(comida);
				} else {
					JOptionPane.showMessageDialog(this.getMenu(), "Complete todos los campos", "Sistema",
							JOptionPane.ERROR_MESSAGE);
				}

		}

		if (e.getSource() == this.getMenu().getBtnCancelar()) {

			int n = JOptionPane.showConfirmDialog(this.getMenu(), "�Deseas cancelar? ", "Cancelar Operacion ",
					JOptionPane.YES_NO_OPTION);

			if (n == JOptionPane.YES_OPTION) {

				this.getMenu().setVisible(false);
			}
		}
		}
	}

	public AltaMenu getMenu() {
		return menu;
	}

	public void setMenu(AltaMenu menu) {
		this.menu = menu;
	}

}
