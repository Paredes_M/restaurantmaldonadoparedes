package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import modelo.Ingrediente;
import modelo.IngredienteDaoImp;
import vista.IngredientesABM;

public class ControladorIngredienteABM implements ActionListener, WindowListener {

	private IngredientesABM ingABM;
	private IngredienteDaoImp idi;

	public ControladorIngredienteABM() {

		this.setIngABM(new IngredientesABM(this));
		this.setIdi(new IngredienteDaoImp());
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == this.getIngABM().getBtnAgregar()) {

			new ControladorAltaIngrediente();
		}

		if (e.getSource() == this.getIngABM().getBtnModificar()) {
			this.getIngABM().setModal(false);
			ControladorModificarIngrediente cme = new ControladorModificarIngrediente();
			Integer indice = this.getIngABM().getTable().getSelectedRow();
			ArrayList<String> seleccion = new ArrayList<String>();
			seleccion.add(this.getIngABM().getTable().getValueAt(indice, 0).toString());
			seleccion.add(this.getIngABM().getTable().getValueAt(indice, 1).toString());
			seleccion.add(this.getIngABM().getTable().getValueAt(indice, 2).toString());
			seleccion.add(this.getIngABM().getTable().getValueAt(indice, 3).toString());
			seleccion.add(this.getIngABM().getTable().getValueAt(indice, 4).toString());
			seleccion.add(this.getIngABM().getTable().getValueAt(indice, 5).toString());
			seleccion.add(this.getIngABM().getTable().getValueAt(indice, 6).toString());
			cme.getIng().completarCampos(seleccion);
			cme.getIng().setVisible(true);
		}

		if (e.getSource() == this.getIngABM().getBtnBorrar()) {
			Integer indice = this.getIngABM().getTable().getSelectedRow();
			String num_ingrediente = this.getIngABM().getTable().getValueAt(indice, 0).toString();
			if (this.getIdi().borrarIngrediente(num_ingrediente)) {
				JOptionPane.showMessageDialog(this.getIngABM(), "Borrado con exito", "Ingredientes",
						JOptionPane.INFORMATION_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(this.getIngABM(), "Error al borrar", "Ingredientes",
						JOptionPane.ERROR_MESSAGE);
			}
		}

		if (e.getSource() == this.getIngABM().getBtnBuscar()) {
			String[] columnas = { "Numero", "Nombre", "Es envasado?", "Es bebida?", "Descripcion", "Cantidad actual",
					"Stock Minimo" };
			DefaultTableModel modelo = new DefaultTableModel(); // creo un nuevo modelo
			modelo.setColumnIdentifiers(columnas); // seteo las columnas del nuevo modelo
			ArrayList<Ingrediente> ingredientes = this.getIdi().buscarIngredientes();
			switch (this.getIngABM().getCmbxFiltroBusqueda().getSelectedIndex()) {
			case 0:
				Ingrediente ingr = this.getIdi()
						.buscarIngredientePorNumero(this.getIngABM().getTxtBusquedaIng().getText());
				if (ingr != null) {
					ingredientes.clear();
					ingredientes.add(ingr);
				}
				break;
			case 1:
				ingredientes = this.getIdi().IngredienteporNombre(this.getIngABM().getTxtBusquedaIng().getText());
				break;
			case 2:
				ingredientes = this.getIdi().IngredienteNoEnvasado();
				break;
			case 3:
				ingredientes = this.getIdi().IngredienteEnvasado();
				break;
			case 4:
				ingredientes = this.getIdi().IngredienteNoBebida();
				break;
			case 5:
				ingredientes = this.getIdi().IngredienteBebida();
				break;
			case 6:
				ingredientes = this.getIdi().IngredienteSinStock();
				break;
			}
			for (Ingrediente ingrediente : ingredientes) {
				Object[] row = new Object[7];
				row[0] = ingrediente.getNum_ingrediente();
				row[1] = ingrediente.getNombre_ingrediente();
				row[2] = ingrediente.getEs_envasado();
				row[3] = ingrediente.getEs_bebida();
				row[4] = ingrediente.getDescripcion_ingrediente();
				row[5] = ingrediente.getCant_ingrediente();
				row[6] = ingrediente.getStock_ingrediente();
				modelo.addRow(row);
			}
			this.getIngABM().getTable().setModel(modelo);
		}
	}

	@Override
	public void windowOpened(WindowEvent e) {

	}

	@Override
	public void windowClosing(WindowEvent e) {

	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {

	}

	@Override
	public void windowDeiconified(WindowEvent e) {

	}

	@Override
	public void windowActivated(WindowEvent e) {

		String[] columnas = { "Numero", "Nombre", "Es envasado?", "Es bebida?", "Descripcion", "Cantidad actual",
				"Stock Minimo" };
		DefaultTableModel modelo = new DefaultTableModel(); // creo un nuevo modelo
		modelo.setColumnIdentifiers(columnas); // seteo las columnas del nuevo modelo
		ArrayList<Ingrediente> ingredientes = this.getIdi().buscarIngredientes();
		for (Ingrediente ingrediente : ingredientes) {
			Object[] row = new Object[7];
			row[0] = ingrediente.getNum_ingrediente();
			row[1] = ingrediente.getNombre_ingrediente();
			row[2] = ingrediente.getEs_envasado();
			row[3] = ingrediente.getEs_bebida();
			row[4] = ingrediente.getDescripcion_ingrediente();
			row[5] = ingrediente.getCant_ingrediente();
			row[6] = ingrediente.getStock_ingrediente();
			modelo.addRow(row);
		}
		this.getIngABM().getTable().setModel(modelo);

	}

	@Override
	public void windowDeactivated(WindowEvent e) {

	}

	public IngredientesABM getIngABM() {
		return ingABM;
	}

	public void setIngABM(IngredientesABM ingABM) {
		this.ingABM = ingABM;
	}

	public IngredienteDaoImp getIdi() {
		return idi;
	}

	public void setIdi(IngredienteDaoImp idi) {
		this.idi = idi;
	}
}
