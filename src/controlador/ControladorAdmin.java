package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import modelo.Venta;
import modelo.VentaDaoImp;
import vista.VistaAdmin;

public class ControladorAdmin implements ActionListener, WindowListener {

	private VistaAdmin vista;
	private VentaDaoImp vdi;

	public ControladorAdmin() {
		this.setVista(new VistaAdmin(this));
		this.setVdi(new VentaDaoImp());
		this.getVista().setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == (this.getVista().getMntmEmpleado())) {

			new ControladorEmpleadoABM();

		}
		
		if (e.getSource() == this.getVista().getMntmTodasLasMesas()) {
			
			
			new ControladorMesas();
			
		}
		
		if (e.getSource() == this.getVista().getMntmPedidos()) {
			
			 ControladorAdminPedidos controlador = new ControladorAdminPedidos();
			 controlador.getVistaPedidos().setVisible(true);
			
		}
		
		if (e.getSource()== this.getVista().getMntmAlmacen()) {
			
			ControladorIngredienteABM controlador = new ControladorIngredienteABM();
			controlador.getIngABM().setVisible(true);
		}
		
		if (e.getSource() == this.getVista().getMntmMenu()) {

			ControladorMenuABM controlador = new ControladorMenuABM(); // ABRE EL MENU DANDO LA POSIBILIDAD DE HACER UN
																		// ABM DE PLATILLOS (solo el y el cocinero
																		// pueden hacer ABM)
			controlador.getMenu().getBtnAltaPlatillo().setEnabled(true);
			controlador.getMenu().getBtnModificarPlatillo().setEnabled(true);
			controlador.getMenu().getBtnBorrarPlatillo().setEnabled(true);
			controlador.getMenu().setVisible(true);
		}

		if (e.getSource().equals(this.getVista().getMntmSalir())) {

			int n = JOptionPane.showConfirmDialog(this.getVista(),
					"�Deseas Cerrar Sesion?", "Salir ", JOptionPane.YES_NO_OPTION);

			if (n == JOptionPane.YES_OPTION) {
				ControladorLogin cont = new ControladorLogin();
				cont.getVista().setVisible(true);
				this.getVista().setVisible(false);

			}

		}

	}

	@Override
	public void windowActivated(WindowEvent e) {
		String[] columnas = { "Numero de Venta", "Legajo", "Total a Pagar", "Fecha", "Hora" };
		DefaultTableModel modelo = new DefaultTableModel(); // creo un nuevo modelo
		modelo.setColumnIdentifiers(columnas); // seteo las columnas del nuevo modelo
		ArrayList<Venta> ventas = this.getVdi().buscarVentas();
		for (Venta venta : ventas) {
			Object[] row = new Object[5];
			row[0] = venta.getNumero_Venta();
			row[1] = venta.getLegajo_Empleado();
			row[2] = venta.getTotalApagar();
			row[3] = venta.getFechaVenta();
			row[4] = venta.getHoraVenta();
			modelo.addRow(row);
		}
		this.getVista().getTable().setModel(modelo);
	}

	@Override
	public void windowClosed(WindowEvent e) {
		

	}

	@Override
	public void windowClosing(WindowEvent e) {
		

	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		

	}

	@Override
	public void windowIconified(WindowEvent e) {
		

	}

	@Override
	public void windowOpened(WindowEvent e) {
		

	}

	public VentaDaoImp getVdi() {
		return vdi;
	}

	public void setVdi(VentaDaoImp vdi) {
		this.vdi = vdi;
	}


	public VistaAdmin getVista() {
		return vista;
	}

	public void setVista(VistaAdmin vista) {
		this.vista = vista;
	}

}
