package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import vista.VistaCocina;

public class ControladorCocina implements ActionListener {
	
	private VistaCocina vista;
	
	
	public ControladorCocina () {
		
		this.setVista(new VistaCocina(this));
		
		this.getVista().setVisible(true);
	}
	
	

	@Override
	public void actionPerformed(ActionEvent e) {
		


		if (e.getSource() == this.getVista().getMntmAlmacen()) {
			
			ControladorIngredienteABM controlador = new ControladorIngredienteABM();
			controlador.getIngABM().setVisible(true);
		}	
		
		if (e.getSource()== this.getVista().getMntmCocinaMenu()) {
			
			ControladorMenuABM controlador = new ControladorMenuABM(); //Suponemos que el cocinero tiene acceso a el menu
		    controlador.getMenu().setVisible(true);
			
		}
		
		
		if (e.getSource() == this.getVista().getMntmCocinaSalir()) {

			

			int n = JOptionPane.showConfirmDialog(this.getVista(), "�Deseas Cerrar Sesion? ", "Salir ",
					JOptionPane.YES_NO_OPTION);

			if (n == JOptionPane.YES_OPTION) {
				ControladorLogin cont = new ControladorLogin();
				cont.getVista().setVisible(true);
				this.getVista().setVisible(false);

			}


		}
		
	}
	
	

	public VistaCocina getVista() {
		return vista;
	}

	public void setVista(VistaCocina vista) {
		this.vista = vista;
	}




}
