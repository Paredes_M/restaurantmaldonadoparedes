package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import modelo.Mesa;
import modelo.MesaDaoImp;
import modelo.Pedido;
import modelo.PedidoDaoImp;
import modelo.Venta;
import modelo.VentaDaoImp;
import vista.PedidoABM;

public class ControladorPedido implements ActionListener, WindowListener {

	private PedidoABM abm;
	private VentaDaoImp vdi;
	private PedidoDaoImp pdi;
	private MesaDaoImp mdi;
	private Mesa mesa;
	private Venta venta;

	// HACE ABM PEDIDO
	public ControladorPedido(Mesa mesa, Venta venta) {
		super();
		this.setVdi(new VentaDaoImp());
		this.setPdi(new PedidoDaoImp());
		this.setMdi(new MesaDaoImp());
		this.setMesa(mesa);
		this.setVenta(venta);
		this.setAbm(new PedidoABM(this));
		this.getAbm().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == this.getAbm().getBtnAgregar()) {

			new ControladorAltaPedido(this.getMesa(), this.getVenta(), this.getPdi().numPedidoNuevo());
		}

		if (e.getSource() == this.getAbm().getBtnModificar()) {
			Integer indice = this.getAbm().getTable().getSelectedRow();
			String num_pedido = this.getAbm().getTable().getValueAt(indice, 0).toString();
			new ControladorModificarPedido(this.getMesa(),this.getVenta(),Integer.valueOf(num_pedido));
		}

		if (e.getSource() == this.getAbm().getBtnBorrar()) {
			Integer indice = this.getAbm().getTable().getSelectedRow();
			String num_pedido = this.getAbm().getTable().getValueAt(indice, 0).toString();
			if (this.getPdi().borrarPedido(num_pedido)) {
				JOptionPane.showMessageDialog(this.getAbm(), "Borrado con exito", "Pedido",
						JOptionPane.INFORMATION_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(this.getAbm(), "Error al borrar", "Pedido",
						JOptionPane.ERROR_MESSAGE);
			}
		}

		if (e.getSource() == this.getAbm().getChckbxOcuparMesa()) {
			System.out.println("cambio el checkbox");
			if (this.getAbm().getChckbxOcuparMesa().isSelected()) {
				this.getMesa().setEstado(false);
			} else {
				this.getMesa().setEstado(true);
			}
			if (this.getMdi().modificarMesa(this.getMesa())) {
				System.out.println("se modifico");
			}
		}

	}

	@Override
	public void windowOpened(WindowEvent e) {
		String[] columnas = { "Numero Pedido", "Numero Mesa", "Descripcion Pedido", "Terminado", "Entregado",
				"Precio" };
		DefaultTableModel modelo = new DefaultTableModel();
		modelo.setColumnIdentifiers(columnas);
		ArrayList<Pedido> pedidos = this.getPdi().buscarPedidosMesa(String.valueOf(this.getMesa().getNumero()),
				String.valueOf(this.getVenta().getNumero_Venta()));
		for (Pedido pedido : pedidos) {
			Object[] row = new Object[6];
			row[0] = pedido.getNum_pedido();
			row[1] = pedido.getNum_mesa();
			row[2] = pedido.getDescripcion_pedido();
			row[3] = pedido.getTerminado_pedido();
			row[4] = pedido.getEntregado_mesa();
			row[5] = pedido.getPrecio_pedido();
			modelo.addRow(row);
		}
		this.getAbm().getTable().setModel(modelo);
		if (pedidos.size() > 0) {
			this.getAbm().getBtnAgregarVenta().setEnabled(true);
			Double total_venta = 0.00; 
			for (Pedido pedido : pedidos) {
				total_venta = total_venta + pedido.getPrecio_pedido();
			}
			this.getAbm().getTxtTotalAPagar().setText(String.valueOf(total_venta));
		} else {
			this.getAbm().getBtnAgregarVenta().setEnabled(false);
			this.getAbm().getTxtTotalAPagar().setText("0.00");
		}
		this.getVdi().agregarventa(this.getVenta()); // ACA SE AGREGA LA VENTA A LA BASE DE DATOS.
	}

	@Override
	public void windowClosing(WindowEvent e) {

	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {

	}

	@Override
	public void windowDeiconified(WindowEvent e) {

	}

	@Override
	public void windowActivated(WindowEvent e) {
		String[] columnas = { "Numero Pedido", "Numero Mesa", "Descripcion Pedido", "Terminado", "Entregado",
				"Precio" };
		DefaultTableModel modelo = new DefaultTableModel();
		modelo.setColumnIdentifiers(columnas);
		ArrayList<Pedido> pedidos = this.getPdi().buscarPedidosMesa(String.valueOf(this.getMesa().getNumero()),
				String.valueOf(this.getVenta().getNumero_Venta()));
		for (Pedido pedido : pedidos) {
			Object[] row = new Object[6];
			row[0] = pedido.getNum_pedido();
			row[1] = pedido.getNum_mesa();
			row[2] = pedido.getDescripcion_pedido();
			row[3] = pedido.getTerminado_pedido();
			row[4] = pedido.getEntregado_mesa();
			row[5] = pedido.getPrecio_pedido();
			modelo.addRow(row);
		}
		this.getAbm().getTable().setModel(modelo);
		if (pedidos.size() > 0) {
			this.getAbm().getBtnAgregarVenta().setEnabled(true);
			Double total_venta = 0.00; 
			for (Pedido pedido : pedidos) {
				total_venta = total_venta + pedido.getPrecio_pedido();
			}
			this.getAbm().getTxtTotalAPagar().setText(String.valueOf(total_venta));
		} else {
			this.getAbm().getBtnAgregarVenta().setEnabled(false);
			this.getAbm().getTxtTotalAPagar().setText("0.00");
		}
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {

	}

	public PedidoABM getAbm() {
		return abm;
	}

	public void setAbm(PedidoABM abm) {
		this.abm = abm;
	}

	public Mesa getMesa() {
		return mesa;
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}

	public PedidoDaoImp getPdi() {
		return pdi;
	}

	public void setPdi(PedidoDaoImp pdi) {
		this.pdi = pdi;
	}

	public Venta getVenta() {
		return venta;
	}

	public void setVenta(Venta venta) {
		this.venta = venta;
	}

	public VentaDaoImp getVdi() {
		return vdi;
	}

	public void setVdi(VentaDaoImp vdi) {
		this.vdi = vdi;
	}

	public MesaDaoImp getMdi() {
		return mdi;
	}

	public void setMdi(MesaDaoImp mdi) {
		this.mdi = mdi;
	}
	

}
