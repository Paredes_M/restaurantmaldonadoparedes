package vista;

import java.awt.Color;
import java.awt.Font;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import controlador.ControladorIngredienteABM;

public class IngredientesABM extends ABSABM {

	private ControladorIngredienteABM cia;
	private JTextField txtBusquedaIng;
	private JComboBox cmbxFiltroBusqueda;
	private JButton btnBuscar;

	public IngredientesABM(ControladorIngredienteABM controlador) {
		getBtnBorrar().setBackground(Color.WHITE);
		getBtnModificar().setBackground(Color.WHITE);
		getBtnAgregar().setBackground(Color.WHITE);
		this.setCia(controlador);
		addWindowListener(controlador);
		getScrollPane().setLocation(10, 50);
		getBtnAgregar().setBounds(33, 279, 161, 23);
		getBtnModificar().setSize(161, 23);
		getBtnModificar().setLocation(221, 279);
		getBtnBorrar().setLocation(411, 279);
		super.setSize(600, 380);

		txtBusquedaIng = new JTextField();
		txtBusquedaIng.setBounds(221, 11, 256, 20);
		getContentPane().add(txtBusquedaIng);
		txtBusquedaIng.setColumns(10);

		cmbxFiltroBusqueda = new JComboBox();
		cmbxFiltroBusqueda.setFont(new Font("Tahoma", Font.PLAIN, 11));
		cmbxFiltroBusqueda.setForeground(Color.BLACK);
		cmbxFiltroBusqueda.setBackground(Color.WHITE);
		cmbxFiltroBusqueda.setModel(new DefaultComboBoxModel(new String[] {"Numero", "Nombre", "No Envasados", "Si Envasados", "No Bebidas", "Si Bebidas", "Sin Stock"}));
		cmbxFiltroBusqueda.setBounds(92, 11, 109, 20);
		getContentPane().add(cmbxFiltroBusqueda);

		JLabel lblBuscarPor = new JLabel("Buscar Por:");
		lblBuscarPor.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblBuscarPor.setBounds(10, 14, 72, 14);
		getContentPane().add(lblBuscarPor);

		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(controlador);
		btnBuscar.setBackground(Color.WHITE);
		btnBuscar.setBounds(487, 11, 98, 20);
		getContentPane().add(btnBuscar);

		super.setTitle("Almacen");

		super.getBtnAgregar().setText("Agregar Ingrediente");
		super.getBtnModificar().setText("Modificar Ingrediente");
		super.getBtnBorrar().setText("Borrar Ingrediente");

		super.getBtnAgregar().addActionListener(controlador);
		super.getBtnModificar().addActionListener(controlador);
		super.getBtnBorrar().addActionListener(controlador);

		super.getTable().setModel(new DefaultTableModel(
				new Object[][] { { null, null, null, null, null, null }, { null, null, null, null, null, null },
						{ null, null, null, null, null, null }, { null, null, null, null, null, null },
						{ null, null, null, null, null, null }, { null, null, null, null, null, null },
						{ null, null, null, null, null, null }, { null, null, null, null, null, null },
						{ null, null, null, null, null, null }, { null, null, null, null, null, null },
						{ null, null, null, null, null, null }, },
				new String[] { "Numero", "Nombre ", "Descripcion", "Envasado", "Cantidad", "Stock" }));

		TableColumnModel modeloColumna = super.getTable().getColumnModel();
		modeloColumna.getColumn(0).setPreferredWidth(70);
		modeloColumna.getColumn(1).setPreferredWidth(150);
		modeloColumna.getColumn(2).setPreferredWidth(250);
		modeloColumna.getColumn(3).setPreferredWidth(100);
		modeloColumna.getColumn(4).setPreferredWidth(100);
		modeloColumna.getColumn(5).setPreferredWidth(100);

	}

	public JTextField getTxtBusquedaIng() {
		return txtBusquedaIng;
	}

	public void setTxtBusquedaIng(JTextField txtBusquedaIng) {
		this.txtBusquedaIng = txtBusquedaIng;
	}

	public JButton getBtnBuscar() {
		return btnBuscar;
	}

	public void setBtnBuscar(JButton btnBuscar) {
		this.btnBuscar = btnBuscar;
	}

	public ControladorIngredienteABM getCia() {
		return cia;
	}

	public void setCia(ControladorIngredienteABM cia) {
		this.cia = cia;
	}

	public JComboBox getCmbxFiltroBusqueda() {
		return cmbxFiltroBusqueda;
	}

	public void setCmbxFiltroBusqueda(JComboBox cmbxFiltroBusqueda) {
		this.cmbxFiltroBusqueda = cmbxFiltroBusqueda;
	}
	
	
}
