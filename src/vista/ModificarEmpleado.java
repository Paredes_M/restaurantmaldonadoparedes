package vista;

import java.util.ArrayList;

import controlador.ControladorModificarEmpleado;

public class ModificarEmpleado extends ABSEmpleadoAM {

	private ControladorModificarEmpleado controlador;

	public ModificarEmpleado(ControladorModificarEmpleado controlador) {
		this.setControlador(controlador);
		super.getBtnGuardarUsuario().addActionListener(controlador);
		super.getBtnCancelar().addActionListener(controlador);

	}

	public void completarCampos(ArrayList<String> seleccion) {
		this.getTxtLegajo().setText(seleccion.get(0));
		this.getTxtNombre().setText(seleccion.get(1));
		this.getTxtApellido().setText(seleccion.get(2));
		this.getTxtDni().setText(seleccion.get(3));
		this.getTxtNcontacto().setText(seleccion.get(4));
		this.getTxtEmail().setText(seleccion.get(5));
		this.getTxtUsuario().setText(seleccion.get(6));
		this.getTxtContrasenia().setText(seleccion.get(7));
		if (seleccion.get(8).equals("Mesero")) {
			this.getRdbtnMesero().setSelected(true);
		} else {
			this.getRdbtnCocinero().setSelected(true);
		}
	}

	public ControladorModificarEmpleado getControlador() {
		return controlador;
	}

	public void setControlador(ControladorModificarEmpleado controlador) {
		this.controlador = controlador;
	}

}
