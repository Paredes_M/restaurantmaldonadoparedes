
package vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorCocina;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Color;
import java.awt.Font;

public class VistaCocina extends JFrame {

	private JPanel contentPane;
	private ControladorCocina controlador;
	private JTable table;
	private JMenuItem mntmCocinaSalir;
	private JMenuItem mntmAlmacen;
	private JMenuItem mntmCocinaMenu;
	private JButton btnEntregar;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					VistaCocina frame = new VistaCocina();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public VistaCocina(ControladorCocina controlador) {
		setResizable(false);
		setTitle("Sistema de Cocina");
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 390);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 32, 564, 266);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				 "Numero de Pedido", "Numero de Mesa","Descripcion", "En Proceso"
			}
		));
		scrollPane.setViewportView(table);
		
		btnEntregar = new JButton("Entregar");
		btnEntregar.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnEntregar.setBackground(Color.LIGHT_GRAY);
		btnEntregar.setForeground(Color.BLACK);
		btnEntregar.setBounds(227, 309, 109, 31);
		contentPane.add(btnEntregar);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(Color.WHITE);
		menuBar.setBounds(0, 0, 584, 21);
		contentPane.add(menuBar);
		
		JMenu mnCocinaArchivo = new JMenu("Archivo");
		mnCocinaArchivo.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		mnCocinaArchivo.setForeground(Color.LIGHT_GRAY);
		menuBar.add(mnCocinaArchivo);
		
		mntmAlmacen = new JMenuItem("Almacen");
		mntmAlmacen.addActionListener(this.getControlador());
		mntmAlmacen.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		mntmAlmacen.setForeground(Color.BLACK);
		mnCocinaArchivo.add(mntmAlmacen);
		
		mntmCocinaSalir = new JMenuItem("Salir");
		mntmCocinaSalir.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		mntmCocinaSalir.addActionListener(this.getControlador());
		mntmCocinaSalir.setForeground(Color.BLACK);
		mnCocinaArchivo.add(mntmCocinaSalir);
		
		JMenu mnCocinaEditar = new JMenu("Editar");
		mnCocinaEditar.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		mnCocinaEditar.setForeground(Color.LIGHT_GRAY);
		menuBar.add(mnCocinaEditar);
		
		mntmCocinaMenu = new JMenuItem("Menu");
		mntmCocinaMenu.addActionListener(controlador);
		mntmCocinaMenu.setBackground(Color.LIGHT_GRAY);
		mntmCocinaMenu.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		mntmCocinaMenu.setForeground(Color.BLACK);
		mnCocinaEditar.add(mntmCocinaMenu);
		
		this.setLocationRelativeTo(null);
	}

	public ControladorCocina getControlador() {
		return controlador;
	}

	public void setControlador(ControladorCocina controlador) {
		this.controlador = controlador;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JMenuItem getMntmCocinaSalir() {
		return mntmCocinaSalir;
	}

	public void setMntmCocinaSalir(JMenuItem mntmCocinaSalir) {
		this.mntmCocinaSalir = mntmCocinaSalir;
	}

	public JMenuItem getMntmAlmacen() {
		return mntmAlmacen;
	}

	public void setMntmAlmacen(JMenuItem mntmAlmacen) {
		this.mntmAlmacen = mntmAlmacen;
	}

	public JMenuItem getMntmCocinaMenu() {
		return mntmCocinaMenu;
	}

	public void setMntmCocinaMenu(JMenuItem mntmCocinaMenu) {
		this.mntmCocinaMenu = mntmCocinaMenu;
	}

	public JButton getBtnEntregar() {
		return btnEntregar;
	}

	public void setBtnEntregar(JButton btnEntregar) {
		this.btnEntregar = btnEntregar;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}
}
