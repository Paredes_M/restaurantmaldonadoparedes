package vista;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorAdmin;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;

public class VistaAdmin extends JFrame {


	private JPanel contentPane;
	private ControladorAdmin controlador;
	private JMenuItem mntmEmpleado;
	private JMenuItem mntmSalir;
	private JMenuItem mntmPedidos;
	private JMenuItem mntmTodasLasMesas;
	private JMenuItem mntmAlmacen;
	private JMenuBar menuBar;
	private JTable table;
	private JButton btnListarVentas;
	private JComboBox cmbxFiltro;
	private JButton btnBuscar;
	private JMenuItem mntmMenu;
	private JTextField txtBusqueda;

	public VistaAdmin(ControladorAdmin controlador) {
		setResizable(false);
		this.setControlador(controlador);
		addWindowListener(controlador);
		setFont(new Font("Segoe UI", Font.PLAIN, 12));
		setTitle("Administrador");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 734, 413);
		contentPane = new JPanel();
		contentPane.setAutoscrolls(true);
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		menuBar = new JMenuBar();
		menuBar.setMaximumSize(new Dimension(32767, 32767));
		menuBar.setBounds(0, 0, 718, 21);
		menuBar.setAlignmentX(Component.LEFT_ALIGNMENT);
		menuBar.setBackground(Color.WHITE);
		contentPane.add(menuBar);

		JMenu mnArchivo = new JMenu("Archivo");
		mnArchivo.setForeground(Color.GRAY);
		mnArchivo.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		menuBar.add(mnArchivo);

		mntmEmpleado = new JMenuItem("Empleados..");
		mntmEmpleado.setForeground(Color.BLACK);
		mntmEmpleado.addActionListener(this.getControlador());
		mntmEmpleado.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		mnArchivo.add(mntmEmpleado);

		mntmSalir = new JMenuItem("Salir");
		mntmSalir.setForeground(Color.BLACK);
		mntmSalir.addActionListener(this.getControlador());
		mntmSalir.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		mnArchivo.add(mntmSalir);

		JMenu mnEditar = new JMenu("Editar");
		mnEditar.setForeground(Color.GRAY);
		mnEditar.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		menuBar.add(mnEditar);

		mntmMenu = new JMenuItem("Menu");
		mntmMenu.addActionListener(this.getControlador());
		mntmMenu.setForeground(Color.BLACK);
		mntmMenu.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		mnEditar.add(mntmMenu);

		JMenu mnVer = new JMenu("Ver");
		mnVer.setForeground(Color.GRAY);
		mnVer.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		menuBar.add(mnVer);

		mntmTodasLasMesas = new JMenuItem("Mesas");
		mntmTodasLasMesas.setForeground(Color.BLACK);
		mntmTodasLasMesas.addActionListener(this.getControlador());
		mntmTodasLasMesas.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		mnVer.add(mntmTodasLasMesas);

		mntmAlmacen = new JMenuItem("Almacen");
		mntmAlmacen.setForeground(Color.BLACK);
		mntmAlmacen.addActionListener(this.getControlador());

		mntmPedidos = new JMenuItem("Pedidos en el dia");
		mnVer.add(mntmPedidos);
		mntmPedidos.setHorizontalAlignment(SwingConstants.LEFT);
		mntmPedidos.setForeground(Color.BLACK);
		mntmPedidos.addActionListener(this.getControlador());
		mntmPedidos.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		mntmAlmacen.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		mnVer.add(mntmAlmacen);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(9, 83, 698, 256);
		contentPane.add(scrollPane);

		table = new JTable();
		table.setBorder(new LineBorder(Color.LIGHT_GRAY));
		table.setModel(new DefaultTableModel(new Object[][] { { null, null, null, null, null },
				{ null, null, null, null, null }, { null, null, null, null, null }, { null, null, null, null, null },
				{ null, null, null, null, null }, { null, null, null, null, null }, { null, null, null, null, null },
				{ null, null, null, null, null }, { null, null, null, null, null }, { null, null, null, null, null },
				{ null, null, null, null, null }, { null, null, null, null, null }, { null, null, null, null, null }, },
				new String[] { "Numero ", "Descripcion", "Total Ventas", "Fecha", "Hora" }));
		scrollPane.setViewportView(table);
		
		btnListarVentas = new JButton("Listar Ventas");
		btnListarVentas.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnListarVentas.setBackground(Color.WHITE);
		btnListarVentas.setBounds(567, 350, 140, 23);
		contentPane.add(btnListarVentas);
		
		JLabel lblBuscarPor = new JLabel("Buscar Por:");
		lblBuscarPor.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblBuscarPor.setBounds(10, 32, 87, 31);
		contentPane.add(lblBuscarPor);
		
		cmbxFiltro = new JComboBox();
		cmbxFiltro.setModel(new DefaultComboBoxModel(new String[] {"", "Fecha", "Mesero", "Total Ventas"}));
		cmbxFiltro.setBackground(Color.WHITE);
		cmbxFiltro.setBounds(92, 39, 140, 21);
		contentPane.add(cmbxFiltro);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnBuscar.setBackground(Color.WHITE);
		btnBuscar.setBounds(616, 39, 91, 21);
		contentPane.add(btnBuscar);
		
		txtBusqueda = new JTextField();
		txtBusqueda.setBounds(239, 39, 367, 21);
		contentPane.add(txtBusqueda);
		txtBusqueda.setColumns(10);

		table.getTableHeader().setReorderingAllowed(false);
		table.getTableHeader().setResizingAllowed(false);
		this.setLocationRelativeTo(null);

	}

	public JMenuItem getMntmEmpleado() {
		return mntmEmpleado;
	}

	public void setMntmEmpleado(JMenuItem mntmEmpleado) {
		this.mntmEmpleado = mntmEmpleado;
	}

	public ControladorAdmin getControlador() {
		return controlador;
	}

	public void setControlador(ControladorAdmin controlador) {
		this.controlador = controlador;
	}

	public JMenuItem getMntmNuevoUsuario() {
		return mntmEmpleado;
	}

	public void setMntmNuevoUsuario(JMenuItem mntmNuevoUsuario) {
		this.mntmEmpleado = mntmNuevoUsuario;
	}

	public JMenuItem getMntmSalir() {
		return mntmSalir;
	}

	public void setMntmSalir(JMenuItem mntmSalir) {
		this.mntmSalir = mntmSalir;
	}

	public JMenuItem getMntmModificarPedidos() {
		return mntmPedidos;
	}

	public void setMntmModificarPedidos(JMenuItem mntmModificarPedidos) {
		this.mntmPedidos = mntmModificarPedidos;
	}

	public JMenuItem getMntmMesasDisponibles() {
		return mntmTodasLasMesas;
	}

	public void setMntmMesasDisponibles(JMenuItem mntmMesasDisponibles) {
		this.mntmTodasLasMesas = mntmMesasDisponibles;
	}

	public JMenuItem getMntmMesasOcupadas() {
		return mntmTodasLasMesas;
	}

	public void setMntmMesasOcupadas(JMenuItem mntmMesasOcupadas) {
		this.mntmTodasLasMesas = mntmMesasOcupadas;
	}

	public JMenuItem getMntmAlmacen() {
		return mntmAlmacen;
	}

	public void setMntmAlmacen(JMenuItem mntmAlmacen) {
		this.mntmAlmacen = mntmAlmacen;
	}

	public JMenuItem getMntmPedidos() {
		return mntmPedidos;
	}

	public void setMntmPedidos(JMenuItem mntmPedidos) {
		this.mntmPedidos = mntmPedidos;
	}

	public JMenuItem getMntmTodasLasMesas() {
		return mntmTodasLasMesas;
	}

	public void setMntmTodasLasMesas(JMenuItem mntmTodasLasMesas) {
		this.mntmTodasLasMesas = mntmTodasLasMesas;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JButton getBtnListarVentas() {
		return btnListarVentas;
	}

	public void setBtnListarVentas(JButton btnListarVentas) {
		this.btnListarVentas = btnListarVentas;
	}

	public JComboBox getCmbxFiltro() {
		return cmbxFiltro;
	}

	public void setCmbxFiltro(JComboBox cmbxFiltro) {
		this.cmbxFiltro = cmbxFiltro;
	}

	public JButton getBtnBuscar() {
		return btnBuscar;
	}

	public void setBtnBuscar(JButton btnBuscar) {
		this.btnBuscar = btnBuscar;
	}

	public JMenuItem getMntmMenu() {
		return mntmMenu;
	}

	public void setMntmMenu(JMenuItem mntmMenu) {
		this.mntmMenu = mntmMenu;
	}

	public JTextField getTxtBusqueda() {
		return txtBusqueda;
	}

	public void setTxtBusqueda(JTextField txtBusqueda) {
		this.txtBusqueda = txtBusqueda;
	}
}
