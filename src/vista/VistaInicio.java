package vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorInicio;
import modelo.Mesa;

import javax.swing.JButton;
import java.awt.Font;
import java.util.ArrayList;
import java.awt.Color;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class VistaInicio extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private ControladorInicio controlador;
	private JButton btnMesa1;
	private JButton btnMesa2;
	private JButton btnMesa3;
	private JButton btnMesa4;
	private JButton btnMesa5;
	private JButton btnMesa6;
	private JButton btnMesa7;
	private JButton btnMesa8;
	private JButton btnMesa15;
	private JButton btnMesa14;
	private JButton btnMesa13;
	private JButton btnMesa12;
	private JButton btnMesa11;
	private JButton btnMesa10;
	private JButton btnMesa9;
	private JMenuItem mntmSalir;
	private JMenuItem mntmAlmacen;
	private JTable table;
	private JMenuItem mntmMenu;
	private ArrayList<JButton> botonesMesas;

	/**
	 * Create the frame.
	 */
	public VistaInicio(ControladorInicio controlador) {
		setResizable(false);
		setForeground(Color.BLACK);
		setTitle("Sistema de Mesero");
		this.setControlador(controlador);
		addWindowListener(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 555);
		contentPane = new JPanel();
		contentPane.setAutoscrolls(true);
		contentPane.setBackground(new Color(240, 240, 240));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.setBotonesMesas(new ArrayList<JButton>());
		
		btnMesa1 = new Mesa(1, true);
		btnMesa1.addMouseListener(this.getControlador());
		btnMesa1.setBackground(Color.WHITE);
		btnMesa1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnMesa1.setBounds(30, 50, 150, 50);
		contentPane.add(btnMesa1);

		btnMesa2 = new Mesa(2, true);
		btnMesa2.addMouseListener(this.getControlador());
		btnMesa2.setBackground(Color.WHITE);
		btnMesa2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnMesa2.setBounds(30, 125, 150, 50);
		contentPane.add(btnMesa2);

		btnMesa5 = new Mesa(5, true);
		btnMesa5.addMouseListener(this.getControlador());
		btnMesa5.setBackground(Color.WHITE);
		btnMesa5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnMesa5.setBounds(30, 350, 150, 50);
		contentPane.add(btnMesa5);

		btnMesa6 = new Mesa(6, true);
		btnMesa6.addMouseListener(this.getControlador());
		btnMesa6.setBackground(Color.WHITE);
		btnMesa6.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnMesa6.setBounds(200, 50, 150, 50);
		contentPane.add(btnMesa6);

		btnMesa3 = new Mesa(3, true);
		btnMesa3.addMouseListener(this.getControlador());
		btnMesa3.setBackground(Color.WHITE);
		btnMesa3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnMesa3.setBounds(30, 200, 150, 50);
		contentPane.add(btnMesa3);

		btnMesa7 = new Mesa(7, true);
		btnMesa7.addMouseListener(this.getControlador());
		btnMesa7.setBackground(Color.WHITE);
		btnMesa7.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnMesa7.setBounds(200, 125, 150, 50);
		contentPane.add(btnMesa7);

		btnMesa4 = new Mesa(4, true);
		btnMesa4.addMouseListener(this.getControlador());
		btnMesa4.setBackground(Color.WHITE);
		btnMesa4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnMesa4.setBounds(30, 275, 150, 50);
		contentPane.add(btnMesa4);

		btnMesa8 = new Mesa(8, true);
		btnMesa8.addMouseListener(this.getControlador());
		btnMesa8.setBackground(Color.WHITE);
		btnMesa8.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnMesa8.setBounds(200, 200, 150, 50);
		contentPane.add(btnMesa8);

		btnMesa9 = new Mesa(9, true);
		btnMesa9.addMouseListener(this.getControlador());
		btnMesa9.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnMesa9.setBackground(Color.WHITE);
		btnMesa9.setBounds(200, 275, 150, 50);
		contentPane.add(btnMesa9);

		btnMesa10 = new Mesa(10, true);
		btnMesa10.addMouseListener(this.getControlador());
		btnMesa10.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnMesa10.setBackground(Color.WHITE);
		btnMesa10.setBounds(200, 350, 150, 50);
		contentPane.add(btnMesa10);

		btnMesa11 = new Mesa(11, true);
		btnMesa11.addMouseListener(this.getControlador());
		btnMesa11.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnMesa11.setBackground(Color.WHITE);
		btnMesa11.setBounds(370, 50, 150, 50);
		contentPane.add(btnMesa11);

		btnMesa12 = new Mesa(12, true);
		btnMesa12.addMouseListener(this.getControlador());
		btnMesa12.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnMesa12.setBackground(Color.WHITE);
		btnMesa12.setBounds(370, 125, 150, 50);
		contentPane.add(btnMesa12);

		btnMesa13 = new Mesa(13, true);
		btnMesa13.addMouseListener(this.getControlador());
		btnMesa13.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnMesa13.setBackground(Color.WHITE);
		btnMesa13.setBounds(370, 200, 150, 50);
		contentPane.add(btnMesa13);

		btnMesa14 = new Mesa(14, true);
		btnMesa14.addMouseListener(this.getControlador());
		btnMesa14.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnMesa14.setBackground(Color.WHITE);
		btnMesa14.setBounds(370, 275, 150, 50);
		contentPane.add(btnMesa14);

		btnMesa15 = new Mesa(15, true);
		btnMesa15.addMouseListener(this.getControlador());
		btnMesa15.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnMesa15.setBackground(Color.WHITE);
		btnMesa15.setBounds(370, 350, 150, 50);
		contentPane.add(btnMesa15);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 984, 21);
		contentPane.add(menuBar);

		JMenu mnMenuArchivo = new JMenu("Archivo");
		mnMenuArchivo.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		mnMenuArchivo.setForeground(Color.GRAY);
		menuBar.add(mnMenuArchivo);

		mntmSalir = new JMenuItem("Salir");
		mntmSalir.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		mntmSalir.setForeground(Color.BLACK);
		mntmSalir.addActionListener(this.getControlador());

		JMenuItem mntmBuscar = new JMenuItem("Buscar");
		mntmBuscar.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		mntmBuscar.setForeground(Color.BLACK);
		mnMenuArchivo.add(mntmBuscar);
		mnMenuArchivo.add(mntmSalir);

		JMenu mnMenuVer = new JMenu("Ver");
		mnMenuVer.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		mnMenuVer.setForeground(Color.GRAY);
		menuBar.add(mnMenuVer);

		mntmAlmacen = new JMenuItem("Almacen");
		mntmAlmacen.addActionListener(controlador);
		mntmAlmacen.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		mntmAlmacen.setForeground(Color.BLACK);
		mnMenuVer.add(mntmAlmacen);

		mntmMenu = new JMenuItem("Menu");
		mntmMenu.addActionListener(controlador);
		mntmMenu.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		mntmMenu.setForeground(Color.BLACK);
		mnMenuVer.add(mntmMenu);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(530, 50, 444, 350);
		contentPane.add(scrollPane);

		table = new JTable();
		table.setModel(new DefaultTableModel(
				new Object[][] { { null, null }, { null, null }, { null, null }, { null, null }, { null, null },
						{ null, null }, { null, null }, { null, null }, { null, null }, { null, null }, { null, null },
						{ null, null }, { null, null }, { null, null }, { null, null }, { null, null }, { null, null },
						{ null, null }, { null, null }, { null, null }, { null, null }, { null, null }, { null, null },
						{ null, null }, { null, null }, { null, null }, { null, null }, { null, null }, { null, null },
						{ null, null }, { null, null }, { null, null }, },
				new String[] { "Mesa", "Numero Pedido ", "En Proceso", "Entregado" }) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			boolean[] columnEditables = new boolean[] { false, true };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		scrollPane.setViewportView(table);
		table.getTableHeader().setReorderingAllowed(false);
		table.getTableHeader().setResizingAllowed(false);
		this.setLocationRelativeTo(null);

		this.getBotonesMesas().add(this.getBtnMesa1());
		this.getBotonesMesas().add(this.getBtnMesa2());
		this.getBotonesMesas().add(this.getBtnMesa3());
		this.getBotonesMesas().add(this.getBtnMesa4());
		this.getBotonesMesas().add(this.getBtnMesa5());
		this.getBotonesMesas().add(this.getBtnMesa6());
		this.getBotonesMesas().add(this.getBtnMesa7());
		this.getBotonesMesas().add(this.getBtnMesa8());
		this.getBotonesMesas().add(this.getBtnMesa9());
		this.getBotonesMesas().add(this.getBtnMesa10());
		this.getBotonesMesas().add(this.getBtnMesa11());
		this.getBotonesMesas().add(this.getBtnMesa12());
		this.getBotonesMesas().add(this.getBtnMesa13());
		this.getBotonesMesas().add(this.getBtnMesa14());
		this.getBotonesMesas().add(this.getBtnMesa15());
	}

	public ControladorInicio getControlador() {
		return controlador;
	}

	public void setControlador(ControladorInicio controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnMesa1() {
		return btnMesa1;
	}

	public void setBtnMesa1(JButton btnMesa1) {
		this.btnMesa1 = btnMesa1;
	}

	public JButton getBtnMesa2() {
		return btnMesa2;
	}

	public void setBtnMesa2(JButton btnMesa2) {
		this.btnMesa2 = btnMesa2;
	}

	public JButton getBtnMesa3() {
		return btnMesa3;
	}

	public void setBtnMesa3(JButton btnMesa3) {
		this.btnMesa3 = btnMesa3;
	}

	public JButton getBtnMesa4() {
		return btnMesa4;
	}

	public void setBtnMesa4(JButton btnMesa4) {
		this.btnMesa4 = btnMesa4;
	}

	public JButton getBtnMesa5() {
		return btnMesa5;
	}

	public void setBtnMesa5(JButton btnMesa5) {
		this.btnMesa5 = btnMesa5;
	}

	public JButton getBtnMesa6() {
		return btnMesa6;
	}

	public void setBtnMesa6(JButton btnMesa6) {
		this.btnMesa6 = btnMesa6;
	}

	public JButton getBtnMesa7() {
		return btnMesa7;
	}

	public void setBtnMesa7(JButton btnMesa7) {
		this.btnMesa7 = btnMesa7;
	}

	public JButton getBtnMesa8() {
		return btnMesa8;
	}

	public void setBtnMesa8(JButton btnMesa8) {
		this.btnMesa8 = btnMesa8;
	}

	public JButton getBtnMesa15() {
		return btnMesa15;

	}

	public void setBtnMesa15(JButton btnMesa15) {
		this.btnMesa15 = btnMesa15;
	}

	public JButton getBtnMesa14() {
		return btnMesa14;
	}

	public void setBtnMesa14(JButton btnMesa14) {
		this.btnMesa14 = btnMesa14;
	}

	public JButton getBtnMesa13() {
		return btnMesa13;
	}

	public void setBtnMesa13(JButton btnMesa13) {
		this.btnMesa13 = btnMesa13;
	}

	public JButton getBtnMesa12() {
		return btnMesa12;
	}

	public void setBtnMesa12(JButton btnMesa12) {
		this.btnMesa12 = btnMesa12;
	}

	public JButton getBtnMesa11() {
		return btnMesa11;
	}

	public void setBtnMesa11(JButton btnMesa11) {
		this.btnMesa11 = btnMesa11;
	}

	public JButton getBtnMesa10() {
		return btnMesa10;
	}

	public void setBtnMesa10(JButton btnMesa10) {
		this.btnMesa10 = btnMesa10;
	}

	public JButton getBtnMesa9() {
		return btnMesa9;
	}

	public void setBtnMesa9(JButton btnMesa9) {
		this.btnMesa9 = btnMesa9;
	}

	public JMenuItem getMntmSalir() {
		return mntmSalir;
	}

	public void setMntmSalir(JMenuItem mntmSalir) {
		this.mntmSalir = mntmSalir;
	}

	public JMenuItem getMntmAlmacen() {
		return mntmAlmacen;
	}

	public void setMntmAlmacen(JMenuItem mntmAlmacen) {
		this.mntmAlmacen = mntmAlmacen;
	}

	public JMenuItem getMntmMenu() {
		return mntmMenu;
	}

	public void setMntmMenu(JMenuItem mntmMenu) {
		this.mntmMenu = mntmMenu;
	}

	public ArrayList<JButton> getBotonesMesas() {
		return botonesMesas;
	}

	public void setBotonesMesas(ArrayList<JButton> botonesMesas) {
		this.botonesMesas = botonesMesas;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}
	
}
