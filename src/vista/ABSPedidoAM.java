package vista;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import javax.swing.JTextPane;

public class ABSPedidoAM extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTable table;
	private JTable table_1;
	private JTextField txtBusqueda;
	private JButton btnAniadir;
	private JButton btnQuitar;
	private JButton btnBuscar;
	private JButton btnOpcion1;
	private JButton btnOpcion2;
	private JLabel lblSeleccioneLosPlatillos;
	private JTextField textCantidad;
	private JButton btnMenos;
	private JButton btnMas;
	private JTextPane txtpnIngreseSuDescripcion;

	public ABSPedidoAM() {
		crearVista();
	}

	private void crearVista() {
		setModal(true);
		setAlwaysOnTop(true);
		setTitle(" ");
		setBounds(100, 100, 763, 582);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 60, 305, 291);
		contentPanel.add(scrollPane);

		table = new JTable();
		table.setModel(new DefaultTableModel(
				new Object[][] { { null, null }, { null, null }, { null, null }, { null, null }, { null, null },
						{ null, null }, { null, null }, { null, null }, { null, null }, { null, null }, { null, null },
						{ null, null }, { null, null }, { null, null }, { null, null }, },
				new String[] { "Numero Platillo", "Nombre Platillo" }));
		scrollPane.setViewportView(table);
		table.getTableHeader().setReorderingAllowed(false);
		table.getTableHeader().setResizingAllowed(false);

		btnAniadir = new JButton("A�adir");
		btnAniadir.setBounds(325, 132, 90, 25);
		btnAniadir.setBackground(Color.WHITE);
		contentPanel.add(btnAniadir);

		btnQuitar = new JButton("Quitar");
		btnQuitar.setBounds(325, 238, 90, 25);
		btnQuitar.setBackground(Color.WHITE);
		contentPanel.add(btnQuitar);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(424, 62, 313, 291);
		contentPanel.add(scrollPane_1);

		table_1 = new JTable();
		table_1.setModel(new DefaultTableModel(
				new Object[][] { { null, null }, { null, null }, { null, null }, { null, null }, { null, null },
						{ null, null }, { null, null }, { null, null }, { null, null }, { null, null }, { null, null },
						{ null, null }, { null, null }, { null, null }, { null, null }, },
				new String[] { "Numero Platillo", "Nombre Platillo" }));
		scrollPane_1.setViewportView(table_1);

		table_1.getTableHeader().setReorderingAllowed(false);
		table_1.getTableHeader().setResizingAllowed(false);

		txtBusqueda = new JTextField();
		txtBusqueda.setBounds(424, 24, 229, 20);
		contentPanel.add(txtBusqueda);
		txtBusqueda.setColumns(10);

		lblSeleccioneLosPlatillos = new JLabel("Seleccione los ingredientes que desea agregar/quitar: ");
		lblSeleccioneLosPlatillos.setBounds(10, 22, 305, 20);
		lblSeleccioneLosPlatillos.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contentPanel.add(lblSeleccioneLosPlatillos);

		btnBuscar = new JButton("Buscar");
		btnBuscar.setBounds(658, 23, 79, 23);
		btnBuscar.setBackground(Color.WHITE);
		contentPanel.add(btnBuscar);

		textCantidad = new JTextField();
		textCantidad.setBounds(325, 202, 90, 25);
		contentPanel.add(textCantidad);
		textCantidad.setColumns(10);

		btnMenos = new JButton("-"); //hay que darle funcion
		btnMenos.setBounds(312, 165, 53, 25);
		btnMenos.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contentPanel.add(btnMenos);

		btnMas = new JButton("+"); //hay que darle funcion
		btnMas.setBounds(372, 165, 53, 25);
		btnMas.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contentPanel.add(btnMas);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(10, 405, 727, 75);
		contentPanel.add(scrollPane_2);
		
		txtpnIngreseSuDescripcion = new JTextPane();
		txtpnIngreseSuDescripcion.setToolTipText("ingrese su descripcion del pedido aqui...");
		txtpnIngreseSuDescripcion.setText("ingrese su descripcion del pedido aqui...");
		txtpnIngreseSuDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtpnIngreseSuDescripcion.setContentType("ingrese su descripcion del pedido aqui..");
		scrollPane_2.setViewportView(txtpnIngreseSuDescripcion);
		
		JLabel lblNewLabel = new JLabel("Inserta la descripcion aqui:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(10, 369, 286, 25);
		contentPanel.add(lblNewLabel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				btnOpcion1 = new JButton("");
				btnOpcion1.setBackground(Color.WHITE);
				btnOpcion1.setActionCommand("OK");
				buttonPane.add(btnOpcion1);
				getRootPane().setDefaultButton(btnOpcion1);
			}
			{
				btnOpcion2 = new JButton("");
				btnOpcion2.setBackground(Color.WHITE);
				btnOpcion2.setActionCommand("Cancel");
				buttonPane.add(btnOpcion2);
			}
		}

		this.setLocationRelativeTo(null);
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JTable getTable_1() {
		return table_1;
	}

	public void setTable_1(JTable table_1) {
		this.table_1 = table_1;
	}

	public JTextField getTxtBusqueda() {
		return txtBusqueda;
	}

	public void setTxtBusqueda(JTextField txtBusqueda) {
		this.txtBusqueda = txtBusqueda;
	}

	public JButton getBtnAniadir() {
		return btnAniadir;
	}

	public void setBtnAniadir(JButton btnAniadir) {
		this.btnAniadir = btnAniadir;
	}

	public JButton getBtnQuitar() {
		return btnQuitar;
	}

	public void setBtnQuitar(JButton btnQuitar) {
		this.btnQuitar = btnQuitar;
	}

	public JButton getBtnBuscar() {
		return btnBuscar;
	}

	public void setBtnBuscar(JButton btnBuscar) {
		this.btnBuscar = btnBuscar;
	}

	public JButton getBtnOpcion1() {
		return btnOpcion1;
	}

	public void setBtnOpcion1(JButton btnOpcion1) {
		this.btnOpcion1 = btnOpcion1;
	}

	public JButton getBtnOpcion2() {
		return btnOpcion2;
	}

	public void setBtnOpcion2(JButton btnOpcion2) {
		this.btnOpcion2 = btnOpcion2;
	}

	public JLabel getLblSeleccioneLosPlatillos() {
		return lblSeleccioneLosPlatillos;
	}

	public void setLblSeleccioneLosPlatillos(JLabel lblSeleccioneLosPlatillos) {
		this.lblSeleccioneLosPlatillos = lblSeleccioneLosPlatillos;
	}

	public JTextField getTextCantidad() {
		return textCantidad;
	}

	public void setTextCantidad(JTextField textCantidad) {
		this.textCantidad = textCantidad;
	}

	public JButton getBtnMenos() {
		return btnMenos;
	}

	public void setBtnMenos(JButton btnMenos) {
		this.btnMenos = btnMenos;
	}

	public JButton getBtnMas() {
		return btnMas;
	}

	public void setBtnMas(JButton btnMas) {
		this.btnMas = btnMas;
	}
	
	public JTextPane getTxtpnIngreseSuDescripcion() {
		return txtpnIngreseSuDescripcion;
	}

	public void setTxtpnIngreseSuDescripcion(JTextPane txtpnIngreseSuDescripcion) {
		this.txtpnIngreseSuDescripcion = txtpnIngreseSuDescripcion;
	}
}
