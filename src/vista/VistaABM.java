package vista;

import javax.swing.JDialog;
import javax.swing.JPanel;

import controlador.ControladorPedido;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.Cursor;
import java.awt.Color;

public abstract class VistaABM extends JDialog{
	
	//private ControladorPedido controladorP;
	private JTable table;
	private JPanel contentPane;
	private JButton btnAgregar;
	private JButton btnModificar;
	private JButton btnBorrar;
	private JScrollPane scrollPane;
	
	
	public VistaABM() {
		
		crearVista();	
	}
	
	private void crearVista() {
		setModal(true);
		setResizable(false);
		setAlwaysOnTop(true);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		setTitle(" ");
		setBounds(100, 100, 450, 323);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 414, 196);
	    contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				" ", " ", " ", " "
			}
		));
		scrollPane.setViewportView(table);
		
		btnAgregar = new JButton(" ");
		btnAgregar.setBackground(Color.WHITE);
		btnAgregar.setBounds(10, 229, 129, 23);
		getContentPane().add(btnAgregar);
		
		btnModificar = new JButton(" ");
		btnModificar.setBackground(Color.WHITE);
		btnModificar.setBounds(149, 229, 140, 23);
		getContentPane().add(btnModificar);
		
		btnBorrar = new JButton(" ");
		btnBorrar.setBackground(Color.WHITE);
		btnBorrar.setBounds(299, 229, 125, 23);
		getContentPane().add(btnBorrar);
		
		this.setLocationRelativeTo(null);
		
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	public JButton getBtnModificar() {
		return btnModificar;
	}

	public void setBtnModificar(JButton btnModificar) {
		this.btnModificar = btnModificar;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}

	public void setBtnBorrar(JButton btnBorrar) {
		this.btnBorrar = btnBorrar;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	
}
