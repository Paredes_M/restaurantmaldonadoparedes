package vista;

import controlador.ControladorPedido;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PedidoABM extends ABSABM{
	
	private ControladorPedido controladorP;
	private JTextField txtTotalAPagar;
	private JButton btnAgregarVenta;
	private JCheckBox chckbxOcuparMesa;

	
	public PedidoABM(ControladorPedido controlador) {
		super();
		addWindowListener(controlador);
		getBtnModificar().setLocation(210, 251);
		getBtnBorrar().setLocation(396, 251);
		super.setSize(600,350);
		getBtnAgregar().setLocation(29, 251);
		this.setControladorP(controlador);
		getScrollPane().setBackground(Color.WHITE);
		
		
		JLabel lblTotalVenta = new JLabel("Total a Pagar:");
		lblTotalVenta.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTotalVenta.setBounds(101, 289, 98, 30);
		getContentPane().add(lblTotalVenta);
		
		txtTotalAPagar = new JTextField();
		txtTotalAPagar.setBackground(Color.WHITE);
		txtTotalAPagar.setEditable(false);
		txtTotalAPagar.setHorizontalAlignment(SwingConstants.CENTER);
		txtTotalAPagar.setBounds(210, 296, 146, 20);
		getContentPane().add(txtTotalAPagar);
		txtTotalAPagar.setColumns(10);
		
		btnAgregarVenta = new JButton("Crear Venta");
		btnAgregarVenta.setEnabled(false);
		btnAgregarVenta.setBackground(Color.WHITE);
		btnAgregarVenta.setBounds(396, 295, 146, 23);
		getContentPane().add(btnAgregarVenta);
		
		chckbxOcuparMesa = new JCheckBox("Ocupar Mesa");
		chckbxOcuparMesa.addActionListener(controlador);
		chckbxOcuparMesa.setFont(new Font("Tahoma", Font.PLAIN, 12));
		chckbxOcuparMesa.setBounds(29, 214, 125, 23);
		getContentPane().add(chckbxOcuparMesa);
		super.setTitle("Pedidos de Mesa");
		super.getBtnAgregar().setText("Agregar Pedido");
		super.getBtnBorrar().setText("Borrar Pedido");
		super.getBtnModificar().setText("Modificar Pedido");
		super.getBtnAgregar().addActionListener(controlador);
		super.getBtnBorrar().addActionListener(controlador);
		super.getBtnModificar().addActionListener(controlador);
		super.getTable().setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null,null,null},
				{null, null, null, null,null,null},
				{null, null, null, null,null,null},
				{null, null, null, null,null,null},
				{null, null, null, null,null,null},
				{null, null, null, null,null,null},
				{null, null, null, null,null,null},
				{null, null, null, null,null,null},
				{null, null, null, null,null,null},
				{null, null, null, null,null,null},
				{null, null, null, null,null,null},
			},
			new String[] {
				"Numero Pedido", "Numero Mesa ", "Descripcion ", "Terminado ","Entregado", "Precio"}
			));
		
		
	TableColumnModel modeloColumna = super.getTable().getColumnModel();
	modeloColumna.getColumn(0).setPreferredWidth(70);
	modeloColumna.getColumn(1).setPreferredWidth(70);
	modeloColumna.getColumn(2).setPreferredWidth(250);
	modeloColumna.getColumn(3).setPreferredWidth(70);
	modeloColumna.getColumn(4).setPreferredWidth(70);
	modeloColumna.getColumn(5).setPreferredWidth(100);
	}
	
	public ControladorPedido getControladorP() {
		return controladorP;
	}
	public void setControladorP(ControladorPedido controladorP) {
		this.controladorP = controladorP;
	}

	public JTextField getTxtTotalAPagar() {
		return txtTotalAPagar;
	}

	public void setTxtTotalAPagar(JTextField txtTotalAPagar) {
		this.txtTotalAPagar = txtTotalAPagar;
	}

	public JButton getBtnAgregarVenta() {
		return btnAgregarVenta;
	}

	public void setBtnAgregarVenta(JButton btnAgregarVenta) {
		this.btnAgregarVenta = btnAgregarVenta;
	}

	public JCheckBox getChckbxOcuparMesa() {
		return chckbxOcuparMesa;
	}

	public void setChckbxOcuparMesa(JCheckBox chckbxOcuparMesa) {
		this.chckbxOcuparMesa = chckbxOcuparMesa;
	}
}
