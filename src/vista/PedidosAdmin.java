package vista;

import controlador.ControladorAdminPedidos;
import javax.swing.table.DefaultTableModel;
import javax.swing.JDialog;

public class PedidosAdmin extends ABSABM{
	
	private ControladorAdminPedidos controlador;
	
	public PedidosAdmin(ControladorAdminPedidos controlador) {
		this.setControlador(this.getControlador());
		getTable().setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null,null, null},
				{null, null, null, null,null, null},
				{null, null, null, null,null, null},
				{null, null, null, null,null, null},
				{null, null, null, null,null, null},
				{null, null, null, null,null, null},
				{null, null, null, null,null, null},
				{null, null, null, null,null, null},
				{null, null, null, null,null, null},
				{null, null, null, null,null, null},
				{null, null, null, null,null, null},
			},
			new String[] {
				"Numero Pedido", "Numero Mesa","Descripcion ", "Terminado "," Entregado", "Precio"
			}
		));
		getBtnModificar().setLocation(218, 229);
		
		addWindowListener(this.getControlador());
		
		super.setTitle("Ver Pedidos");
		super.getBtnAgregar().setText("Agregar Pedido");;
		super.getBtnModificar().setText("Modificar Pedido");
		super.getBtnBorrar().setText("Borrar Pedido");
		
		super.getBtnAgregar().setEnabled(false);
		super.getBtnModificar().setEnabled(false);
		super.getBtnBorrar().setEnabled(false);
		

	}

	public ControladorAdminPedidos getControlador() {
		return controlador;
	}

	public void setControlador(ControladorAdminPedidos controlador) {
		this.controlador = controlador;
	}
	
	

}
