package vista;

import java.util.ArrayList;

import controlador.ControladorModificarMenu;

public class ModificarMenu extends ABSMenuAM {

	private ControladorModificarMenu controlador;

	public ModificarMenu(ControladorModificarMenu controlador) {

		this.setControlador(controlador);
		super.setTitle("Modificar Comida");
		super.getBtnSiguiente().addActionListener(controlador);
		super.getBtnCancelar().addActionListener(controlador);

	}

	public ControladorModificarMenu getControlador() {
		return controlador;
	}

	public void setControlador(ControladorModificarMenu controlador) {
		this.controlador = controlador;
	}

	public void completarCampos(ArrayList<String> seleccion) {
		this.getTxtNumeroComida().setText(seleccion.get(0));
		this.getTxtNombreComida().setText(seleccion.get(1));
		this.getTextDescripcion().setText(seleccion.get(2));
		if (seleccion.get(3).equals("true")) {
			this.getRdbtnPlatilloSi().setSelected(true);
		} else {
			this.getRdbtnPlatilloNo().setSelected(true);
		}
		this.getTxtPrecioUnitario().setText(seleccion.get(4));
	}

}
