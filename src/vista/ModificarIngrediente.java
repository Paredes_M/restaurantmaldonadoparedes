package vista;

import java.util.ArrayList;

import controlador.ControladorModificarIngrediente;

public class ModificarIngrediente extends ABSIngredienteAM{

	private ControladorModificarIngrediente controlador;

	public ModificarIngrediente (ControladorModificarIngrediente controlador) {
		
		this.setControlador(controlador);
		super.setTitle("Modificación ingrediente");
		super.getBtnAceptar().setText("Modificar Ingrediente");
		super.getBtnCancelar().setText("Cancelar");
		super.getBtnAceptar().addActionListener(controlador);
		super.getBtnCancelar().addActionListener(controlador);
	}
	
	
	public ControladorModificarIngrediente getControlador() {
		return controlador;
	}

	public void setControlador(ControladorModificarIngrediente controlador) {
		this.controlador = controlador;
	}
	
	public void completarCampos(ArrayList<String> seleccion) {
		this.getTxtNumeroIngrediente().setText(seleccion.get(0));
		this.getTxtNombreIngrediente().setText(seleccion.get(1));
		if (seleccion.get(2).equals("true")) {
			this.getRdbtnEsBebidaSi().setSelected(true);
		} else {
			this.getRdbtnEsBebidaNo().setSelected(true);
		}
		if (seleccion.get(3).equals("true")) {
			this.getRdbtnEsEnvasadoSI().setSelected(true);
		} else {
			this.getRdbtnEsEnvasadoNo().setSelected(true);
		}
		this.getTextDescripcion().setText(seleccion.get(4));
		this.getTxtCantidad().setText(seleccion.get(5));
		this.getTxtStock().setText(seleccion.get(6));
	}
	
}
