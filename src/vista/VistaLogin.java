package vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import controlador.ControladorLogin;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JCheckBox;

public class VistaLogin extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsuario;
	private JButton btnIngresar;
	private ControladorLogin controlador;

	private JPasswordField psfContrasenia;

	public VistaLogin(ControladorLogin controlador) {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setControlador(controlador);
		setBounds(100, 100, 425, 256);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblUsuario.setBounds(65, 44, 84, 14);
		contentPane.add(lblUsuario);

		JLabel lblContrasenia = new JLabel("Contrase\u00F1a:");
		lblContrasenia.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblContrasenia.setBounds(65, 92, 84, 14);
		contentPane.add(lblContrasenia);

		txtUsuario = new JTextField();
		txtUsuario.setBounds(159, 43, 183, 20);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);

		btnIngresar = new JButton("Ingresar");
		btnIngresar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnIngresar.addActionListener(this.getControlador());
		btnIngresar.setBounds(152, 164, 89, 23);
		contentPane.add(btnIngresar);

		psfContrasenia = new JPasswordField();
		psfContrasenia.addKeyListener(this.getControlador());
		psfContrasenia.setBounds(159, 91, 183, 20);
		contentPane.add(psfContrasenia);

		this.setLocationRelativeTo(null);
	}

	public ControladorLogin getControlador() {
		return controlador;
	}

	public void setControlador(ControladorLogin controlador) {
		this.controlador = controlador;
	}

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public void setTxtUsuario(JTextField txtUsuario) {
		this.txtUsuario = txtUsuario;
	}

	public JButton getBtnIngresar() {
		return btnIngresar;
	}

	public void setBtnIngresar(JButton btnIngresar) {
		this.btnIngresar = btnIngresar;
	}

	public JPasswordField getPsfContrasenia() {
		return psfContrasenia;
	}

	public void setPsfContrasenia(JPasswordField psfContrasenia) {
		this.psfContrasenia = psfContrasenia;
	}

}
