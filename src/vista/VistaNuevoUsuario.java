package vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorNuevoUsuario;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class VistaNuevoUsuario extends JDialog {

	private JPanel contentPane;
	private ControladorNuevoUsuario controlador;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtDni;
	private JTextField txtUsuario;
	private JTextField txtContrasenia;
	private JButton btnGuardarUsuario;
	private JTextField txtNcontacto;
	private JRadioButton rdbtnMesero;
	private JRadioButton rdbtnCocinero;
	private JTextField txtLegajo;
	private JButton btnCancelar;
	private JTextField txtEmail;
	private JComboBox cmbEmail;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					VistaNuevoUsuario frame = new VistaNuevoUsuario();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public VistaNuevoUsuario(ControladorNuevoUsuario controlador) {
		setModal(true);
		setTitle("Nuevo Usuario");
		setAutoRequestFocus(false);
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 366);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLegajo = new JLabel("Legajo:");
		lblLegajo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblLegajo.setBounds(29, 17, 112, 18);
		contentPane.add(lblLegajo);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNombre.setBounds(29, 46, 112, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblApellido.setBounds(29, 71, 112, 14);
		contentPane.add(lblApellido);
		
		JLabel lblDni = new JLabel("DNI:");
		lblDni.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDni.setBounds(29, 96, 112, 14);
		contentPane.add(lblDni);
		
		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblUsuario.setBounds(29, 121, 112, 14);
		contentPane.add(lblUsuario);
		
		JLabel lblContrasenia = new JLabel("Contrase\u00F1a:");
		lblContrasenia.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblContrasenia.setBounds(29, 146, 112, 14);
		contentPane.add(lblContrasenia);
		
		btnGuardarUsuario = new JButton("Guardar");
		btnGuardarUsuario.setBackground(Color.WHITE);
		btnGuardarUsuario.addActionListener(this.getControlador());
		btnGuardarUsuario.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnGuardarUsuario.setBounds(104, 293, 89, 23);
		contentPane.add(btnGuardarUsuario);
	    
		txtLegajo = new JTextField();
		txtLegajo.setEnabled(false);
		txtLegajo.addKeyListener(this.getControlador());
		txtLegajo.setBounds(163, 18, 49, 20);
		contentPane.add(txtLegajo);
		txtLegajo.setColumns(10);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(163, 45, 239, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setBounds(163, 70, 239, 20);
		contentPane.add(txtApellido);
		txtApellido.setColumns(10);
		
		txtDni = new JTextField();
		txtDni.setBounds(163, 95, 239, 20);
		contentPane.add(txtDni);
		txtDni.setColumns(10);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(163, 120, 239, 20);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		txtContrasenia = new JTextField();
		txtContrasenia.setBounds(163, 145, 239, 20);
		contentPane.add(txtContrasenia);
		txtContrasenia.setColumns(10);
		
		JLabel lblNcontacto = new JLabel("N\u00B0 Contacto:");
		lblNcontacto.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNcontacto.setBounds(31, 213, 92, 17);
		contentPane.add(lblNcontacto);
		
		txtNcontacto = new JTextField();
		txtNcontacto.setBounds(163, 210, 239, 20);
		contentPane.add(txtNcontacto);
		txtNcontacto.setColumns(10);
		
		rdbtnMesero = new JRadioButton("Mesero");
		rdbtnMesero.addMouseListener(this.getControlador());
		rdbtnMesero.setSelected(true);
		rdbtnMesero.setBounds(163, 237, 109, 23);
		contentPane.add(rdbtnMesero);
		
		rdbtnCocinero = new JRadioButton("Cocinero");
		rdbtnCocinero.addMouseListener(this.getControlador());
		rdbtnCocinero.setBounds(163, 263, 109, 23);
		contentPane.add(rdbtnCocinero);
		
		ButtonGroup botones = new ButtonGroup();
		botones.add(rdbtnMesero);
		botones.add(rdbtnCocinero);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(this.getControlador());
		btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnCancelar.setBackground(Color.WHITE);
		btnCancelar.setBounds(219, 293, 99, 23);
		contentPane.add(btnCancelar);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEmail.setBounds(29, 177, 46, 14);
		contentPane.add(lblEmail);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(163, 176, 123, 23);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		cmbEmail = new JComboBox();
		cmbEmail.setBackground(Color.WHITE);
		cmbEmail.setModel(new DefaultComboBoxModel(new String[] {"@hotmail.com", "@gmail.com", "@live.com"}));
		cmbEmail.setBounds(290, 176, 112, 23);
		contentPane.add(cmbEmail);
		
		this.setLocationRelativeTo(null);
	}

	
	public ControladorNuevoUsuario getControlador() {
		return controlador;
	}

	public void setControlador(ControladorNuevoUsuario controlador) {
		this.controlador = controlador;
	}


	public JTextField getTxtNombre() {
		return txtNombre;
	}


	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}


	public JTextField getTxtApellido() {
		return txtApellido;
	}


	public void setTxtApellido(JTextField txtApellido) {
		this.txtApellido = txtApellido;
	}


	public JTextField getTxtDni() {
		return txtDni;
	}


	public void setTxtDni(JTextField txtDni) {
		this.txtDni = txtDni;
	}


	public JTextField getTxtUsuario() {
		return txtUsuario;
	}


	public void setTxtUsuario(JTextField txtUsuario) {
		this.txtUsuario = txtUsuario;
	}


	public JTextField getTxtContrasenia() {
		return txtContrasenia;
	}


	public void setTxtContrasenia(JTextField txtContrasenia) {
		this.txtContrasenia = txtContrasenia;
	}



	public JButton getBtnGuardarUsuario() {
		return btnGuardarUsuario;
	}


	public void setBtnGuardarUsuario(JButton btnGuardarUsuario) {
		this.btnGuardarUsuario = btnGuardarUsuario;
	}


	public JTextField getTxtNcontacto() {
		return txtNcontacto;
	}


	public void setTxtNcontacto(JTextField txtNcontacto) {
		this.txtNcontacto = txtNcontacto;
	}


	public JRadioButton getRdbtnMesero() {
		return rdbtnMesero;
	}


	public void setRdbtnMesero(JRadioButton rdbtnMesero) {
		this.rdbtnMesero = rdbtnMesero;
	}


	public JRadioButton getRdbtnCocinero() {
		return rdbtnCocinero;
	}


	public void setRdbtnCocinero(JRadioButton rdbtnCocinero) {
		this.rdbtnCocinero = rdbtnCocinero;
	}


	public JTextField getTxtLegajo() {
		return txtLegajo;
	}


	public void setTxtLegajo(JTextField txtLegajo) {
		this.txtLegajo = txtLegajo;
	}


	public JButton getBtnCancelar() {
		return btnCancelar;
	}


	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}


	public JTextField getTxtEmail() {
		return txtEmail;
	}


	public void setTxtEmail(JTextField txtEmail) {
		this.txtEmail = txtEmail;
	}


	public JComboBox getCmbEmail() {
		return cmbEmail;
	}


	public void setCmbEmail(JComboBox cmbEmail) {
		this.cmbEmail = cmbEmail;
	}
	
	
}
