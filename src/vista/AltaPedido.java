package vista;

import controlador.ControladorAltaPedido;

import javax.swing.JScrollPane;
import java.awt.BorderLayout;

public class AltaPedido extends ABSPedidoAM {

	private ControladorAltaPedido cap;
	
	public AltaPedido(ControladorAltaPedido cap) {
		
		setTitle(" Alta Pedido");
		this.setCap(cap);
		super.getBtnOpcion1().setText("Aceptar");
		super.getBtnOpcion2().setText("Cancelar");
		addWindowListener(cap);
		super.getBtnOpcion1().addActionListener(cap);
		super.getBtnOpcion2().addActionListener(cap);
		super.getBtnBuscar().addActionListener(cap);
		super.getBtnAniadir().addActionListener(cap);
		super.getBtnQuitar().addActionListener(cap);
	}

	public ControladorAltaPedido getCap() {
		return cap;
	}

	public void setCap(ControladorAltaPedido cap) {
		this.cap = cap;
	}
	
}
