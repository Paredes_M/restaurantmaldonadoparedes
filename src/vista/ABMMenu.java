package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorMenuABM;

public class ABMMenu extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JButton btnBuscar;
	private JTable table;
	private JButton btnAltaPlatillo;
	private JButton btnBorrarPlatillo;
	private JButton btnModificarPlatillo;
	private JComboBox comboBox;
	private ControladorMenuABM controlador;
	private JTextField txtBusqueda;
	
	
	public ABMMenu (ControladorMenuABM controlador) {
		
		this.setControlador(controlador);
		addWindowListener(controlador);
		setAlwaysOnTop(true);
		setModal(true);
		setTitle("Tabla de Menu");
		setBounds(100, 100, 842, 461);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 44, 806, 285);
		contentPanel.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				"Numero", "Nombre", "Descripcion", "Precio"
			}
		));
		scrollPane.setViewportView(table);
		
		btnAltaPlatillo = new JButton("Nuevo Platillo");
		btnAltaPlatillo.addActionListener(controlador);
		btnAltaPlatillo.setForeground(Color.BLACK);
		btnAltaPlatillo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnAltaPlatillo.setBackground(Color.WHITE);
		btnAltaPlatillo.setBounds(58, 357, 149, 23);
		contentPanel.add(btnAltaPlatillo);
		
		btnModificarPlatillo = new JButton("Modificar Platillo");
		btnModificarPlatillo.addActionListener(controlador);
		btnModificarPlatillo.setForeground(Color.BLACK);
		btnModificarPlatillo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnModificarPlatillo.setBackground(Color.WHITE);
		btnModificarPlatillo.setBounds(327, 357, 162, 23);
		contentPanel.add(btnModificarPlatillo);
		
		btnBorrarPlatillo = new JButton("Borrar Platillo");
		btnBorrarPlatillo.addActionListener(controlador);
		btnBorrarPlatillo.setForeground(Color.BLACK);
		btnBorrarPlatillo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnBorrarPlatillo.setBackground(Color.WHITE);
		btnBorrarPlatillo.setBounds(620, 357, 181, 23);
		contentPanel.add(btnBorrarPlatillo);
		
		JLabel lblSeleccione = new JLabel("Filtrar por:");
		lblSeleccione.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSeleccione.setBounds(10, 11, 79, 22);
		contentPanel.add(lblSeleccione);
		
		comboBox = new JComboBox();
		comboBox.setForeground(Color.BLACK);
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 11));
		comboBox.setBackground(Color.WHITE);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Numero", "Precio", "Platillos", "Bebidas"}));
		comboBox.setBounds(83, 14, 126, 20);
		contentPanel.add(comboBox);
		
		txtBusqueda = new JTextField();
		txtBusqueda.setBounds(227, 14, 414, 20);
		contentPanel.add(txtBusqueda);
		txtBusqueda.setColumns(10);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(controlador);
		btnBuscar.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnBuscar.setForeground(Color.BLACK);
		btnBuscar.setBackground(Color.WHITE);
		btnBuscar.setBounds(667, 10, 122, 23);
		contentPanel.add(btnBuscar);
		
		table.getTableHeader().setReorderingAllowed(false);
		table.getTableHeader().setResizingAllowed(false);
		this.setLocationRelativeTo(null);
		
		
		
		
	}
	


	public ControladorMenuABM getControlador() {
		return controlador;
	}

	public void setControlador(ControladorMenuABM controlador) {
		this.controlador = controlador;
	}


	public JButton getBtnBuscar() {
		return btnBuscar;
	}
	public void setBtnBuscar(JButton btnBuscar) {
		this.btnBuscar = btnBuscar;
	}
	public JTable getTable() {
		return table;
	}
	public void setTable(JTable table) {
		this.table = table;
	}
	public JButton getBtnAltaPlatillo() {
		return btnAltaPlatillo;
	}
	public void setBtnAltaPlatillo(JButton btnAltaPlatillo) {
		this.btnAltaPlatillo = btnAltaPlatillo;
	}
	public JButton getBtnBorrarPlatillo() {
		return btnBorrarPlatillo;
	}
	public void setBtnBorrarPlatillo(JButton btnBorrarPlatillo) {
		this.btnBorrarPlatillo = btnBorrarPlatillo;
	}
	public JButton getBtnModificarPlatillo() {
		return btnModificarPlatillo;
	}
	public void setBtnModificarPlatillo(JButton btnModificarPlatillo) {
		this.btnModificarPlatillo = btnModificarPlatillo;
	}
	public JComboBox getComboBox() {
		return comboBox;
	}
	public void setComboBox(JComboBox comboBox) {
		this.comboBox = comboBox;
	}



	public JTextField getTxtBusqueda() {
		return txtBusqueda;
	}



	public void setTxtBusqueda(JTextField txtBusqueda) {
		this.txtBusqueda = txtBusqueda;
	}
}