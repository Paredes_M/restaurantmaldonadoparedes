package vista;

import controlador.ControladorModificarMenuDos;

public class ModificarMenuDos extends ABSPedidoAM {

	
	private ControladorModificarMenuDos controlador;

	public ModificarMenuDos(ControladorModificarMenuDos controlador) {
		this.setControlador(controlador);
		addWindowListener(controlador);
		
		super.getLblSeleccioneLosPlatillos().setText("Seleccione los ingredientes que desea agregar/quitar: ");
		super.getBtnOpcion1().setText("Atras");
		super.getBtnOpcion2().setText("Modificar comida");
		
		super.getBtnAniadir().addActionListener(controlador);
		super.getBtnQuitar().addActionListener(controlador);
		super.getBtnBuscar().addActionListener(controlador);
		super.getBtnMas().addActionListener(controlador);
		super.getBtnMenos().addActionListener(controlador);
		
		super.getBtnOpcion1().addActionListener(controlador);
		super.getBtnOpcion2().addActionListener(controlador);
		
		super.getTable().getColumn("Numero Platillo").setHeaderValue("Numero Ingrediente");
		super.getTable().getColumn("Nombre Platillo").setHeaderValue("Nombre Ingrediente");
		
		super.getTable_1().getColumn("Numero Platillo").setHeaderValue("Numero Ingrediente");
		super.getTable_1().getColumn("Nombre Platillo").setHeaderValue("Nombre Ingrediente");
	}
	
	public ControladorModificarMenuDos getControlador() {
		return controlador;
	}

	public void setControlador(ControladorModificarMenuDos controlador) {
		this.controlador = controlador;
	}
}
