package vista;

import javax.swing.JDialog;

import modelo.Mesa;

import javax.swing.JButton;
import javax.swing.JLabel;

import controlador.ControladorMesas;

import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Color;

public class MesaAdmin extends JDialog {
	private JButton btnMesa1;
	private JButton btnMesa2;
	private JButton btnMesa3;
	private JButton btnMesa4;
	private JButton btnMesa5;
	private JButton btnMesa6;
	private JButton btnMesa7;
	private JButton btnMesa8;
	private JButton btnMesa9;
	private JButton btnMesa10;
	private JButton btnMesa11;
	private JButton btnMesa12;
	private JButton btnMesa13;
	private JButton btnMesa14;
	private JButton btnMesa15;
	private JComboBox cmbxDisponibilidad;
	private ControladorMesas controlador;
	private ArrayList<JButton> botonesMesas;
	

	

	public MesaAdmin(ControladorMesas controlador) {
		setTitle("Mesas");
		this.setControlador(this.getControlador());
		addWindowListener(controlador);
		setBounds(100, 100, 668, 434);
		getContentPane().setLayout(null);
		this.setBotonesMesas(new ArrayList<JButton>());
		
		btnMesa1 = new Mesa(1, true);
		btnMesa1.setBackground(Color.WHITE);
		btnMesa1.setBounds(119, 96, 88, 42);
		getContentPane().add(btnMesa1);
		
		btnMesa2 = new Mesa(2, true);
		btnMesa2.setBackground(Color.WHITE);
		btnMesa2.setBounds(274, 96, 88, 42);
		getContentPane().add(btnMesa2);
		
		btnMesa3 = new Mesa(3, true);
		btnMesa3.setBackground(Color.WHITE);
		btnMesa3.setBounds(432, 96, 88, 42);
		getContentPane().add(btnMesa3);
		
		btnMesa4 = new Mesa(4, true);
		btnMesa4.setBackground(Color.WHITE);
		btnMesa4.setBounds(51, 156, 94, 42);
		getContentPane().add(btnMesa4);
		
		btnMesa5 = new Mesa(5, true);
		btnMesa5.setBackground(Color.WHITE);
		btnMesa5.setBounds(193, 156, 94, 42);
		getContentPane().add(btnMesa5);
		
		btnMesa6 = new Mesa(6, true);
		btnMesa6.setBackground(Color.WHITE);
		btnMesa6.setBounds(348, 156, 94, 42);
		getContentPane().add(btnMesa6);
		
		btnMesa7 = new Mesa(7, true);
		btnMesa7.setBackground(Color.WHITE);
		btnMesa7.setBounds(507, 156, 94, 42);
		getContentPane().add(btnMesa7);
		
		btnMesa8 = new Mesa(8, true);
		btnMesa8.setBackground(Color.WHITE);
		btnMesa8.setBounds(51, 223, 94, 42);
		getContentPane().add(btnMesa8);
		
		btnMesa9 = new Mesa(9, true);
		btnMesa9.setBackground(Color.WHITE);
		btnMesa9.setBounds(193, 223, 94, 42);
		getContentPane().add(btnMesa9);
		
		btnMesa10 = new Mesa(10, true);
		btnMesa10.setBackground(Color.WHITE);
		btnMesa10.setBounds(348, 223, 94, 42);
		getContentPane().add(btnMesa10);
		
		btnMesa11 = new Mesa(11, true);
		btnMesa11.setBackground(Color.WHITE);
		btnMesa11.setBounds(507, 223, 94, 42);
		getContentPane().add(btnMesa11);
		
		btnMesa12 = new Mesa(12, true);
		btnMesa12.setBackground(Color.WHITE);
		btnMesa12.setBounds(51, 302, 94, 42);
		getContentPane().add(btnMesa12);
		
		btnMesa13 = new Mesa(13, true);
		btnMesa13.setBackground(Color.WHITE);
		btnMesa13.setBounds(193, 302, 94, 42);
		getContentPane().add(btnMesa13);
		
		btnMesa14 = new Mesa(14, true);
		btnMesa14.setBackground(Color.WHITE);
		btnMesa14.setBounds(348, 302, 94, 42);
		getContentPane().add(btnMesa14);
		
		btnMesa15 = new Mesa(15, true);
		btnMesa15.setBackground(Color.WHITE);
		btnMesa15.setBounds(507, 302, 94, 42);
		getContentPane().add(btnMesa15);
		
		JLabel lblMesas = new JLabel("Mesas:");
		lblMesas.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMesas.setBounds(10, 18, 48, 33);
		getContentPane().add(lblMesas);
		
		cmbxDisponibilidad = new JComboBox();
		cmbxDisponibilidad.addActionListener(controlador);
		cmbxDisponibilidad.setBackground(Color.WHITE);
		cmbxDisponibilidad.setFont(new Font("Tahoma", Font.PLAIN, 15));
		cmbxDisponibilidad.setModel(new DefaultComboBoxModel(new String[] {"Todas", "Solo Disponibles", "Solo Ocupadas"}));
		cmbxDisponibilidad.setBounds(66, 22, 155, 25);
		getContentPane().add(cmbxDisponibilidad);
		
		this.getBotonesMesas().add(this.getBtnMesa1());
		this.getBotonesMesas().add(this.getBtnMesa2());
		this.getBotonesMesas().add(this.getBtnMesa3());
		this.getBotonesMesas().add(this.getBtnMesa4());
		this.getBotonesMesas().add(this.getBtnMesa5());
		this.getBotonesMesas().add(this.getBtnMesa6());
		this.getBotonesMesas().add(this.getBtnMesa7());
		this.getBotonesMesas().add(this.getBtnMesa8());
		this.getBotonesMesas().add(this.getBtnMesa9());
		this.getBotonesMesas().add(this.getBtnMesa10());
		this.getBotonesMesas().add(this.getBtnMesa11());
		this.getBotonesMesas().add(this.getBtnMesa12());
		this.getBotonesMesas().add(this.getBtnMesa13());
		this.getBotonesMesas().add(this.getBtnMesa14());
		this.getBotonesMesas().add(this.getBtnMesa15());
		
		
		this.setLocationRelativeTo(null);

	}



	public ControladorMesas getControlador() {
		return controlador;
	}



	public void setControlador(ControladorMesas controlador) {
		this.controlador = controlador;
	}



	public JButton getBtnMesa1() {
		return btnMesa1;
	}



	public void setBtnMesa1(JButton btnMesa1) {
		this.btnMesa1 = btnMesa1;
	}



	public JButton getBtnMesa2() {
		return btnMesa2;
	}



	public void setBtnMesa2(JButton btnMesa2) {
		this.btnMesa2 = btnMesa2;
	}



	public JButton getBtnMesa3() {
		return btnMesa3;
	}



	public void setBtnMesa3(JButton btnMesa3) {
		this.btnMesa3 = btnMesa3;
	}



	public JButton getBtnMesa4() {
		return btnMesa4;
	}



	public void setBtnMesa4(JButton btnMesa4) {
		this.btnMesa4 = btnMesa4;
	}



	public JButton getBtnMesa5() {
		return btnMesa5;
	}



	public void setBtnMesa5(JButton btnMesa5) {
		this.btnMesa5 = btnMesa5;
	}



	public JButton getBtnMesa6() {
		return btnMesa6;
	}



	public void setBtnMesa6(JButton btnMesa6) {
		this.btnMesa6 = btnMesa6;
	}



	public JButton getBtnMesa7() {
		return btnMesa7;
	}



	public void setBtnMesa7(JButton btnMesa7) {
		this.btnMesa7 = btnMesa7;
	}



	public JButton getBtnMesa8() {
		return btnMesa8;
	}



	public void setBtnMesa8(JButton btnMesa8) {
		this.btnMesa8 = btnMesa8;
	}



	public JButton getBtnMesa9() {
		return btnMesa9;
	}



	public void setBtnMesa9(JButton btnMesa9) {
		this.btnMesa9 = btnMesa9;
	}



	public JButton getBtnMesa10() {
		return btnMesa10;
	}



	public void setBtnMesa10(JButton btnMesa10) {
		this.btnMesa10 = btnMesa10;
	}



	public JButton getBtnMesa11() {
		return btnMesa11;
	}



	public void setBtnMesa11(JButton btnMesa11) {
		this.btnMesa11 = btnMesa11;
	}



	public JButton getBtnMesa12() {
		return btnMesa12;
	}



	public void setBtnMesa12(JButton btnMesa12) {
		this.btnMesa12 = btnMesa12;
	}



	public JButton getBtnMesa13() {
		return btnMesa13;
	}



	public void setBtnMesa13(JButton btnMesa13) {
		this.btnMesa13 = btnMesa13;
	}



	public JButton getBtnMesa14() {
		return btnMesa14;
	}



	public void setBtnMesa14(JButton btnMesa14) {
		this.btnMesa14 = btnMesa14;
	}



	public JButton getBtnMesa15() {
		return btnMesa15;
	}



	public void setBtnMesa15(JButton btnMesa15) {
		this.btnMesa15 = btnMesa15;
	}



	public JComboBox getCmbxDisponibilidad() {
		return cmbxDisponibilidad;
	}



	public void setCmbxDisponibilidad(JComboBox cmbxDisponibilidad) {
		this.cmbxDisponibilidad = cmbxDisponibilidad;
	}



	public ArrayList<JButton> getBotonesMesas() {
		return botonesMesas;
	}



	public void setBotonesMesas(ArrayList<JButton> botonesMesas) {
		this.botonesMesas = botonesMesas;
	}
}
