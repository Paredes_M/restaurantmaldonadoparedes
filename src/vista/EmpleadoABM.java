package vista;

import java.awt.Color;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import controlador.ControladorEmpleadoABM;
import controlador.ControladorPedido;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;

public class EmpleadoABM extends ABSABM {

	private ControladorEmpleadoABM controladorEmp;
	private JTextField txtBusqueda;
	private JButton btnBuscar;
	private JComboBox cbxFiltro;


	public EmpleadoABM(ControladorEmpleadoABM controlador) {
		super();
		addWindowListener(controlador);
		getBtnBorrar().setBackground(Color.WHITE);
		getBtnModificar().setBackground(Color.WHITE);
		getBtnAgregar().setBackground(Color.WHITE);
		this.setControladorEmp(controlador);
		addWindowListener(controlador);
		super.setSize(582, 330);
		getBtnBorrar().setLocation(408, 249);
		getBtnModificar().setLocation(210, 249);
		getBtnAgregar().setLocation(20, 249);
		getScrollPane().setBounds(10, 42, 556, 196);
		setAlwaysOnTop(false);
		getTable().setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		super.getBtnAgregar().setText("Agregar Empleado");
		super.getBtnModificar().setText("Modificar Empleado");
		super.getBtnBorrar().setText("Borrar Empleado");
		super.getBtnAgregar().addMouseListener(this.getControladorEmp());
		super.getBtnModificar().addMouseListener(this.getControladorEmp());
		super.getBtnBorrar().addMouseListener(this.getControladorEmp());
		
		getScrollPane().setBackground(Color.WHITE);
		
		JLabel lblBuscar = new JLabel("Buscar Por:");
		lblBuscar.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblBuscar.setBounds(10, 11, 71, 20);
		getContentPane().add(lblBuscar);
		
		cbxFiltro = new JComboBox();
		cbxFiltro.setBackground(Color.WHITE);
		cbxFiltro.setModel(new DefaultComboBoxModel(new String[] {"Nombre", "Apellido", "Tipo Empleado"}));
		cbxFiltro.setBounds(89, 11, 146, 20);
		getContentPane().add(cbxFiltro);
		
		txtBusqueda = new JTextField();
		txtBusqueda.setBounds(245, 11, 213, 20);
		getContentPane().add(txtBusqueda);
		txtBusqueda.setColumns(10);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.addMouseListener(controlador);
		btnBuscar.setBackground(Color.WHITE);
		btnBuscar.setBounds(468, 11, 98, 20);
		getContentPane().add(btnBuscar);
		super.setTitle("Empleados");
		super.getTable().setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null,null,null, null},
				{null, null, null, null,null,null, null},
				{null, null, null, null,null,null, null},
				{null, null, null, null,null,null, null},
				{null, null, null, null,null,null, null},
				{null, null, null, null,null,null, null},
				{null, null, null, null,null,null, null},
				{null, null, null, null,null,null, null},
				{null, null, null, null,null,null, null},
				{null, null, null, null,null,null, null},
				{null, null, null, null,null,null, null},
			},
			new String[] {
				"Legajo", "Nombre ", "Apellido", "Dni ","Numero de Contacto", "Email","Tipo de Empleado"}
			));
		
		TableColumnModel modeloColumna = super.getTable().getColumnModel();
		modeloColumna.getColumn(0).setPreferredWidth(70);
		modeloColumna.getColumn(1).setPreferredWidth(150);
		modeloColumna.getColumn(2).setPreferredWidth(150);
		modeloColumna.getColumn(3).setPreferredWidth(100);
		modeloColumna.getColumn(4).setPreferredWidth(150);
		modeloColumna.getColumn(5).setPreferredWidth(200);
		modeloColumna.getColumn(6).setPreferredWidth(150);
	}

	
	
	public ControladorEmpleadoABM getControladorEmp() {
		return controladorEmp;
	}

	public void setControladorEmp(ControladorEmpleadoABM controladorEmp) {
		this.controladorEmp = controladorEmp;
	}



	public JTextField getTxtBusqueda() {
		return txtBusqueda;
	}



	public void setTxtBusqueda(JTextField txtBusqueda) {
		this.txtBusqueda = txtBusqueda;
	}



	public JButton getBtnBuscar() {
		return btnBuscar;
	}



	public void setBtnBuscar(JButton btnBuscar) {
		this.btnBuscar = btnBuscar;
	}



	public JComboBox getCbxFiltro() {
		return cbxFiltro;
	}



	public void setCbxFiltro(JComboBox cbxFiltro) {
		this.cbxFiltro = cbxFiltro;
	}
}
