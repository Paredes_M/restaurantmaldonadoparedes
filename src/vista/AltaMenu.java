package vista;

import controlador.ControladorAltaMenu;

public class AltaMenu extends ABSMenuAM {

	private ControladorAltaMenu controlador;

	public AltaMenu(ControladorAltaMenu controlador) {

		this.setControlador(controlador);
		super.setTitle("Nueva comida");
		super.getBtnSiguiente().addActionListener(controlador);
		super.getBtnCancelar().addActionListener(controlador);

	}

	public ControladorAltaMenu getControlador() {
		return controlador;
	}

	public void setControlador(ControladorAltaMenu controlador) {
		this.controlador = controlador;
	}

}
