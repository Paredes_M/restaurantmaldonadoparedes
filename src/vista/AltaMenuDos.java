package vista;

import controlador.ControladorAltaMenuDos;

public class AltaMenuDos extends ABSPedidoAM {
	
	private ControladorAltaMenuDos controlador;
	
	public AltaMenuDos(ControladorAltaMenuDos controlador) {
		
		this.setControlador(controlador);
		addWindowListener(controlador);
		super.setTitle("Agregar Receta");
		super.getLblSeleccioneLosPlatillos().setText("Seleccione los ingredientes que desea agregar/quitar: ");
		super.getBtnOpcion1().setText("Atras");
		super.getBtnOpcion2().setText("Crear comida");
		
		super.getBtnAniadir().addActionListener(controlador);
		super.getBtnQuitar().addActionListener(controlador);
		super.getBtnBuscar().addActionListener(controlador);
		super.getBtnMas().addActionListener(controlador);
		super.getBtnMenos().addActionListener(controlador);
		
		super.getBtnOpcion1().addActionListener(controlador);
		super.getBtnOpcion2().addActionListener(controlador);
		
		super.getTable().getColumn("Numero Platillo").setHeaderValue("Numero Ingrediente");
		super.getTable().getColumn("Nombre Platillo").setHeaderValue("Nombre Ingrediente");
		
		super.getTable_1().getColumn("Numero Platillo").setHeaderValue("Numero Ingrediente");
		super.getTable_1().getColumn("Nombre Platillo").setHeaderValue("Nombre Ingrediente");
	}

	public ControladorAltaMenuDos getControlador() {
		return controlador;
	}

	public void setControlador(ControladorAltaMenuDos controlador) {
		this.controlador = controlador;
	}
	

}
