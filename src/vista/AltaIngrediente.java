package vista;

import controlador.ControladorAltaIngrediente;

public class AltaIngrediente extends ABSIngredienteAM {
	
	private ControladorAltaIngrediente controlador;
	
	public AltaIngrediente(ControladorAltaIngrediente controlador) {
		
		this.setControlador(controlador);
		super.setTitle("Alta ingrediente");
		super.getBtnAceptar().setText("Agregar Ingrediente");
		super.getBtnCancelar().setText("Cancelar");
		super.getBtnAceptar().addActionListener(controlador);
		super.getBtnCancelar().addActionListener(controlador);
	}

	public ControladorAltaIngrediente getControlador() {
		return controlador;
	}

	public void setControlador(ControladorAltaIngrediente controlador) {
		this.controlador = controlador;
	}

}
