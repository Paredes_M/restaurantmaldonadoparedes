package vista;

import controlador.ControladorAltaEmpleado;

public class AltaEmpleado extends ABSEmpleadoAM {

	private ControladorAltaEmpleado controlador;

	public AltaEmpleado(ControladorAltaEmpleado controlador) {
		getTxtLegajo().setEnabled(true);
		this.setControlador(controlador);
		super.getBtnGuardarUsuario().addActionListener(controlador);
		super.getBtnCancelar().addActionListener(controlador);
		super.getTxtLegajo().addKeyListener(controlador);
		super.getTxtDni().addKeyListener(controlador);
	}

	
	public ControladorAltaEmpleado getControlador() {
		return controlador;
	}

	public void setControlador(ControladorAltaEmpleado controlador) {
		this.controlador = controlador;
	}


	
}
