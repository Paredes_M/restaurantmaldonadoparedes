package vista;

import controlador.ControladorModificarPedido;

public class ModificarPedido extends ABSPedidoAM {

	private ControladorModificarPedido cmp;
	
	public ModificarPedido(ControladorModificarPedido cmp) {
		setTitle("Modificar Pedido");
		this.setCmp(cmp);
		super.getBtnOpcion1().setText("Aceptar");
		super.getBtnOpcion2().setText("Cancelar");
		super.getBtnOpcion1().addActionListener(cmp);
		super.getBtnOpcion2().addActionListener(cmp);
		super.getBtnBuscar().addActionListener(cmp);
		super.getBtnAniadir().addActionListener(cmp);
		super.getBtnQuitar().addActionListener(cmp);
		addWindowListener(cmp);
	}

	public ControladorModificarPedido getCmp() {
		return cmp;
	}

	public void setCmp(ControladorModificarPedido cap) {
		this.cmp = cap;
	}
	
}
