package vista;

import javax.swing.JDialog;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JRadioButton;

public abstract class ABSMenuAM extends JDialog {
	private JTextField txtNumeroComida;
	private JTextField txtNombreComida;
	private JTextField txtPrecioUnitario;
	private JButton btnAtras;
	private JButton btnSiguiente;
	private JButton btnCancelar;
	private JScrollPane scrollPane;
	private JTextPane textDescripcion;
	private JRadioButton rdbtnPlatilloSi;
	private JRadioButton rdbtnPlatilloNo;


	public ABSMenuAM() {
		setModal(true);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setAlwaysOnTop(true);

		crearVista();
	}

	private void crearVista() {

		setBounds(100, 100, 600, 459);
		getContentPane().setLayout(null);

		JLabel lblNumeroComida = new JLabel("N\u00FAmero Comida:");
		lblNumeroComida.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNumeroComida.setBounds(41, 28, 115, 35);
		getContentPane().add(lblNumeroComida);

		JLabel lblNombreComida = new JLabel("Nombre Comida:");
		lblNombreComida.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNombreComida.setBounds(41, 74, 115, 34);
		getContentPane().add(lblNombreComida);

		JLabel lblDescripcion = new JLabel("Descripci\u00F3n:");
		lblDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDescripcion.setBounds(41, 119, 80, 35);
		getContentPane().add(lblDescripcion);

		JLabel lblPlatillo = new JLabel("Platillo:");
		lblPlatillo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPlatillo.setBounds(41, 300, 80, 23);
		getContentPane().add(lblPlatillo);

		txtNumeroComida = new JTextField();
		txtNumeroComida.setBounds(158, 37, 146, 20);
		getContentPane().add(txtNumeroComida);
		txtNumeroComida.setColumns(10);

		txtNombreComida = new JTextField();
		txtNombreComida.setColumns(10);
		txtNombreComida.setBounds(158, 84, 349, 20);
		getContentPane().add(txtNombreComida);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(158, 130, 349, 155);
		getContentPane().add(scrollPane);

		textDescripcion = new JTextPane();
		scrollPane.setViewportView(textDescripcion);

		btnAtras = new JButton("Atr\u00E1s");
		btnAtras.setEnabled(false);
		btnAtras.setBackground(Color.WHITE);
		btnAtras.setBounds(282, 386, 89, 23);
		getContentPane().add(btnAtras);

		btnSiguiente = new JButton("Siguiente");
		btnSiguiente.setBackground(Color.WHITE);
		btnSiguiente.setBounds(381, 386, 89, 23);
		getContentPane().add(btnSiguiente);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBackground(Color.WHITE);
		btnCancelar.setBounds(480, 386, 89, 23);
		getContentPane().add(btnCancelar);

		JLabel lblPrecioUnitario = new JLabel("Precio Unitario:");
		lblPrecioUnitario.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPrecioUnitario.setBounds(41, 341, 115, 23);
		getContentPane().add(lblPrecioUnitario);

		txtPrecioUnitario = new JTextField();
		txtPrecioUnitario.setBounds(157, 344, 147, 20);
		getContentPane().add(txtPrecioUnitario);
		txtPrecioUnitario.setColumns(10);
		
		rdbtnPlatilloSi = new JRadioButton("SI");
		rdbtnPlatilloSi.setBounds(158, 302, 49, 23);
		getContentPane().add(rdbtnPlatilloSi);
		
		rdbtnPlatilloNo = new JRadioButton("NO");
		rdbtnPlatilloNo.setBounds(209, 302, 49, 23);
		getContentPane().add(rdbtnPlatilloNo);
		
		ButtonGroup botones = new ButtonGroup();
		botones.add(rdbtnPlatilloSi);
		botones.add(rdbtnPlatilloNo);

		this.setLocationRelativeTo(null);
	}

	public JTextField getTxtNumeroComida() {
		return txtNumeroComida;
	}

	public void setTxtNumeroComida(JTextField txtNumeroComida) {
		this.txtNumeroComida = txtNumeroComida;
	}

	public JTextField getTxtNombreComida() {
		return txtNombreComida;
	}

	public void setTxtNombreComida(JTextField txtNombreComida) {
		this.txtNombreComida = txtNombreComida;
	}

	public JTextField getTxtPrecioUnitario() {
		return txtPrecioUnitario;
	}

	public void setTxtPrecioUnitario(JTextField txtPrecioUnitario) {
		this.txtPrecioUnitario = txtPrecioUnitario;
	}

	public JButton getBtnAtras() {
		return btnAtras;
	}

	public void setBtnAtras(JButton btnAtras) {
		this.btnAtras = btnAtras;
	}

	public JButton getBtnSiguiente() {
		return btnSiguiente;
	}

	public void setBtnSiguiente(JButton btnSiguiente) {
		this.btnSiguiente = btnSiguiente;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public JTextPane getTextDescripcion() {
		return textDescripcion;
	}

	public void setTextDescripcion(JTextPane textDescripcion) {
		this.textDescripcion = textDescripcion;
	}

	public JRadioButton getRdbtnPlatilloSi() {
		return rdbtnPlatilloSi;
	}

	public void setRdbtnPlatilloSi(JRadioButton rdbtnPlatilloSi) {
		this.rdbtnPlatilloSi = rdbtnPlatilloSi;
	}

	public JRadioButton getRdbtnPlatilloNo() {
		return rdbtnPlatilloNo;
	}

	public void setRdbtnPlatilloNo(JRadioButton rdbtnPlatilloNo) {
		this.rdbtnPlatilloNo = rdbtnPlatilloNo;
	}
}
