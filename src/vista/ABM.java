package vista;

import javax.swing.JDialog;
import javax.swing.JPanel;

import controlador.ControladorPedido;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.Cursor;
import java.awt.Color;

public class ABM extends JDialog{
	
	private ControladorPedido controladorP;
	private JTable table;
	private JPanel contentPane;
	
	public ABM(ControladorPedido controladorP) {
		setModal(true);
		setResizable(false);
		setAlwaysOnTop(true);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		this.setControladorP(controladorP);
		setTitle("Pedidos de mesa");
		setBounds(100, 100, 450, 323);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 414, 196);
	    contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				"Numero Pedido", "Numero Mesa", "Estado", "Descripcion"
			}
		));
		scrollPane.setViewportView(table);
		
		JButton btnNuevoPedido = new JButton("Nuevo Pedido");
		btnNuevoPedido.setBackground(Color.WHITE);
		btnNuevoPedido.setBounds(10, 229, 129, 23);
		getContentPane().add(btnNuevoPedido);
		
		JButton btnModificarPedido = new JButton("Modificar Pedido");
		btnModificarPedido.setBackground(Color.WHITE);
		btnModificarPedido.setBounds(149, 229, 140, 23);
		getContentPane().add(btnModificarPedido);
		
		JButton btnBorrarPedido = new JButton("Borrar Pedido");
		btnBorrarPedido.setBackground(Color.WHITE);
		btnBorrarPedido.setBounds(299, 229, 125, 23);
		getContentPane().add(btnBorrarPedido);
		
		table.getTableHeader().setReorderingAllowed(false);
		table.getTableHeader().setResizingAllowed(false);
		this.setLocationRelativeTo(null);
	}

	public ControladorPedido getControladorP() {
		return controladorP;
	}
	public void setControladorP(ControladorPedido controladorP) {
		this.controladorP = controladorP;
	}


}
