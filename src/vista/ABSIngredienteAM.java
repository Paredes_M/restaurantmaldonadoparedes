package vista;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;

public abstract class ABSIngredienteAM extends JDialog {
	private JTextField txtNumeroIngrediente;
	private JTextField txtNombreIngrediente;
	private JTextField txtCantidad;
	private JTextField txtStock;
	private JRadioButton rdbtnEsBebidaSi;
	private JRadioButton rdbtnEsBebidaNo;
	private JRadioButton rdbtnEsEnvasadoSI;
	private JRadioButton rdbtnEsEnvasadoNo;
	private JTextPane textDescripcion;
	private JScrollPane scrollPane;
	private JButton btnCancelar;
	private JButton btnAceptar;


	public ABSIngredienteAM() {
		setModal(true);
		crearVista();
	}
	
	private void crearVista() {
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setAlwaysOnTop(true);
		setBounds(100, 100, 620, 514);
		getContentPane().setLayout(null);
		
		JLabel lblNumeroIngrediente = new JLabel("Numero Ingrediente:");
		lblNumeroIngrediente.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNumeroIngrediente.setBounds(40, 32, 137, 19);
		getContentPane().add(lblNumeroIngrediente);
		
		txtNumeroIngrediente = new JTextField();
		txtNumeroIngrediente.setBounds(217, 31, 137, 20);
		getContentPane().add(txtNumeroIngrediente);
		txtNumeroIngrediente.setColumns(10);
		
		txtNombreIngrediente = new JTextField();
		txtNombreIngrediente.setBounds(217, 70, 207, 23);
		getContentPane().add(txtNombreIngrediente);
		txtNombreIngrediente.setColumns(10);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(215, 113, 356, 127);
		getContentPane().add(scrollPane);
		
	    textDescripcion = new JTextPane();
		scrollPane.setViewportView(textDescripcion);
		
		JLabel lblNombreIngrediente = new JLabel("Nombre Ingrediente:");
		lblNombreIngrediente.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNombreIngrediente.setBounds(40, 72, 167, 21);
		getContentPane().add(lblNombreIngrediente);
		
		JLabel lblDescripcion = new JLabel("Descripci\u00F3n:");
		lblDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDescripcion.setBounds(40, 113, 117, 14);
		getContentPane().add(lblDescripcion);
		
		JLabel lblEsBebida = new JLabel("Es Bebida?");
		lblEsBebida.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEsBebida.setBounds(40, 259, 89, 19);
		getContentPane().add(lblEsBebida);
		
		JLabel lblCantidadActual = new JLabel("Cantidad Actual:");
		lblCantidadActual.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCantidadActual.setBounds(40, 354, 117, 14);
		getContentPane().add(lblCantidadActual);
		
		txtCantidad = new JTextField();
		txtCantidad.setBounds(217, 353, 137, 20);
		getContentPane().add(txtCantidad);
		txtCantidad.setColumns(10);
		
		btnCancelar = new JButton("");
		btnCancelar.setBackground(Color.WHITE);
		btnCancelar.setBounds(315, 441, 89, 23);
		getContentPane().add(btnCancelar);
		
		btnAceptar = new JButton("");
		btnAceptar.setBackground(Color.WHITE);
		btnAceptar.setBounds(426, 441, 155, 23);
		getContentPane().add(btnAceptar);
		
		JLabel lblEsEnvasado = new JLabel("Es Envasado?");
		lblEsEnvasado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEsEnvasado.setBounds(40, 302, 89, 20);
		getContentPane().add(lblEsEnvasado);
		
		JLabel lblStockMinimo = new JLabel("Stock M\u00EDnimo:");
		lblStockMinimo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblStockMinimo.setBounds(40, 402, 117, 14);
		getContentPane().add(lblStockMinimo);
		
		txtStock = new JTextField();
		txtStock.setBounds(217, 401, 137, 21);
		getContentPane().add(txtStock);
		txtStock.setColumns(10);
		
		rdbtnEsBebidaSi = new JRadioButton("Si");
		rdbtnEsBebidaSi.setBounds(212, 259, 45, 23);
		getContentPane().add(rdbtnEsBebidaSi);
		
		rdbtnEsBebidaNo = new JRadioButton("No");
		rdbtnEsBebidaNo.setBounds(271, 259, 45, 23);
		getContentPane().add(rdbtnEsBebidaNo);
		
		rdbtnEsEnvasadoSI = new JRadioButton("Si");
		rdbtnEsEnvasadoSI.setBounds(212, 303, 45, 23);
		getContentPane().add(rdbtnEsEnvasadoSI);
		
		rdbtnEsEnvasadoNo = new JRadioButton("No");
		rdbtnEsEnvasadoNo.setBounds(271, 303, 45, 23);
		getContentPane().add(rdbtnEsEnvasadoNo);
		
		ButtonGroup botones1 = new ButtonGroup();
		botones1.add(rdbtnEsBebidaSi);
		botones1.add(rdbtnEsBebidaNo);
		
		ButtonGroup botones2 = new ButtonGroup();
		botones2.add(rdbtnEsEnvasadoSI);
		botones2.add(rdbtnEsEnvasadoNo);

		this.setLocationRelativeTo(null);
	}

	public JTextField getTxtNumeroIngrediente() {
		return txtNumeroIngrediente;
	}

	public void setTxtNumeroIngrediente(JTextField txtNumeroIngrediente) {
		this.txtNumeroIngrediente = txtNumeroIngrediente;
	}

	public JTextField getTxtNombreIngrediente() {
		return txtNombreIngrediente;
	}

	public void setTxtNombreIngrediente(JTextField txtNombreIngrediente) {
		this.txtNombreIngrediente = txtNombreIngrediente;
	}

	public JTextField getTxtCantidad() {
		return txtCantidad;
	}

	public void setTxtCantidad(JTextField txtCantidad) {
		this.txtCantidad = txtCantidad;
	}

	public JTextField getTxtStock() {
		return txtStock;
	}

	public void setTxtStock(JTextField txtStock) {
		this.txtStock = txtStock;
	}

	public JRadioButton getRdbtnEsBebidaSi() {
		return rdbtnEsBebidaSi;
	}

	public void setRdbtnEsBebidaSi(JRadioButton rdbtnEsBebidaSi) {
		this.rdbtnEsBebidaSi = rdbtnEsBebidaSi;
	}

	public JRadioButton getRdbtnEsBebidaNo() {
		return rdbtnEsBebidaNo;
	}

	public void setRdbtnEsBebidaNo(JRadioButton rdbtnEsBebidaNo) {
		this.rdbtnEsBebidaNo = rdbtnEsBebidaNo;
	}

	public JRadioButton getRdbtnEsEnvasadoSI() {
		return rdbtnEsEnvasadoSI;
	}

	public void setRdbtnEsEnvasadoSI(JRadioButton rdbtnEsEnvasadoSI) {
		this.rdbtnEsEnvasadoSI = rdbtnEsEnvasadoSI;
	}

	public JRadioButton getRdbtnEsEnvasadoNo() {
		return rdbtnEsEnvasadoNo;
	}

	public void setRdbtnEsEnvasadoNo(JRadioButton rdbtnEsEnvasadoNo) {
		this.rdbtnEsEnvasadoNo = rdbtnEsEnvasadoNo;
	}

	public JTextPane getTextDescripcion() {
		return textDescripcion;
	}

	public void setTextDescripcion(JTextPane textDescripcion) {
		this.textDescripcion = textDescripcion;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public void setScrollPane(JScrollPane scrollPane) {
		this.scrollPane = scrollPane;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}

	
}
