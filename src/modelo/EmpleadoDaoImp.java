package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class EmpleadoDaoImp implements EmpleadoDao {

	@Override
	public Boolean agregarEmpleado(Empleado e) {
		ArrayList<String> parametros = new ArrayList<String>();
		parametros.add(String.valueOf(e.getLegajo()));
		parametros.add(e.getNombre());
		parametros.add(e.getApellido());
		parametros.add(e.getDni());
		parametros.add(e.getNum_contacto());
		parametros.add(e.getEmail());
		parametros.add(e.getUsuario());
		parametros.add(e.getContrasenia());
		parametros.add(e.getTipo_empleado());
		String consulta = "insert into restaurante.empleado(legajo,nombre,apellido,dni,num_contacto,email,usuario,contraseņa,tipo_empleado)"
				+ " values(?,?,?,?,?,?,?,?,?)";
		return BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Boolean modificarEmpleado(Empleado e) {
		ArrayList<String> parametros = new ArrayList<String>();
		parametros.add(e.getNombre());
		parametros.add(e.getApellido());
		parametros.add(e.getDni());
		parametros.add(e.getNum_contacto());
		parametros.add(e.getEmail());
		parametros.add(e.getUsuario());
		parametros.add(e.getContrasenia());
		parametros.add(e.getTipo_empleado());
		parametros.add(String.valueOf(e.getLegajo()));
		String consulta = "update restaurante.empleado set nombre=?,apellido=?,dni=?,num_contacto=?,email=?,usuario=?,contraseņa=?,tipo_empleado=? where legajo=?";
		return BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Boolean borrarEmpleado(String legajo) {
		ArrayList<String> parametros = new ArrayList<String>();
		parametros.add(legajo);
		String consulta = "delete from restaurante.empleado where legajo=?";
		return BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Empleado buscarEmpleado(String legajo) {
		ArrayList<Empleado> emp = new ArrayList<Empleado>();
		Empleado e = null;
		emp = this.buscarEmpleados();
		for (Empleado empleado : emp) {
			if (empleado.getLegajo() == Integer.valueOf(legajo)) {
				e = empleado;
			}
		}
		return e;
	}

	@Override
	public ArrayList<Empleado> buscarEmpleados() {
		String consulta = "select * from restaurante.empleado";
		ArrayList<Empleado> emp = new ArrayList<Empleado>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Empleado e;
				e = new Empleado(Integer.parseInt(rs.getString("legajo")), rs.getString("nombre"),
						rs.getString("apellido"), rs.getString("dni"), rs.getString("num_contacto"),
						rs.getString("email"), rs.getString("usuario"), rs.getString("contraseņa"),
						rs.getString("tipo_empleado"));

				emp.add(e);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return emp;
	}

	@Override
	public ArrayList<Empleado> buscarEmpleadosPorNombre(String nombre) {
		String consulta = "select * from restaurante.empleado where nombre like '%" + nombre + "%' order by nombre";
		ArrayList<Empleado> emp = new ArrayList<Empleado>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Empleado e;

				e = new Empleado(Integer.parseInt(rs.getString("legajo")), rs.getString("nombre"),
						rs.getString("apellido"), rs.getString("dni"), rs.getString("num_contacto"),
						rs.getString("email"), rs.getString("usuario"), rs.getString("contraseņa"),
						rs.getString("tipo_empleado"));

				emp.add(e);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return emp;
	}

	@Override
	public ArrayList<Empleado> buscarEmpleadosPorApellido(String apellido) {
		String consulta = "select * from restaurante.empleado where apellido like '%" + apellido
				+ "%' order by apellido";
		ArrayList<Empleado> emp = new ArrayList<Empleado>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Empleado e;

				e = new Empleado(Integer.parseInt(rs.getString("legajo")), rs.getString("nombre"),
						rs.getString("apellido"), rs.getString("dni"), rs.getString("num_contacto"),
						rs.getString("email"), rs.getString("usuario"), rs.getString("contraseņa"),
						rs.getString("tipo_empleado"));

				emp.add(e);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return emp;
	}

	@Override
	public ArrayList<Empleado> buscarEmpleadosPorTipoEmpleado(String tipo_empleado) {
		String consulta = "select * from restaurante.empleado where tipo_empleado like '%" + tipo_empleado
				+ "%' order by legajo";
		ArrayList<Empleado> emp = new ArrayList<Empleado>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Empleado e;

				e = new Empleado(Integer.parseInt(rs.getString("legajo")), rs.getString("nombre"),
						rs.getString("apellido"), rs.getString("dni"), rs.getString("num_contacto"),
						rs.getString("email"), rs.getString("usuario"), rs.getString("contraseņa"),
						rs.getString("tipo_empleado"));

				emp.add(e);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return emp;
	}

	@Override
	public Empleado buscarEmpleadoPorUsuarioContraseņa(String usuario, String contraseņa) {
		ArrayList<Empleado> emp = new ArrayList<Empleado>();
		Empleado e = new Empleado();
		emp = this.buscarEmpleados();
		for (Empleado empleado : emp) {
			if (empleado.getUsuario().equals(usuario) && empleado.getContrasenia().equals(contraseņa)) {
				e = empleado;
			}
		}
		return e;
	}
}
