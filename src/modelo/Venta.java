package modelo;

import java.time.LocalDate;
import java.time.LocalTime;

public class Venta {

	private Integer numero_Venta;
	private Integer legajo_Empleado;
	private LocalDate fechaVenta;
	private LocalTime horaVenta;
	private Double totalApagar;

	public Venta(Integer numero_Venta, Integer legajo_Empleado, Double totalApagar, LocalDate fechaVenta, LocalTime horaVenta) {
		super();
		this.setNumero_Venta(numero_Venta);
		this.setLegajo_Empleado(legajo_Empleado);
		this.setTotalApagar(totalApagar);
		this.setFechaVenta(fechaVenta);
		this.setHoraVenta(horaVenta);
	}

	public LocalDate getFechaVenta() {
		return fechaVenta;
	}

	public void setFechaVenta(LocalDate fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public Double getTotalApagar() {
		return totalApagar;
	}

	public void setTotalApagar(Double totalApagar) {
		this.totalApagar = totalApagar;
	}

	public LocalTime getHoraVenta() {
		return horaVenta;
	}

	public void setHoraVenta(LocalTime horaVenta) {
		this.horaVenta = horaVenta;
	}

	public Integer getNumero_Venta() {
		return numero_Venta;
	}

	public void setNumero_Venta(Integer numero_Venta) {
		this.numero_Venta = numero_Venta;
	}

	public Integer getLegajo_Empleado() {
		return legajo_Empleado;
	}

	public void setLegajo_Empleado(Integer legajo_Empleado) {
		this.legajo_Empleado = legajo_Empleado;
	}
	

}
