package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class VentaDaoImp implements VentaDao {

	@Override
	public Boolean agregarventa(Venta v) {
		ArrayList<String> parametros = new ArrayList<String>();
		parametros.add(String.valueOf(v.getNumero_Venta()));
		parametros.add(String.valueOf(v.getLegajo_Empleado()));
		parametros.add(String.valueOf(v.getTotalApagar()));
		parametros.add(String.valueOf(v.getFechaVenta()));
		parametros.add(String.valueOf(v.getHoraVenta()));
		String consulta = "insert into restaurante.venta(num_venta,legajo,total_pagar,fecha_venta,hora_venta)"
				+ " values(?,?,?,?,?)";
		return BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Boolean modificarVenta(Venta v) {
		ArrayList<String> parametros = new ArrayList<String>();
		parametros.add(String.valueOf(v.getLegajo_Empleado()));
		parametros.add(String.valueOf(v.getTotalApagar()));
		parametros.add(String.valueOf(v.getFechaVenta()));
		parametros.add(String.valueOf(v.getHoraVenta()));
		parametros.add(String.valueOf(v.getNumero_Venta()));
		String consulta = "update restaurante.venta set legajo=?,total_pagar=?,fecha_venta=?,hora_venta=? where num_venta=?";
		return BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Boolean borrarVenta(String num_venta) {
		ArrayList<String> parametros = new ArrayList<String>();
		parametros.add(num_venta);
		String consulta = "delete from restaurante.venta where num_venta=?";
		return BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Venta buscarVentaPorNumero(String numero_venta) {
		ArrayList<Venta> ventas = new ArrayList<Venta>();
		Venta v = null;
		ventas = this.buscarVentas();
		for (Venta venta : ventas) {
			if (venta.getNumero_Venta() == Integer.valueOf(numero_venta)) {
				v = venta;
			}
		}
		return v;
	}

	@Override
	public ArrayList<Venta> buscarVentas() {
		String consulta = "select * from restaurante.venta";
		ArrayList<Venta> ventas = new ArrayList<Venta>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Venta v;
				v = new Venta(Integer.parseInt(rs.getString("num_venta")), Integer.parseInt(rs.getString("legajo")),
						Double.parseDouble(rs.getString("total_pagar")), LocalDate.parse(rs.getString("fecha_venta")),
						LocalTime.parse(rs.getString("hora_venta")));
				ventas.add(v);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ventas;
	}

	@Override
	public ArrayList<Venta> VentadelaFecha(String fecha_venta) {
		String consulta = "select * from restaurante.venta where feha_venta like '%" + fecha_venta + "%'";
		ArrayList<Venta> ventas = new ArrayList<Venta>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Venta v;
				v = new Venta(Integer.parseInt(rs.getString("num_venta")), Integer.parseInt(rs.getString("legajo")),
						Double.parseDouble(rs.getString("total_pagar")), LocalDate.parse(rs.getString("fecha_venta")),
						LocalTime.parse(rs.getString("hora_venta")));
				ventas.add(v);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ventas;
		
	}

	@Override
	public ArrayList<Venta> VentadeMesero(String legajo) {
		String consulta = "select * from restaurante.venta where legajo='" + legajo + "'";
		ArrayList<Venta> ventas = new ArrayList<Venta>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Venta v;
				v = new Venta(Integer.parseInt(rs.getString("num_venta")), Integer.parseInt(rs.getString("legajo")),
						Double.parseDouble(rs.getString("total_pagar")), LocalDate.parse(rs.getString("fecha_venta")),
						LocalTime.parse(rs.getString("hora_venta")));
				ventas.add(v);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ventas;
	}

	@Override
	public Integer nuevoNumVenta() {
		String consulta = "SELECT * FROM restaurante.venta WHERE num_venta = (SELECT MAX(num_venta) FROM restaurante.venta) ORDER BY num_venta";
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		Integer num_max = 0;
		try {
			while (rs.next()) {
				num_max = Integer.parseInt(rs.getString("num_venta"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return num_max + 1;
	}

	@Override
	public void BorrarVentasVacias() {
		String consulta = "select * from restaurante.venta where total_pagar = 0";
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				this.borrarVenta(rs.getString("num_venta"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
