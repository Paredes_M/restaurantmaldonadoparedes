package modelo;

import java.util.ArrayList;

public interface ComidaDao {

	// Altas, Bajas y Modificaciones (ABM)
	Boolean agregarComida(Comida c);

	Boolean modificarComida(Comida c);

	Boolean borrarComida(String num_comida);

	//
	Comida buscarComidaPorNumero(String numero_comida);

	ArrayList<Comida> buscarComidaPorNombre(String nombre_comida);

	ArrayList<Comida> buscarComidas();

	ArrayList<Comida> buscarPecio_Comida();

	ArrayList<Comida> buscarEs_Platillo();

	ArrayList<Comida> buscarEs_Bebida();

	
//----------------------AGREGADO EL 16/11	
	ArrayList<Receta> buscarRecetas(String num_comida);

	Boolean AgregarRecetaComida(ArrayList<Receta> recetas, Comida c);

	Boolean ModificarRecetaComida(ArrayList<Receta> recetas, Comida c);

	Boolean BorrarRecetaComida(ArrayList<Receta> recetas, Comida c);

}
