package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class IngredienteDaoImp implements IngredienteDao {

	@Override
	public Boolean agregarIngrediente(Ingrediente ing) {
		ArrayList<String> parametros = new ArrayList<String>();
		parametros.add(String.valueOf(ing.getNum_ingrediente()));
		parametros.add(ing.getNombre_ingrediente());
		if (ing.getEs_envasado()) {
			parametros.add("1");
		} else {
			parametros.add("0");
		}
		if (ing.getEs_bebida()) {
			parametros.add("1");
		} else {
			parametros.add("0");
		}
		parametros.add(ing.getDescripcion_ingrediente());
		parametros.add(String.valueOf(ing.getCant_ingrediente()));
		parametros.add(String.valueOf(ing.getStock_ingrediente()));
		String consulta = "insert into restaurante.ingrediente(num_ingrediente,nombre_ingrediente,es_envasado,es_bebida,descripcion_ingrediente,cant_ingrediente,stock_ingrediente)"
				+ " values(?,?,?,?,?,?,?)";
		return BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Boolean modificarIngrediente(Ingrediente ing) {
		ArrayList<String> parametros = new ArrayList<String>();
		parametros.add(ing.getNombre_ingrediente());
		if (ing.getEs_envasado()) {
			parametros.add("1");
		} else {
			parametros.add("0");
		}
		if (ing.getEs_bebida()) {
			parametros.add("1");
		} else {
			parametros.add("0");
		}
		parametros.add(ing.getDescripcion_ingrediente());
		parametros.add(String.valueOf(ing.getCant_ingrediente()));
		parametros.add(String.valueOf(ing.getStock_ingrediente()));
		parametros.add(String.valueOf(ing.getNum_ingrediente()));
		String consulta = "update restaurante.ingrediente set nombre_ingrediente=?,es_envasado=?,es_bebida=?,descripcion_ingrediente=?,cant_ingrediente=?,stock_ingrediente=? where num_ingrediente=?";
		return BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Boolean borrarIngrediente(String num_ingrediente) {
		ArrayList<Receta> recetas = this.buscarRecetas(num_ingrediente);
		for (Receta receta : recetas) {
			ArrayList<String> parametrosReceta = new ArrayList<String>();
			parametrosReceta.add(String.valueOf(receta.getNum_comida()));
			parametrosReceta.add(String.valueOf(receta.getNum_ingrediente()));
			String consulta = "delete from restaurante.comida_ingrediente where num_comida=? and num_ingrediente=? ";
			BaseDeDatos.getInstance().manipularEntidades(consulta, parametrosReceta);
		}
		ArrayList<String> parametros = new ArrayList<String>();
		parametros.add(num_ingrediente);
		String consulta = "delete from restaurante.ingrediente where num_ingrediente=?";
		return BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Ingrediente buscarIngredientePorNumero(String num_ingrediente) {
		ArrayList<Ingrediente> ing = new ArrayList<Ingrediente>();
		Ingrediente i = null;
		ing = this.buscarIngredientes();
		for (Ingrediente ingrediente : ing) {
			if (ingrediente.getNum_ingrediente() == Integer.valueOf(num_ingrediente)) {
				i = ingrediente;
			}
		}
		return i;
	}

	@Override
	public ArrayList<Ingrediente> buscarIngredientes() {
		String consulta = "select * from restaurante.ingrediente";
		ArrayList<Ingrediente> ing = new ArrayList<Ingrediente>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Ingrediente i;
				Boolean es_envasado, es_bebida;
				if (rs.getString("es_envasado").equals("1")) {
					es_envasado = true;
				} else {
					es_envasado = false;
				}
				if (rs.getString("es_bebida").equals("1")) {
					es_bebida = true;
				} else {
					es_bebida = false;
				}
				i = new Ingrediente(Integer.valueOf(rs.getString("num_ingrediente")),
						rs.getString("nombre_ingrediente"), es_envasado, es_bebida,
						rs.getString("descripcion_ingrediente"), Integer.valueOf(rs.getString("cant_ingrediente")),
						Integer.valueOf(rs.getString("stock_ingrediente")));
				ing.add(i);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ing;
	}

	@Override
	public ArrayList<Ingrediente> IngredienteporNombre(String nombre_ingrediente) {
		String consulta = "select * from restaurante.ingrediente where nombre_ingrediente like '%" + nombre_ingrediente
				+ "%' order by nombre_ingrediente";
		ArrayList<Ingrediente> ing = new ArrayList<Ingrediente>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Ingrediente i;
				Boolean es_envasado, es_bebida;
				if (rs.getString("es_envasado").equals("1")) {
					es_envasado = true;
				} else {
					es_envasado = false;
				}
				if (rs.getString("es_bebida").equals("1")) {
					es_bebida = true;
				} else {
					es_bebida = false;
				}
				i = new Ingrediente(Integer.valueOf(rs.getString("num_ingrediente")),
						rs.getString("nombre_ingrediente"), es_envasado, es_bebida,
						rs.getString("descripcion_ingrediente"), Integer.valueOf(rs.getString("cant_ingrediente")),
						Integer.valueOf(rs.getString("stock_ingrediente")));
				ing.add(i);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ing;
	}

	@Override
	public ArrayList<Ingrediente> IngredienteEnvasado() {
		String consulta = "select * from restaurante.ingrediente where es_envasado = 1";
		ArrayList<Ingrediente> ing = new ArrayList<Ingrediente>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Ingrediente i;
				Boolean envasado, bebida;
				if (rs.getString("es_envasado").equals("1")) {
					envasado = true;
				} else {
					envasado = false;
				}
				if (rs.getString("es_bebida").equals("1")) {
					bebida = true;
				} else {
					bebida = false;
				}
				i = new Ingrediente(Integer.valueOf(rs.getString("num_ingrediente")),
						rs.getString("nombre_ingrediente"), envasado, bebida, rs.getString("descripcion_ingrediente"),
						Integer.valueOf(rs.getString("cant_ingrediente")),
						Integer.valueOf(rs.getString("stock_ingrediente")));
				ing.add(i);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ing;
	}

	@Override
	public ArrayList<Ingrediente> IngredienteBebida() {
		String consulta = "select * from restaurante.ingrediente where es_bebida = 1";
		ArrayList<Ingrediente> ing = new ArrayList<Ingrediente>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Ingrediente i;
				Boolean envasado, bebida;
				if (rs.getString("es_envasado").equals("1")) {
					envasado = true;
				} else {
					envasado = false;
				}
				if (rs.getString("es_bebida").equals("1")) {
					bebida = true;
				} else {
					bebida = false;
				}
				i = new Ingrediente(Integer.valueOf(rs.getString("num_ingrediente")),
						rs.getString("nombre_ingrediente"), envasado, bebida, rs.getString("descripcion_ingrediente"),
						Integer.valueOf(rs.getString("cant_ingrediente")),
						Integer.valueOf(rs.getString("stock_ingrediente")));
				ing.add(i);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ing;
	}

	@Override
	public ArrayList<Ingrediente> IngredienteSinStock() {
		String consulta = "select * from restaurante.ingrediente where cant_ingrediente < stock_ingrediente";
		ArrayList<Ingrediente> ing = new ArrayList<Ingrediente>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Ingrediente i;
				Boolean envasado, bebida;
				if (rs.getString("es_envasado").equals("1")) {
					envasado = true;
				} else {
					envasado = false;
				}
				if (rs.getString("es_bebida").equals("1")) {
					bebida = true;
				} else {
					bebida = false;
				}
				i = new Ingrediente(Integer.valueOf(rs.getString("num_ingrediente")),
						rs.getString("nombre_ingrediente"), envasado, bebida, rs.getString("descripcion_ingrediente"),
						Integer.valueOf(rs.getString("cant_ingrediente")),
						Integer.valueOf(rs.getString("stock_ingrediente")));
				ing.add(i);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ing;
	}

	@Override
	public ArrayList<Ingrediente> IngredienteNoEnvasado() {
		String consulta = "select * from restaurante.ingrediente where es_envasado = 0";
		ArrayList<Ingrediente> ing = new ArrayList<Ingrediente>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Ingrediente i;
				Boolean envasado, bebida;
				if (rs.getString("es_envasado").equals("1")) {
					envasado = true;
				} else {
					envasado = false;
				}
				if (rs.getString("es_bebida").equals("1")) {
					bebida = true;
				} else {
					bebida = false;
				}
				i = new Ingrediente(Integer.valueOf(rs.getString("num_ingrediente")),
						rs.getString("nombre_ingrediente"), envasado, bebida, rs.getString("descripcion_ingrediente"),
						Integer.valueOf(rs.getString("cant_ingrediente")),
						Integer.valueOf(rs.getString("stock_ingrediente")));
				ing.add(i);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ing;
	}

	@Override
	public ArrayList<Ingrediente> IngredienteNoBebida() {
		String consulta = "select * from restaurante.ingrediente where es_bebida = 0";
		ArrayList<Ingrediente> ing = new ArrayList<Ingrediente>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Ingrediente i;
				Boolean envasado, bebida;
				if (rs.getString("es_envasado").equals("1")) {
					envasado = true;
				} else {
					envasado = false;
				}
				if (rs.getString("es_bebida").equals("1")) {
					bebida = true;
				} else {
					bebida = false;
				}
				i = new Ingrediente(Integer.valueOf(rs.getString("num_ingrediente")),
						rs.getString("nombre_ingrediente"), envasado, bebida, rs.getString("descripcion_ingrediente"),
						Integer.valueOf(rs.getString("cant_ingrediente")),
						Integer.valueOf(rs.getString("stock_ingrediente")));
				ing.add(i);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ing;
	}
	
	@Override
	public ArrayList<Receta> buscarRecetas(String num_ingrediente) {
		String consulta = "select * from restaurante.comida_ingrediente where num_ingrediente= " + num_ingrediente;
		ArrayList<Receta> rec = new ArrayList<Receta>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Receta r;
				r = new Receta(Integer.valueOf(rs.getString("num_comida")),
						Integer.valueOf(rs.getString("num_ingrediente")),
						Integer.valueOf(rs.getString("cant_ingredientes_usado")));
				rec.add(r);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rec;
	}
}
