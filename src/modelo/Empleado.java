package modelo;

public class Empleado {

	private Integer legajo;
	private String nombre;
	private String apellido;
	private String dni;
	private String num_contacto;
	private String email;
	private String usuario;
	private String contrasenia;
	private String tipo_empleado;

	public Empleado() {
		super();
		this.setLegajo(0);
		this.setNombre("");
		this.setApellido("");
		this.setDni("");
		this.setNum_contacto("");
		this.setEmail("");
		this.setUsuario("");
		this.setContrasenia("");
		this.setTipo_empleado("");
	}

	public Empleado(Integer legajo, String nombre, String apellido, String dni, String num_contacto, String email,
			String usuario, String contrasenia, String tipo_empleado) {
		super();
		this.setLegajo(legajo);
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setDni(dni);
		this.setNum_contacto(num_contacto);
		this.setEmail(email);
		this.setUsuario(usuario);
		this.setContrasenia(contrasenia);
		this.setTipo_empleado(tipo_empleado);
	}

	@Override
	public String toString() {
		return "Empleado [legajo=" + legajo + ", nombre=" + nombre + ", apellido=" + apellido + ", dni=" + dni
				+ ", num_contacto=" + num_contacto + ", email=" + email + ", usuario=" + usuario + ", contrasenia="
				+ contrasenia + ", tipo_empleado=" + tipo_empleado + "]";
	}

	public Integer getLegajo() {
		return legajo;
	}

	public void setLegajo(Integer legajo) {
		this.legajo = legajo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNum_contacto() {
		return num_contacto;
	}

	public void setNum_contacto(String num_contacto) {
		this.num_contacto = num_contacto;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getTipo_empleado() {
		return tipo_empleado;
	}

	public void setTipo_empleado(String tipo_empleado) {
		this.tipo_empleado = tipo_empleado;
	}
}
