package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ComidaDaoImp implements ComidaDao {

	@Override
	public Boolean agregarComida(Comida c) {
		ArrayList<String> parametros = new ArrayList<String>();
		parametros.add(String.valueOf(c.getNum_comida()));
		parametros.add(c.getNombre_comida());
		parametros.add(c.getDescripcion_comida());
		if (c.getEs_platillo()) {
			parametros.add("1");
		} else {
			parametros.add("0");
		}
		parametros.add(String.valueOf(c.getPrecio_unitario()));
		String consulta = "insert into restaurante.comida(num_comida,nombre_comida,descripcion_comida,es_platillo,precio_unitario)"
				+ " values(?,?,?,?,?)";
		return BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Boolean modificarComida(Comida c) {
		ArrayList<String> parametros = new ArrayList<String>();
		parametros.add(c.getNombre_comida());
		parametros.add(c.getDescripcion_comida());
		if (c.getEs_platillo()) {
			parametros.add("1");
		} else {
			parametros.add("0");
		}
		parametros.add(String.valueOf(c.getPrecio_unitario()));
		parametros.add(String.valueOf(c.getNum_comida()));
		String consulta = "update restaurante.comida set nombre_comida=?,descripcion_comida=?,es_platillo=?,precio_unitario=? where num_comida=?";
		return BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Boolean borrarComida(String num_comida) {
		ArrayList<Receta> recetas = this.buscarRecetas(num_comida);
		for (Receta receta : recetas) {
			ArrayList<String> parametrosReceta = new ArrayList<String>();
			parametrosReceta.add(String.valueOf(receta.getNum_comida()));
			parametrosReceta.add(String.valueOf(receta.getNum_ingrediente()));
			String consulta = "delete from restaurante.comida_ingrediente where num_comida=? and num_ingrediente=? ";
			BaseDeDatos.getInstance().manipularEntidades(consulta, parametrosReceta);
		}
		ArrayList<String> parametros = new ArrayList<String>();
		parametros.add(num_comida);
		String consulta = "delete from restaurante.comida where num_comida=?";
		return BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Comida buscarComidaPorNumero(String numero_comida) {
		ArrayList<Comida> com = new ArrayList<Comida>();
		Comida c = null;
		com = this.buscarComidas();
		for (Comida comida : com) {
			if (comida.getNum_comida() == Integer.valueOf(numero_comida)) {
				c = comida;
			}
		}
		return c;
	}

	@Override
	public ArrayList<Comida> buscarComidaPorNombre(String nombre_comida) {
		String consulta = "select * from restaurante.comida where nombre_comida like '%" + nombre_comida
				+ "%' order by nombre_comida";
		ArrayList<Comida> com = new ArrayList<Comida>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Comida c;
				Boolean es_platillo;
				if (rs.getString("es_platillo").equals("1")) {
					es_platillo = true;
				} else {
					es_platillo = false;
				}
				c = new Comida(Integer.parseInt(rs.getString("num_comida")), rs.getString("nombre_comida"),
						rs.getString("descripcion_comida"), es_platillo,
						Double.valueOf(rs.getString("precio_unitario")));
				com.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return com;
	}

	@Override
	public ArrayList<Comida> buscarComidas() {
		String consulta = "select * from restaurante.comida";
		ArrayList<Comida> com = new ArrayList<Comida>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Comida c;
				Boolean es_platillo;
				if (rs.getString("es_platillo").equals("1")) {
					es_platillo = true;
				} else {
					es_platillo = false;
				}
				c = new Comida(Integer.parseInt(rs.getString("num_comida")), rs.getString("nombre_comida"),
						rs.getString("descripcion_comida"), es_platillo,
						Double.valueOf(rs.getString("precio_unitario")));
				com.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return com;
	}

	@Override
	public ArrayList<Comida> buscarPecio_Comida() {
		String consulta = "select * from restaurante.comida order by precio_unitario";
		ArrayList<Comida> com = new ArrayList<Comida>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Comida c;
				Boolean es_platillo;
				if (rs.getString("es_platillo").equals("1")) {
					es_platillo = true;
				} else {
					es_platillo = false;
				}
				c = new Comida(Integer.parseInt(rs.getString("num_comida")), rs.getString("nombre_comida"),
						rs.getString("descripcion_comida"), es_platillo,
						Double.valueOf(rs.getString("precio_unitario")));
				com.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return com;
	}

	@Override
	public ArrayList<Comida> buscarEs_Platillo() {
		String consulta = "select * from restaurante.comida where es_platillo";
		ArrayList<Comida> com = new ArrayList<Comida>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Comida c;
				Boolean es_platillo;
				if (rs.getString("es_platillo").equals("1")) {
					es_platillo = true;
				} else {
					es_platillo = false;
				}
				c = new Comida(Integer.parseInt(rs.getString("num_comida")), rs.getString("nombre_comida"),
						rs.getString("descripcion_comida"), es_platillo,
						Double.valueOf(rs.getString("precio_unitario")));
				com.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return com;
	}

	@Override
	public ArrayList<Comida> buscarEs_Bebida() {
		String consulta = "select * from restaurante.comida where es_platillo = '0'";
		ArrayList<Comida> com = new ArrayList<Comida>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Comida c;
				Boolean es_platillo;
				if (rs.getString("es_platillo").equals("1")) {
					es_platillo = true;
				} else {
					es_platillo = false;
				}
				c = new Comida(Integer.parseInt(rs.getString("num_comida")), rs.getString("nombre_comida"),
						rs.getString("descripcion_comida"), es_platillo,
						Double.valueOf(rs.getString("precio_unitario")));
				com.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return com;
	}

	@Override
	public ArrayList<Receta> buscarRecetas(String num_comida) {
		String consulta = "select * from restaurante.comida_ingrediente where num_comida= " + num_comida;
		ArrayList<Receta> rec = new ArrayList<Receta>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Receta r;
				r = new Receta(Integer.valueOf(rs.getString("num_comida")),
						Integer.valueOf(rs.getString("num_ingrediente")),
						Integer.valueOf(rs.getString("cant_ingredientes_usado")));
				rec.add(r);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rec;
	}

	@Override
	public Boolean AgregarRecetaComida(ArrayList<Receta> recetas, Comida c) {
		Boolean logico = true;
		if (this.agregarComida(c)) {
			for (Receta receta : recetas) {
				ArrayList<String> parametros = new ArrayList<String>();
				parametros.add(String.valueOf(receta.getNum_comida()));
				parametros.add(String.valueOf(receta.getNum_ingrediente()));
				parametros.add(String.valueOf(receta.getCant_ingrediente_usado()));
				String consulta = "insert into restaurante.comida_ingrediente(num_comida,num_ingrediente,cant_ingredientes_usado)"
						+ " values(?,?,?)";
				if (!BaseDeDatos.getInstance().manipularEntidades(consulta, parametros)) {
					logico = false;
				}
			}
		}
		return logico;
	}

	@Override
	public Boolean ModificarRecetaComida(ArrayList<Receta> recetas, Comida c) {
		Boolean logicomodi = true;
		if (this.modificarComida(c)) {
			this.BorrarRecetaComida(recetas, c);
			for (Receta receta : recetas) {
				ArrayList<String> parametros = new ArrayList<String>();
				parametros.add(String.valueOf(receta.getNum_comida()));
				parametros.add(String.valueOf(receta.getNum_ingrediente()));
				parametros.add(String.valueOf(receta.getCant_ingrediente_usado()));
				String consulta = "insert into restaurante.comida_ingrediente(num_comida,num_ingrediente,cant_ingredientes_usado)"
						+ " values(?,?,?)";
				if (!BaseDeDatos.getInstance().manipularEntidades(consulta, parametros)) {
					logicomodi = false;
				}
			}
		}
		return logicomodi;
	}

	@Override
	public Boolean BorrarRecetaComida(ArrayList<Receta> recetas, Comida c) {
		Boolean logicoborr = true;
		for (Receta receta : recetas) {
			ArrayList<String> parametrosReceta = new ArrayList<String>();
			parametrosReceta.add(String.valueOf(receta.getNum_comida()));
			parametrosReceta.add(String.valueOf(receta.getNum_ingrediente()));
			String consulta = "delete from restaurante.comida_ingrediente where num_comida=? and num_ingrediente=? ";
			if (BaseDeDatos.getInstance().manipularEntidades(consulta, parametrosReceta)) {
				logicoborr = false;
			}
		}
		return logicoborr;
	}

}
