package modelo;

import java.util.ArrayList;

public interface VentaDao {

	// ABM
	Boolean agregarventa(Venta v);

	Boolean modificarVenta(Venta v);

	Boolean borrarVenta(String num_venta);

	//
	Integer nuevoNumVenta();
	
	Venta buscarVentaPorNumero(String numero_venta);

	ArrayList<Venta> buscarVentas();
	
	ArrayList<Venta> VentadelaFecha(String fecha_venta);
	
	ArrayList<Venta> VentadeMesero(String legajo);
	
	void BorrarVentasVacias();

}
