package modelo;

public class Ingrediente {

	private Integer num_ingrediente;
	private String nombre_ingrediente;
	private Boolean es_envasado;
	private Boolean es_bebida;
	private String descripcion_ingrediente;
	private Integer cant_ingrediente;
	private Integer stock_ingrediente;

	public Ingrediente(Integer num_ingrediente, String nombre_ingrediente, Boolean es_envasado, Boolean es_bebida,
			String descripcion_ingrediente, Integer cant_ingrediente, Integer stock_ingrediente) {
		super();
		this.setNum_ingrediente(num_ingrediente);
		this.setNombre_ingrediente(nombre_ingrediente);
		this.setEs_envasado(es_envasado);
		this.setEs_bebida(es_bebida);
		this.setDescripcion_ingrediente(descripcion_ingrediente);
		this.setCant_ingrediente(cant_ingrediente);
		this.setStock_ingrediente(stock_ingrediente);
	}

	public Integer getNum_ingrediente() {
		return num_ingrediente;
	}

	public void setNum_ingrediente(Integer num_ingrediente) {
		this.num_ingrediente = num_ingrediente;
	}

	public String getNombre_ingrediente() {
		return nombre_ingrediente;
	}

	public void setNombre_ingrediente(String nombre_ingrediente) {
		this.nombre_ingrediente = nombre_ingrediente;
	}

	public Boolean getEs_envasado() {
		return es_envasado;
	}

	public void setEs_envasado(Boolean es_envasado) {
		this.es_envasado = es_envasado;
	}

	public Boolean getEs_bebida() {
		return es_bebida;
	}

	public void setEs_bebida(Boolean es_bebida) {
		this.es_bebida = es_bebida;
	}

	public String getDescripcion_ingrediente() {
		return descripcion_ingrediente;
	}

	public void setDescripcion_ingrediente(String descripcion_ingrediente) {
		this.descripcion_ingrediente = descripcion_ingrediente;
	}

	public Integer getCant_ingrediente() {
		return cant_ingrediente;
	}

	public void setCant_ingrediente(Integer cant_ingrediente) {
		this.cant_ingrediente = cant_ingrediente;
	}

	public Integer getStock_ingrediente() {
		return stock_ingrediente;
	}

	public void setStock_ingrediente(Integer stock_ingrediente) {
		this.stock_ingrediente = stock_ingrediente;
	}

}
