package modelo;

import java.util.ArrayList;

public interface IngredienteDao {

	//ABM
	Boolean agregarIngrediente(Ingrediente ing);
	Boolean modificarIngrediente(Ingrediente ing);
	Boolean borrarIngrediente(String num_ingrediente);
	
	//
	Ingrediente buscarIngredientePorNumero(String num_ingrediente);
	ArrayList<Ingrediente> buscarIngredientes();
	ArrayList<Ingrediente> IngredienteporNombre(String nombre_ingrediente);
	ArrayList<Ingrediente> IngredienteEnvasado();
	ArrayList<Ingrediente> IngredienteNoEnvasado();
	ArrayList<Ingrediente> IngredienteBebida();
	ArrayList<Ingrediente> IngredienteNoBebida();
	ArrayList<Ingrediente> IngredienteSinStock();
	ArrayList<Receta> buscarRecetas(String num_ingrediente);
	
	
}
