package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PedidoDaoImp implements PedidoDao {

	@Override
	public Boolean agregarPedido(Pedido pedido) {
		ArrayList<String> parametros = new ArrayList<String>();
		parametros.add(String.valueOf(pedido.getNum_pedido()));
		parametros.add(String.valueOf(pedido.getNum_venta()));
		parametros.add(String.valueOf(pedido.getNum_mesa()));
		parametros.add(pedido.getDescripcion_pedido());
		if (pedido.getTerminado_pedido()) {
			parametros.add("1");
		} else {
			parametros.add("0");
		}
		if (pedido.getEntregado_mesa()) {
			parametros.add("1");
		} else {
			parametros.add("0");
		}
		parametros.add(String.valueOf(pedido.getPrecio_pedido()));
		String consulta = "insert into restaurante.pedido(num_pedido,num_venta,num_mesa,descripcion_pedido,terminado_pedido,entregado_mesa,precio_pedido)"
				+ " values(?,?,?,?,?,?,?)";
		return BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Boolean modificarPedido(Pedido pedido) {
		ArrayList<String> parametros = new ArrayList<String>();
		parametros.add(String.valueOf(pedido.getNum_venta()));
		parametros.add(String.valueOf(pedido.getNum_mesa()));
		parametros.add(pedido.getDescripcion_pedido());
		if (pedido.getTerminado_pedido()) {
			parametros.add("1");
		} else {
			parametros.add("0");
		}
		if (pedido.getEntregado_mesa()) {
			parametros.add("1");
		} else {
			parametros.add("0");
		}
		parametros.add(String.valueOf(pedido.getPrecio_pedido()));
		parametros.add(String.valueOf(pedido.getNum_pedido()));
		String consulta = "update restaurante.pedido set num_venta=?,num_mesa=?,descripcion_pedido=?,terminado_pedido=?,entregado_mesa=?,precio_pedido=? where num_pedido=?";
		return BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Boolean borrarPedido(String num_pedido) {
		ArrayList<Pedido_Comidas> pedidos = this.buscarPedidoComidas(num_pedido);
		for (Pedido_Comidas pedido_comida : pedidos) {
			ArrayList<String> parametrosPedidoComidas = new ArrayList<String>();
			parametrosPedidoComidas.add(String.valueOf(pedido_comida.getNum_pedido()));
			parametrosPedidoComidas.add(String.valueOf(pedido_comida.getNum_comida()));
			String consulta = "delete from restaurante.pedido_comida where num_pedido=? and num_comida=? ";
			BaseDeDatos.getInstance().manipularEntidades(consulta, parametrosPedidoComidas);
		}
		ArrayList<String> parametros = new ArrayList<String>();
		parametros.add(num_pedido);
		String consulta = "delete from restaurante.pedido where num_pedido=?";
		return BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public Pedido buscarPedido(String num_pedido) {
		ArrayList<Pedido> ped = new ArrayList<Pedido>();
		Pedido p = null;
		ped = this.buscarPedidos();
		for (Pedido pedido : ped) {
			if (pedido.getNum_pedido() == Integer.valueOf(num_pedido)) {
				p = pedido;
			}
		}
		return p;
	}

	@Override
	public ArrayList<Pedido> buscarPedidosMesa(String num_mesa, String num_venta) {
		String consulta = "select * from restaurante.pedido where num_venta = " + num_venta + " and num_mesa = "
				+ num_mesa;
		ArrayList<Pedido> ped = new ArrayList<Pedido>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Pedido p;
				Boolean terminado_pedido;
				Boolean entregado_pedido;
				if (rs.getString("terminado_pedido").equals("1")) {
					terminado_pedido = true;
				} else {
					terminado_pedido = false;
				}
				if (rs.getString("entregado_mesa").equals("1")) {
					entregado_pedido = true;
				} else {
					entregado_pedido = false;
				}
				p = new Pedido(Integer.valueOf(rs.getString("num_pedido")), Integer.valueOf(rs.getString("num_venta")),
						Integer.valueOf(rs.getString("num_mesa")), rs.getString("descripcion_pedido"), terminado_pedido,
						entregado_pedido, Double.parseDouble((rs.getString("precio_pedido"))));
				ped.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ped;
	}

	@Override
	public ArrayList<Pedido> buscarPedidosVenta(String num_venta) {
		String consulta = "select * from restaurante.pedido where num_venta = " + num_venta;
		ArrayList<Pedido> ped = new ArrayList<Pedido>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Pedido p;
				Boolean terminado_pedido;
				Boolean entregado_pedido;
				if (rs.getString("terminado_pedido").equals("1")) {
					terminado_pedido = true;
				} else {
					terminado_pedido = false;
				}
				if (rs.getString("entregado_mesa").equals("1")) {
					entregado_pedido = true;
				} else {
					entregado_pedido = false;
				}
				p = new Pedido(Integer.valueOf(rs.getString("num_pedido")), Integer.valueOf(rs.getString("num_venta")),
						Integer.valueOf(rs.getString("num_mesa")), rs.getString("descripcion_pedido"), terminado_pedido,
						entregado_pedido, Double.parseDouble((rs.getString("precio_pedido"))));
				ped.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ped;
	}

	@Override
	public ArrayList<Pedido> buscarPedidosNoTerminadoNoEntregado() {
		String consulta = "select * from restaurante.pedido where terminado_pedido = 0 and entregado_mesa = 0";
		ArrayList<Pedido> ped = new ArrayList<Pedido>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Pedido p;
				Boolean terminado_pedido;
				Boolean entregado_pedido;
				if (rs.getString("terminado_pedido").equals("1")) {
					terminado_pedido = true;
				} else {
					terminado_pedido = false;
				}
				if (rs.getString("entregado_mesa").equals("1")) {
					entregado_pedido = true;
				} else {
					entregado_pedido = false;
				}
				p = new Pedido(Integer.valueOf(rs.getString("num_pedido")), Integer.valueOf(rs.getString("num_venta")),
						Integer.valueOf(rs.getString("num_mesa")), rs.getString("descripcion_pedido"), terminado_pedido,
						entregado_pedido, Double.parseDouble((rs.getString("precio_pedido"))));
				ped.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ped;
	}

	@Override
	public ArrayList<Pedido> buscarPedidosTerminadoNoEntregado() {
		String consulta = "select * from restaurante.pedido where terminado_pedido = 1 and entregado_mesa = 0";
		ArrayList<Pedido> ped = new ArrayList<Pedido>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Pedido p;
				Boolean terminado_pedido;
				Boolean entregado_pedido;
				if (rs.getString("terminado_pedido").equals("1")) {
					terminado_pedido = true;
				} else {
					terminado_pedido = false;
				}
				if (rs.getString("entregado_mesa").equals("1")) {
					entregado_pedido = true;
				} else {
					entregado_pedido = false;
				}
				p = new Pedido(Integer.valueOf(rs.getString("num_pedido")), Integer.valueOf(rs.getString("num_venta")),
						Integer.valueOf(rs.getString("num_mesa")), rs.getString("descripcion_pedido"), terminado_pedido,
						entregado_pedido, Double.parseDouble((rs.getString("precio_pedido"))));
				ped.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ped;
	}

	@Override
	public ArrayList<Pedido> buscarPedidos() {
		String consulta = "select * from restaurante.pedido";
		ArrayList<Pedido> ped = new ArrayList<Pedido>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Pedido p;
				Boolean terminado_pedido;
				Boolean entregado_pedido;
				if (rs.getString("terminado_pedido").equals("1")) {
					terminado_pedido = true;
				} else {
					terminado_pedido = false;
				}
				if (rs.getString("entregado_mesa").equals("1")) {
					entregado_pedido = true;
				} else {
					entregado_pedido = false;
				}
				p = new Pedido(Integer.valueOf(rs.getString("num_pedido")), Integer.valueOf(rs.getString("num_venta")),
						Integer.valueOf(rs.getString("num_mesa")), rs.getString("descripcion_pedido"), terminado_pedido,
						entregado_pedido, Double.parseDouble((rs.getString("precio_pedido"))));
				ped.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ped;
	}

	@Override
	public Integer numPedidoNuevo() {
		ArrayList <Pedido> pedidos = this.buscarPedidos();
		Integer nuevoNumero = pedidos.size() + 1;
		return nuevoNumero;
	}

	@Override
	public Boolean agregarPedidoComidas(Pedido pedido, ArrayList<Pedido_Comidas> pedido_comidas) {
		if (this.agregarPedido(pedido)) {
			for (Pedido_Comidas pedido_Comidas2 : pedido_comidas) {
				ArrayList<String> parametros = new ArrayList<String>();
				parametros.add(String.valueOf(pedido_Comidas2.getNum_pedido()));
				parametros.add(String.valueOf(pedido_Comidas2.getNum_comida()));
				parametros.add(String.valueOf(pedido_Comidas2.getCant_comida()));
				String consulta = "insert into restaurante.pedido_comida(num_pedido,num_comida,cant_comida)" + " values(?,?,?)";
				BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
			}
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Boolean modificarPedidoComidas(Pedido pedido, ArrayList<Pedido_Comidas> pedido_comidas) {
		if (this.modificarPedido(pedido)) {
			ArrayList<Pedido_Comidas> pedidos = this.buscarPedidoComidas(String.valueOf(pedido.getNum_pedido()));
			for (Pedido_Comidas pedido_comida : pedidos) {
				ArrayList<String> parametrosPedidoComidas = new ArrayList<String>();
				parametrosPedidoComidas.add(String.valueOf(pedido_comida.getNum_pedido()));
				parametrosPedidoComidas.add(String.valueOf(pedido_comida.getNum_comida()));
				String consulta = "delete from restaurante.pedido_comida where num_pedido=? and num_comida=? ";
				BaseDeDatos.getInstance().manipularEntidades(consulta, parametrosPedidoComidas);
			}
			for (Pedido_Comidas pedido_Comidas2 : pedido_comidas) {
				ArrayList<String> parametros = new ArrayList<String>();
				parametros.add(String.valueOf(pedido_Comidas2.getNum_pedido()));
				parametros.add(String.valueOf(pedido_Comidas2.getNum_comida()));
				parametros.add(String.valueOf(pedido_Comidas2.getCant_comida()));
				String consulta = "insert into restaurante.pedido_comida(num_pedido,num_comida,cant_comida)" + " values(?,?,?)";
				BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
			}
			return true;
		} else {
			return false;
		}
	}

	@Override
	public ArrayList<Pedido_Comidas> buscarPedidoComidas(String num_pedido) {
		String consulta = "select * from restaurante.pedido_comida where num_pedido= " + num_pedido;
		ArrayList<Pedido_Comidas> ped_com = new ArrayList<Pedido_Comidas>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Pedido_Comidas pd;
				pd = new Pedido_Comidas(Integer.valueOf(rs.getString("num_pedido")),
						Integer.valueOf(rs.getString("num_comida")),
						Integer.valueOf(rs.getString("cant_comida")));
				ped_com.add(pd);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ped_com;
	}

}
