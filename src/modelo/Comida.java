package modelo;

public class Comida {
	private Integer num_comida;
	private String nombre_comida;
	private String descripcion_comida;
	private Boolean es_platillo;
	private Double precio_unitario;

	public Comida(Integer num_comida, String nombre_comida, String descripcion_comida, Boolean es_platillo,
			Double precio_unitario) {
		super();
		this.setNum_comida(num_comida);
		this.setNombre_comida(nombre_comida);
		this.setDescripcion_comida(descripcion_comida);
		this.setEs_platillo(es_platillo);
		this.setPrecio_unitario(precio_unitario);
	}

	public Integer getNum_comida() {
		return num_comida;
	}

	public void setNum_comida(Integer num_comida) {
		this.num_comida = num_comida;
	}

	public String getNombre_comida() {
		return nombre_comida;
	}

	public void setNombre_comida(String nombre_comida) {
		this.nombre_comida = nombre_comida;
	}

	public String getDescripcion_comida() {
		return descripcion_comida;
	}

	public void setDescripcion_comida(String descripcion_comida) {
		this.descripcion_comida = descripcion_comida;
	}

	public Boolean getEs_platillo() {
		return es_platillo;
	}

	public void setEs_platillo(Boolean es_platillo) {
		this.es_platillo = es_platillo;
	}

	public Double getPrecio_unitario() {
		return precio_unitario;
	}

	public void setPrecio_unitario(Double precio_unitario) {
		this.precio_unitario = precio_unitario;
	}

}
