package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class BaseDeDatos {

	private Connection conexion = null;
	private static BaseDeDatos bd = null;

	private BaseDeDatos() {

		establecerConexion();
	}

	private void establecerConexion() {

		if (conexion != null)
			return;

		try {

			DriverManager.registerDriver(new com.mysql.jdbc.Driver());

			conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/restaurante", "root", "root");
			System.out.println("Conexi�n correcta con el Driver MYSQL");

		} catch (Exception e) {

			System.out.println("Problemas de conexion con el Driver MYSQL");

		}
	}

	public static BaseDeDatos getInstance() {
		if (bd == null) {
			bd = new BaseDeDatos();

		}
		return bd;
	}

	public ResultSet listarEntidades(String consulta) {

		ResultSet rs = null;
		try {
			Statement s = conexion.createStatement();
			rs = s.executeQuery(consulta);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}

	public ResultSet listarEntidadesParametrizada(String consulta, ArrayList<String> parametros) {
		ResultSet rs = null;
		try {
			PreparedStatement s = conexion.prepareStatement(consulta);
			int i = 0;
			while (i < parametros.size()) {
				s.setString(i + 1, parametros.get(i));

				i++;
			}
			rs = s.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}

	public Boolean manipularEntidades(String consulta, ArrayList<String> parametros) {
		boolean retorno = false;
		try {
			PreparedStatement s = conexion.prepareStatement(consulta);
			int i = 0;
			while (i < parametros.size()) {
				s.setString(i + 1, parametros.get(i));

				i++;
			}
			s.executeUpdate();
			retorno = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}
}
