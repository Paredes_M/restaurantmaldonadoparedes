package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MesaDaoImp implements MesaDao {

	@Override
	public Boolean modificarMesa(Mesa mesa) {
		ArrayList<String> parametros = new ArrayList<String>();
		String estado;
		if (mesa.getEstado()) {
			estado = "1";
		} else {
			estado = "0";
		}
		parametros.add(estado);
		parametros.add(String.valueOf(mesa.getNumero()));
		String consulta = "update restaurante.mesa set estado_mesa=? where num_mesa=?";
		return BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
	}

	@Override
	public ArrayList<Mesa> buscarMesaPorEstado(String estadoMesa) {

		return null;
	}

	@Override
	public Mesa buscarMesaPorNumero(String numeroMesa) {

		return null;
	}

	@Override
	public ArrayList<Mesa> buscarMesas() {
		String consulta = "select * from restaurante.mesa";
		ArrayList<Mesa> mesas = new ArrayList<Mesa>();
		ResultSet rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		try {
			while (rs.next()) {
				Mesa mesa;
				Boolean estadoMesa;
				if (rs.getString("estado_mesa").equals("0")) {
					estadoMesa = false;
				} else {
					estadoMesa = true;
				}
				mesa = new Mesa(Integer.parseInt(rs.getString("num_mesa")), estadoMesa);
				mesas.add(mesa);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return mesas;
	}

}
