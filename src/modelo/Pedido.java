package modelo;

public class Pedido {
    private Integer num_pedido;
	private Integer num_venta;
	private Integer num_mesa;
	private String descripcion_pedido;
	private Boolean terminado_pedido;
	private Boolean entregado_mesa;
	private Double precio_pedido;
	
	public Pedido(Integer num_pedido, Integer num_venta, Integer num_mesa, String descripcion_pedido,
			Boolean terminado_pedido, Boolean entregado_mesa, Double precio_pedido) {
		super();
		this.setNum_pedido(num_pedido);
		this.setNum_venta(num_venta);
		this.setNum_mesa(num_mesa);
		this.setDescripcion_pedido(descripcion_pedido);
		this.setTerminado_pedido(terminado_pedido);
		this.setEntregado_mesa(entregado_mesa);
		this.setPrecio_pedido(precio_pedido);
	}
	
	public Integer getNum_pedido() {
		return num_pedido;
	}
	public void setNum_pedido(Integer num_pedido) {
		this.num_pedido = num_pedido;
	}
	public Integer getNum_venta() {
		return num_venta;
	}
	public void setNum_venta(Integer num_venta) {
		this.num_venta = num_venta;
	}
	public Integer getNum_mesa() {
		return num_mesa;
	}
	public void setNum_mesa(Integer num_mesa) {
		this.num_mesa = num_mesa;
	}
	public String getDescripcion_pedido() {
		return descripcion_pedido;
	}
	public void setDescripcion_pedido(String descripcion_pedido) {
		this.descripcion_pedido = descripcion_pedido;
	}
	public Boolean getTerminado_pedido() {
		return terminado_pedido;
	}
	public void setTerminado_pedido(Boolean terminado_pedido) {
		this.terminado_pedido = terminado_pedido;
	}
	public Boolean getEntregado_mesa() {
		return entregado_mesa;
	}
	public void setEntregado_mesa(Boolean entregado_mesa) {
		this.entregado_mesa = entregado_mesa;
	}
	public Double getPrecio_pedido() {
		return precio_pedido;
	}
	public void setPrecio_pedido(Double precio_pedido) {
		this.precio_pedido = precio_pedido;
	}

	@Override
	public String toString() {
		return "Pedido [getNum_pedido()=" + getNum_pedido() + ", getNum_venta()=" + getNum_venta() + ", getNum_mesa()="
				+ getNum_mesa() + ", getDescripcion_pedido()=" + getDescripcion_pedido() + ", getTerminado_pedido()="
				+ getTerminado_pedido() + ", getEntregado_mesa()=" + getEntregado_mesa() + ", getPrecio_pedido()="
				+ getPrecio_pedido() + "]";
	}

}