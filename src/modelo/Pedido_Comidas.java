package modelo;

public class Pedido_Comidas {

	private Integer num_pedido;
	private Integer num_comida;
	private Integer cant_comida;

	public Pedido_Comidas(Integer num_pedido, Integer num_comida, Integer cant_comida) {
		super();
		this.setNum_pedido(num_pedido);
		this.setNum_comida(num_comida);
		this.setCant_comida(cant_comida);
	}

	public Integer getNum_pedido() {
		return num_pedido;
	}

	public void setNum_pedido(Integer num_pedido) {
		this.num_pedido = num_pedido;
	}

	public Integer getNum_comida() {
		return num_comida;
	}

	public void setNum_comida(Integer num_comida) {
		this.num_comida = num_comida;
	}

	public Integer getCant_comida() {
		return cant_comida;
	}

	public void setCant_comida(Integer cant_comida) {
		this.cant_comida = cant_comida;
	}

	@Override
	public String toString() {
		return "Pedido_Comidas [getNum_pedido()=" + getNum_pedido() + ", getNum_comida()=" + getNum_comida()
				+ ", getCant_comida()=" + getCant_comida() + "]";
	}

}
