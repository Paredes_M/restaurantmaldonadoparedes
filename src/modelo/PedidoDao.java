package modelo;

import java.util.ArrayList;

public interface PedidoDao {
	
    //ABM
	Boolean agregarPedido(Pedido pedido);
	Boolean modificarPedido(Pedido pedido);
	Boolean borrarPedido(String num_pedido);
	
    //
	Boolean agregarPedidoComidas(Pedido pedido, ArrayList<Pedido_Comidas> pedido_comidas);
	Boolean modificarPedidoComidas(Pedido pedido, ArrayList<Pedido_Comidas> pedido_comidas);
	ArrayList<Pedido_Comidas> buscarPedidoComidas(String num_pedido);
	Pedido buscarPedido(String num_pedido);
	Integer numPedidoNuevo();
	ArrayList<Pedido> buscarPedidosMesa(String num_mesa, String num_venta);
	ArrayList<Pedido> buscarPedidosVenta(String num_venta);
	ArrayList<Pedido> buscarPedidosNoTerminadoNoEntregado();
	ArrayList<Pedido> buscarPedidosTerminadoNoEntregado();
	ArrayList<Pedido> buscarPedidos();
}
