package modelo;

public class Receta {
	
	private Integer num_comida;
	private Integer num_ingrediente;
	private Integer cant_ingrediente_usado;
	
	public Receta() {
		this.setNum_comida(0);
		this.setNum_ingrediente(0);
		this.setCant_ingrediente_usado(0);
	}
	
	public Receta(Integer num_comida, Integer num_ingrediente, Integer cant_ingrediente_usado) {
		super();
		this.setNum_comida(num_comida);
		this.setNum_ingrediente(num_ingrediente);
		this.setCant_ingrediente_usado(cant_ingrediente_usado);
	}
	
	public Integer getNum_comida() {
		return num_comida;
	}
	public void setNum_comida(Integer num_comida) {
		this.num_comida = num_comida;
	}
	public Integer getNum_ingrediente() {
		return num_ingrediente;
	}
	public void setNum_ingrediente(Integer num_ingrediente) {
		this.num_ingrediente = num_ingrediente;
	}
	public Integer getCant_ingrediente_usado() {
		return cant_ingrediente_usado;
	}
	public void setCant_ingrediente_usado(Integer cant_ingrediente_usado) {
		this.cant_ingrediente_usado = cant_ingrediente_usado;
	}


}
