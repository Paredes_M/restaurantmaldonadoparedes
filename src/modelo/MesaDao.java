package modelo;

import java.util.ArrayList;

public interface MesaDao {

	Boolean modificarMesa(Mesa mesa);
	
	ArrayList<Mesa> buscarMesaPorEstado(String estadoMesa);
	Mesa buscarMesaPorNumero(String numeroMesa);
	ArrayList<Mesa> buscarMesas();
	
	
}
