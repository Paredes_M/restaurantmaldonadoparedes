package modelo;

import java.util.ArrayList;

public interface EmpleadoDao {

	// Empleado ABM (MESERO/COCINERO)

	Boolean agregarEmpleado(Empleado e);

	Boolean modificarEmpleado(Empleado e);

	Boolean borrarEmpleado(String legajo);

	// BUSQUEDA

	Empleado buscarEmpleado(String legajo);
	
	Empleado buscarEmpleadoPorUsuarioContraseņa(String usuario, String contraseņa);

	ArrayList<Empleado> buscarEmpleadosPorNombre(String nombre);

	ArrayList<Empleado> buscarEmpleadosPorApellido(String apellido);

	ArrayList<Empleado> buscarEmpleadosPorTipoEmpleado(String tipo_empleado);

	ArrayList<Empleado> buscarEmpleados();

}
