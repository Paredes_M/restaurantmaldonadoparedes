package modelo;

import javax.swing.JButton;

public class Mesa extends JButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer numero;
	private Boolean estado;

	
	public Mesa(Integer numero, Boolean estado) {
		super();
		this.setText(String.valueOf(numero));
		this.setNumero(numero);
		this.setEstado(estado);

	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}


}
