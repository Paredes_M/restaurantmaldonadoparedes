-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: restaurante
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comida_ingrediente`
--

DROP TABLE IF EXISTS `comida_ingrediente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comida_ingrediente` (
  `num_comida` int(11) NOT NULL,
  `num_ingrediente` int(11) NOT NULL,
  `cant_ingredientes_usado` int(11) DEFAULT NULL,
  PRIMARY KEY (`num_comida`,`num_ingrediente`),
  KEY `fkey_idx_num_ingrediente` (`num_ingrediente`),
  KEY `fkey_idx_num_comida` (`num_comida`),
  CONSTRAINT `fkey_num_comida` FOREIGN KEY (`num_comida`) REFERENCES `comida` (`num_comida`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkey_num_ingrediente` FOREIGN KEY (`num_ingrediente`) REFERENCES `ingrediente` (`num_ingrediente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comida_ingrediente`
--

LOCK TABLES `comida_ingrediente` WRITE;
/*!40000 ALTER TABLE `comida_ingrediente` DISABLE KEYS */;
INSERT INTO `comida_ingrediente` VALUES (1,1,1),(1,2,1),(1,3,1),(1,4,2),(1,5,1),(1,6,1),(1,7,1),(1,8,1),(1,9,1),(1,10,1),(1,11,1),(1,15,1),(1,18,1),(1,26,1),(2,1,3),(2,2,1),(2,4,1),(2,6,1),(2,12,1),(2,13,1),(2,15,2),(2,16,1),(2,17,1),(2,18,1),(2,29,1),(2,30,1),(3,21,1),(4,22,1),(5,23,1),(6,7,1),(6,12,3),(6,13,2),(6,15,1),(6,16,1),(6,18,2),(6,24,2),(6,25,2),(6,27,1),(6,28,1),(6,31,1),(7,4,3),(7,14,3),(7,16,4);
/*!40000 ALTER TABLE `comida_ingrediente` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-08 15:58:10
