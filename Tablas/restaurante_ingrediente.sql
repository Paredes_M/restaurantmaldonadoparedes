-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: restaurante
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ingrediente`
--

DROP TABLE IF EXISTS `ingrediente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingrediente` (
  `num_ingrediente` int(11) NOT NULL,
  `nombre_ingrediente` varchar(80) DEFAULT NULL,
  `es_envasado` tinyint(4) NOT NULL,
  `es_bebida` tinyint(4) NOT NULL,
  `descripcion_ingrediente` varchar(400) DEFAULT NULL,
  `cant_ingrediente` int(11) DEFAULT NULL,
  `stock_ingrediente` int(11) DEFAULT NULL,
  PRIMARY KEY (`num_ingrediente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingrediente`
--

LOCK TABLES `ingrediente` WRITE;
/*!40000 ALTER TABLE `ingrediente` DISABLE KEYS */;
INSERT INTO `ingrediente` VALUES (1,'huevo',0,0,'Huevo de gallina',60,6),(2,'pan rallado',0,0,'Pan duro rallado',60,6),(3,'perejil',0,0,'Una planta para condimentar',50,5),(4,'sal',0,0,'Sustancia cristalina para condimentar',100,10),(5,'carne picada de ternera',0,0,'Esto es carne picada de ternera',50,5),(6,'pimienta molida negra',0,0,'Grano de una planta negra para condimentar',50,5),(7,'pimenton dulce',0,0,'Pimenton o aji de color es un condimento en polvo de color rojo',50,5),(8,'sal de ajo',0,0,'Mezcla de ajo seco molido y sal de mesa con un humectante',50,5),(9,'pan de hamburguesa',0,0,'Esto es un pan',60,6),(10,'queso chedar',0,0,'Queso palido de sabor agrio',60,6),(11,'lechuga',0,0,'Planta herbacea verde y plana',50,5),(12,'tomate triturado',1,0,'Planta herbacea roja y redondeada envasado',50,5),(13,'pechuga de pollo',0,0,'Pechuga del pollo',50,5),(14,'papa',0,0,'Tuberculo para acompañar en comidas',100,10),(15,'diente de ajo',0,0,'Bulbillo o diente del ajo',120,12),(16,'aceite de girasol',0,0,'Aceite de origen vegetal que se extrae del prensado de las semilla de girasol',120,20),(17,'queso mozzarella',0,0,'Tipo de queso originario de la cocina italiana',60,6),(18,'cebolla',0,0,'Planta herbacea con numerosas propiedades nutritivas y medicinales',60,6),(19,'pimienta molida blanca',0,0,'Grano de una planta blanca para condimentar',60,6),(20,'oregano',0,0,'Planta medicinal para condimentar',60,6),(21,'coca cola de 350ml',1,1,'Botella de gaseosa coca cola como para un vaso',30,3),(22,'sprite de 350ml',1,1,'Botella de gaseosa sprite como para un vaso',30,3),(23,'fanta de 350ml',1,1,'Botella de gaseosa fanta como para un vaso',30,3),(24,'tallarines',0,0,'Un tipo de pasta alargada, de ancho pequeño y forma achatada',80,8),(25,'zanahoria',0,0,'Hortaliza rica en potazio y fosforo',60,6),(26,'tomate',0,0,'Planta herbacea',50,5),(27,'cubo de caldo de pollo',0,0,'Es una forma concentrada y deshidratada de almacenar un caldo de pollo',50,5),(28,'adobo para pizza',0,0,'Oregano, perejil, aji molido, pimenton y laurel en polvo',50,5),(29,'vinagre balsamico',1,0,'Vinagre de origen italiano',50,5),(30,'azucar',0,0,'Endulzante de origen natural, solido, cristalizado',120,12),(31,'morron',0,0,'Pimiento carnoso',50,5);
/*!40000 ALTER TABLE `ingrediente` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-08 15:58:10
